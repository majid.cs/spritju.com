# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.text import slugify
from django.urls import reverse
from django.core.validators import URLValidator
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.cache import cache
from datetime import datetime, timedelta
from django.utils import timezone
from enum import Enum


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def delete(self, *args, **kwargs):
        pass

    def set_cache(self):
        cache.set(self.__class__.__name__, self)

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

        self.set_cache()

    @classmethod
    def load(cls):
        if cache.get(cls.__name__) is None:
            obj, created = cls.objects.get_or_create(pk=1)
            if not created:
                obj.set_cache()
        return cache.get(cls.__name__)


# SiteSettings models here.
class SiteSettings(SingletonModel):
    Spritju_support_Email = models.EmailField(default='support@spritju.com')
    Spritju_phone_number = models.CharField(max_length=255, null=True, blank=True)
    Spritju_Instagram_url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)
    Spritju_Facebook_url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)
    Spritju_linkedin_url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)
    Spritju_twitter_url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)
    Solar_panel_power = models.FloatField(default=310, blank=True, null=True)
    Solar_panel_price = models.FloatField(default=14, blank=True, null=True)
    system_off_price = models.FloatField(default=0.7, blank=True, null=True)
    Panel_area = models.FloatField(default=1.64, blank=True, null=True)
    area_ratio = models.FloatField(default=0.35, blank=True, null=True)
    Actionbox_title = models.TextField(blank=True, null=True)
    Actionbox_description = models.TextField(blank=True, null=True)
    Actionbox_url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)
    Actionbox_url_text = models.CharField(max_length=255, null=True, blank=True)


# models here.
class Category(models.Model):
    name = models.CharField(max_length=32)
    slug = models.SlugField(max_length=80, blank=True)
    posts = models.ManyToManyField('Post', blank=True, related_name='category_posts')
    description = models.TextField(max_length=300, blank=True, null=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('Category_Posts', kwargs={'slug': self.slug})


class client(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)
    logo = ProcessedImageField(processors=[ResizeToFill(430, 450)], upload_to='images/profiles/', format='PNG',
                               options={'quality': 60}, null=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author')
    category = models.ManyToManyField(Category, blank=True, related_name='post_category')
    title = models.CharField(max_length=80, unique=True)
    slug = models.SlugField(max_length=80, blank=True, unique=True)
    content = RichTextUploadingField(blank=True)
    STATE_CHOICES = (
        (0, 'Published'),
        (1, 'Unpublished'),
    )
    status = models.IntegerField(choices=STATE_CHOICES, default=1)
    short_description = models.TextField(max_length=120, blank=True, null=True)
    Meta_description = models.TextField(blank=True, null=True)
    Meta_keywords = models.CharField(max_length=90, blank=True, null=True)
    updated_on = models.DateField(auto_now=True)
    created_on = models.DateField(auto_now_add=True)
    publish_on = models.DateField()
    post_image = ProcessedImageField(processors=[ResizeToFill(870, 400)], upload_to='images/posts/', format='JPEG',
                                     options={'quality': 60}, null=True)
    post_thumb = ImageSpecField(processors=[ResizeToFill(358, 246)], format='JPEG', options={'quality': 60},
                                source='post_image')
    post_small_thumb = ImageSpecField(processors=[ResizeToFill(50, 50)], format='JPEG', options={'quality': 60},
                                      source='post_image')
    post_small_nex = ImageSpecField(processors=[ResizeToFill(125, 75)], format='JPEG', options={'quality': 60},
                                    source='post_image')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('post', kwargs={'slug': self.slug})


class Comment(models.Model):
    Name = models.CharField(max_length=100, null=False, blank=False)
    Email = models.EmailField(max_length=255, null=False, blank=False)
    Phone_Number = models.CharField(max_length=30, null=True, blank=True)
    Website_Url = models.URLField(null=True, blank=True)
    Message = models.TextField(null=False, blank=False)
    replier = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comment_replier', null=True, blank=True)
    reply = models.TextField(null=True, blank=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comment_post')
    STATE_CHOICES = (
        (0, 'Published'),
        (1, 'Unpublished'),
    )
    status = models.IntegerField(choices=STATE_CHOICES, default=1)

    def __str__(self):
        return self.Name + self.Email


class Faq(models.Model):
    questioner_name = models.CharField(max_length=100, null=False, blank=False)
    questioner_email = models.EmailField(max_length=255, null=False, blank=False)
    question = models.TextField()
    replier = models.ForeignKey(User, on_delete=models.CASCADE, related_name='faq_replier')
    reply = models.TextField(blank=True)
    STATE_CHOICES = (
        (0, 'Published'),
        (1, 'Unpublished'),
    )
    status = models.IntegerField(choices=STATE_CHOICES, default=1)

    def __str__(self):
        return self.question


class Contact(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    email = models.EmailField(max_length=255, null=False, blank=False)
    phone = models.CharField(max_length=25, null=False, blank=False)
    body = models.TextField()

    def __str__(self):
        return self.name


class New_Customer(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    email = models.EmailField(max_length=255, null=False, blank=False)
    phone = models.CharField(max_length=100, null=False, blank=False)
    address = models.CharField(max_length=300, null=True, blank=True)
    Lat = models.FloatField(blank=True, null=True)
    lon = models.FloatField(blank=True, null=True)
    offer = models.FloatField(blank=True, null=True)
    body = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Service(models.Model):
    title = models.CharField(max_length=80, unique=True)
    slug = models.SlugField(max_length=80, blank=True, unique=True)
    content = RichTextUploadingField(blank=True)
    Meta_description = models.TextField(blank=True, null=True)
    Meta_keywords = models.CharField(max_length=90, blank=True, null=True)
    Service_image = ProcessedImageField(processors=[ResizeToFill(1920, 1080)], upload_to='images/posts/', format='JPEG',
                                        options={'quality': 60}, null=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Service, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('Service', kwargs={'slug': self.slug})


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True, null=True)
    profile_image = ProcessedImageField(processors=[ResizeToFill(430, 450)], upload_to='images/profiles/',
                                        format='JPEG', options={'quality': 60}, null=True)
    instagram_url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)
    twitter_url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)
    linkedin_url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)
    facebook_url = models.CharField(max_length=300, validators=[URLValidator()], blank=True, null=True)


class Generator(models.Model):
    full_name = models.CharField(max_length=150)
    email = models.EmailField(max_length=255, null=False, blank=False)
    address = models.TextField(max_length=500, blank=True, null=True)
    phone = models.CharField(max_length=100, null=False, blank=False)
    registration_date = models.DateTimeField(auto_now=True)

    class EnergyType(Enum):
        SOLAR = 'Solar'
        HYDRO = 'Hydro'
        WIND = 'Wind'

    energy_type = models.CharField(max_length=5, choices=[(energy, energy.value) for energy in EnergyType])

    def __str__(self):
        return self.full_name

class Consumer(models.Model):
    full_name = models.CharField(max_length=150)
    email = models.EmailField(max_length=255, null=False, blank=False)
    address = models.TextField(max_length=500, blank=True, null=True)
    phone = models.CharField(max_length=100, null=False, blank=False)
    registration_date = models.DateTimeField(auto_now=True)
    consumption_size = models.IntegerField()

    class EnergyType(Enum):
        SOLAR = 'Solar'
        HYDRO = 'Hydro'
        WIND = 'Wind'

    energy_preference = models.CharField(max_length=5, choices=[(energy, energy.value) for energy in EnergyType])

    def __str__(self):
        return self.full_name

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
