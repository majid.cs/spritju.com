# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.SiteSettings)
admin.site.register(models.client)

@admin.register(models.Post)
class PostAdmin(admin.ModelAdmin):
    search_fields = ['title', 'Meta_keywords','content','Meta_description']
    list_display = ('title','author','status','Meta_keywords')
    list_filter = ['status','category']

@admin.register(models.Service)
class ServiceAdmin(admin.ModelAdmin):
    search_fields = ['title', 'Meta_keywords','content','Meta_description']
    list_display = ('title','Meta_description','Meta_keywords')

@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ('name','slug')

@admin.register(models.Comment)
class CommentAdmin(admin.ModelAdmin):
    search_fields = ['Name','Email','Phone_Number','Message','Website_Url']
    list_display =  ['Name','Email','Phone_Number','Message','replier','post','status','Website_Url']
    list_filter = ['post','status']

@admin.register(models.Contact)
class ContactAdmin(admin.ModelAdmin):
    search_fields = ['name','email','phone','body']
    list_display =  ['name','email','phone']

@admin.register(models.New_Customer)
class NewCustomerAdmin(admin.ModelAdmin):
    search_fields = ['name','email','phone','address']
    list_display =  ['name','email','phone','address']

@admin.register(models.Faq)
class FaqAdmin(admin.ModelAdmin):
    search_fields = ['questioner_name','questioner_email','question']
    list_display =  ['questioner_name','questioner_email','question','replier','status']
    list_filter = ['status']

admin.site.register(models.Profile)
