# api/serializers.py

from rest_framework import serializers
from .models import Post

class PostSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Post
#        fields = ('id', 'author', 'title', 'Meta_keywords')
        read_only_fields = ('id', 'title')
