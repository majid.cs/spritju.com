# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import logging
import smtplib
import csv
import time
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import requests
from django.core.mail import send_mail, BadHeaderError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q

from django.shortcuts import get_object_or_404
from django.shortcuts import render, redirect
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.http import JsonResponse
from django.http import (HttpResponse, HttpResponseBadRequest, HttpResponseForbidden)
from .forms import CommentForm, ContactForm, newcustomerForm
from .models import Post, Comment, Category, Contact, New_Customer, Profile, Faq, Service

# Get an instance of a logger
CONSUMPTION = 'Consumption'
SOLAR_PARK_IN_LOCATION_Y = 'Solar Park'
WIND_PARK_IN_LOCATION_X = 'Wind Park'
SINGLE_ORB = 'Single Orb'
DOUBLE_ORB = 'Double Orb'
TRIPLE_ORB = 'Triple Orb'
ORBLESS = 'Orbless'
EMERALD = 'Emerald'
JADE = 'Jade'
PYRITE = 'Pyrite'
ONYX = 'Onyx'

logger = logging.getLogger(__name__)

# creating empty lists
hourly_consumption = []
hourly_generation_x = []
hourly_generation_y = []
single_orb = []
double_orb = []
triple_orb = []
orbless = []
emerald = []
jade = []
pyrite = []
onyx = []
hour = []

esvaas_data_file_is_read = False


def handler404(request, exception):
    return render(request, 'spritju/error-404.html', status=404)


def handler500(request):
    return redirect('index')


def index(request):
    return render(request, 'spritju/index.html')


def about(request):
    form_class = ContactForm
    return render(request, 'spritju/about.html', {'form': form_class})


def blog(request):
    Categories = Category.objects.all()
    Services = Service.objects.all()
    posts = Post.objects.order_by("-id").all()
    paginator = Paginator(posts, 4)
    page = request.GET.get('page')
    recentrposts = Post.objects.filter(status=0).order_by('-created_on')[0:3]
    try:
        # create Page object for the given page
        posts = paginator.page(page)
    except PageNotAnInteger:
        # if page parameter in the query string is not available, return the first page
        posts = paginator.page(1)
    except EmptyPage:
        # if the value of the page parameter exceeds num_pages then return the last page
        posts = paginator.page(paginator.num_pages)
    return render(request, 'spritju/blog.html',
                  {'Categories': Categories, 'posts': posts, 'recentrposts': recentrposts, 'Services': Services})


def Categories(request):
    Categories = Category.objects.all()
    Services = Service.objects.all()
    Category = Category.objects.order_by("-id").all()
    paginator = Paginator(Category, 5)
    page = request.GET.get('page')
    recentrposts = Post.objects.filter(status=0).order_by('-created_on')[0:3]
    try:
        # create Page object for the given page
        catis = paginator.page(page)
    except PageNotAnInteger:
        # if page parameter in the query string is not available, return the first page
        catis = paginator.page(1)
    except EmptyPage:
        # if the value of the page parameter exceeds num_pages then return the last page
        catis = paginator.page(paginator.num_pages)
    return render(request, 'spritju/blog-old.html',
                  {'Categories': Categories, 'recentrposts': recentrposts, 'catis': catis, 'Services': Services})


def Category_Posts(request, slug):
    Categories = Category.objects.all()
    Services = Service.objects.all()
    cat = get_object_or_404(Category, slug=slug)
    posts = Post.objects.filter(category__id__exact=cat.id, status=0).order_by("-id")
    paginator = Paginator(posts, 5)
    page = request.GET.get('page')
    recentrposts = Post.objects.filter(status=0).order_by('-created_on')[0:3]
    try:
        # create Page object for the given page
        posts = paginator.page(page)
    except PageNotAnInteger:
        # if page parameter in the query string is not available, return the first page
        posts = paginator.page(1)
    except EmptyPage:
        # if the value of the page parameter exceeds num_pages then return the last page
        posts = paginator.page(paginator.num_pages)
    return render(request, 'spritju/category.html',
                  {'posts': posts, 'cat': cat, 'recentrposts': recentrposts, 'Categories': Categories,
                   'Services': Services})


@csrf_exempt
def webapp(request):
    lat = request.GET.get('lat', '')
    lon = request.GET.get('lon', '')
    rqurl = "http://re.jrc.ec.europa.eu/pvgis5/PVcalc.php?lat=" + lat + "&lon=" + lon + "&pvtechchoice=crystSi&mountingplace=building&peakpower=1&loss=10&optimalinclination=1&optimalangles=1&raddatabase=PVGIS-SARAH"
    r = requests.get(rqurl)
    # r = requests.get('http://re.jrc.ec.europa.eu/pvgis5/PVcalc.php?lat=59&lon=18&peakpower=1&loss=10&optimalinclination=1&optimalangles=1&raddatabase=PVGIS-SARAH')
    data = r.text.strip().split("\t")
    results = data[151]
    return render(request, 'spritju/webapp.html', {'results': results})


"""        if lat :
            if lon :
                rqurl = 'http://re.jrc.ec.europa.eu/pvgis5/PVcalc.php?lat=' + lat + '&lon=' + lon + '&peakpower=1&loss=10&optimalinclination=1&optimalangles=1&raddatabase=PVGIS-SARAH'
                r = requests.get('http://re.jrc.ec.europa.eu/pvgis5/PVcalc.php?lat=59&lon=18&peakpower=1&loss=10&optimalinclination=1&optimalangles=1&raddatabase=PVGIS-SARAH')
                data = r.text.strip().split("\t")
                #print(r.text.strip().split("\t").index('81.2'))
                results = data[151]
                return render(request, 'spritju/webapp.html', {'results': results})"""


def PostDetailView(request, slug):
    PD = get_object_or_404(Post, slug=slug)
    PF = Profile.objects.get(user=PD.author)
    PC = Comment.objects.filter(post=PD, status=0)
    posts = Post.objects.filter(status=0).order_by('-created_on')[0:3]
    Categories = Category.objects.all()
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = PD
            comment.save()
            messages.success(request, 'Your message is successfully sent!')
            return redirect(request.path,
                            context={'PD': PD, 'form': CommentForm(), 'posts': posts, 'Categories': Categories,
                                     'PF': PF, 'PC': PC})
        else:
            messages.error(request, 'failed in sending your message!')
            return redirect(request.path,
                            context={'PD': PD, 'form': CommentForm(), 'posts': posts, 'Categories': Categories,
                                     'PF': PF, 'PC': PC})
    else:
        form = CommentForm()
    return render(request, 'spritju/blog-post.html',
                  context={'PD': PD, 'form': form, 'posts': posts, 'Categories': Categories, 'PF': PF, 'PC': PC})


def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()

            return redirect('Post-detail', pk=post.pk)
    else:
        form = CommentForm()
    return render(request, 'spritju/blog-single-image-post.html', {'form': form, 'not_send': not_send})


def esvaas(request):
    return render(request, 'spritju/esvaas/index.html')


def consumption(request):
    init_esvaas_data()
    data = {
        'consumptions': hourly_consumption,
        'generation': {WIND_PARK_IN_LOCATION_X: hourly_generation_x, SOLAR_PARK_IN_LOCATION_Y: hourly_generation_y}
    }
    return render(request, 'spritju/esvaas/consumption.html', data)


def generation(request):
    init_esvaas_data()
    data = {
        'consumptions': hourly_consumption,
        'generation': {WIND_PARK_IN_LOCATION_X: hourly_generation_x, SOLAR_PARK_IN_LOCATION_Y: hourly_generation_y}
    }
    return render(request, 'spritju/esvaas/generation.html', data)


def matching(request):
    init_esvaas_data()

    """ define stones """
    emerald_stone = MatchingStone('emerald', '#046307', emerald)
    jade_stone = MatchingStone('jade', '#00a36c', jade)
    pyrite_stone = MatchingStone('pyrite', '#ddba44', pyrite)
    onyx_stone = MatchingStone('onyx', '#353935', onyx)

    """ define orbs """
    triple = MatchingOrb('Triple Orb', '#009600FF', triple_orb)
    double = MatchingOrb('Double Orb', '#1E90FFFF', double_orb)
    single = MatchingOrb('Single Orb', '#FF8C00FF', single_orb)
    orbless_matching = MatchingOrb('Orbless', '#FF0000FF', orbless)

    data = {
        'hours': range(1, 25),
        'days': range(2, 32),
        'consumption': hourly_consumption,
        'consumption_matching_orbs': [triple, double, single, orbless_matching],
        'consumption_matching_stones': [emerald_stone, jade_stone, pyrite_stone, onyx_stone],
    }
    return render(request, 'spritju/esvaas/matching.html', data)


def init_esvaas_data():
    global esvaas_data_file_is_read

    if not esvaas_data_file_is_read:
        esvaas_data_file_is_read = True
        # open the file in read mode
        csvFile = open('spritju/resources/demo-data.csv', 'r')
        file = csv.DictReader(csvFile)
        file.__next__()

        for col in file:
            hourly_consumption.append(int(col[CONSUMPTION]))
            hourly_generation_x.append(int(col[WIND_PARK_IN_LOCATION_X]))
            hourly_generation_y.append(int(col[SOLAR_PARK_IN_LOCATION_Y]))
            triple_orb.append(int(col[TRIPLE_ORB]))
            double_orb.append(int(col[DOUBLE_ORB]))
            single_orb.append(int(col[SINGLE_ORB]))
            orbless.append(int(col[ORBLESS]))
            emerald.append(int(col[EMERALD]))
            jade.append(int(col[JADE]))
            pyrite.append(int(col[PYRITE]))
            onyx.append(int(col[ONYX]))
            hour.append(int)


@csrf_exempt
def search(request):
    Categories = Category.objects.all()
    Services = Service.objects.all()
    recentrposts = Post.objects.filter(status=0).order_by('-created_on')[0:3]
    if request.method == 'GET':
        sq = request.GET.get(str('search_query'))
        if sq:
            Posts = Post.objects.filter(Q(title__icontains=sq) | Q(content__icontains=sq))
            paginator = Paginator(Posts, 5)
            page = request.GET.get('page')
            try:
                s_results = paginator.page(page)
            except PageNotAnInteger:
                s_results = paginator.page(1)
            except EmptyPage:
                s_results = paginator.page(paginator.num_pages)
            return render(request, 'spritju/search.html',
                          {'Categories': Categories, 'posts': s_results, 'recentrposts': recentrposts,
                           'Services': Services})
    return render(request, 'spritju/search.html',
                  {'Categories': Categories, 'recentrposts': recentrposts, 'Services': Services})


def contact(request):
    form_class = ContactForm
    if request.method == 'POST':
        form = form_class(data=request.POST)
        if form.is_valid():
            contact_name = request.POST.get(
                'contact_name'
                , '')
            contact_email = request.POST.get(
                'contact_email'
                , '')
            contact_phone = request.POST.get(
                'contact_phone'
                , '')
            form_content = request.POST.get('content', '')
            message = """Sender's Name: {name}
Sender's Email Address: {email}
Sender's phone number: {phone}

Message:
{message} """.format(name=contact_name,
                     email=contact_email,
                     phone=contact_phone,
                     message=form_content)
            send_mail(
                'Spritju contact inquiry',
                message,
                contact_email,
                ['support@spritju.com'],
                fail_silently=False,
            )
            return JsonResponse(json.loads(' {"success": true} '))
        else:
            json_response = ' {"success": false, "errors": [ '
            errors = form.errors.values()
            for err in errors:
                json_response += '"' + err[0] + '"'
            json_response += ']}'
            response = json.loads(json_response)
            return JsonResponse(response, status=400)

    return JsonResponse({"error": ""}, status=400)


def ceo(request):
    Categories = Category.objects.all()
    Services = Service.objects.all()
    return render(request, 'spritju/about-ceo.html', {'Categories': Categories, 'Services': Services})


def newcustomer(request):
    if request.method == "POST":
        form = newcustomerForm(request.POST)
        if form.is_valid():
            customer = New_Customer()
            customer.name = request.POST.get(
                'customer_name'
                , '')
            customer.email = request.POST.get(
                'customer_email'
                , '')
            customer.phone = request.POST.get(
                'customer_phone'
                , '')
            customer.address = request.POST.get(
                'customer_address'
                , '')
            customer.Lat = request.POST.get(
                'customer_Lat'
                , '')
            customer.lon = request.POST.get(
                'customer_lon'
                , '')
            customer.offer = request.POST.get(
                'customer_offer'
                , '')
            customer.save()
            array_power = request.POST.get(
                'customer_array'
                , '')
            produced_energy = request.POST.get(
                'customer_produced'
                , '')
            offer_including = request.POST.get(
                'customer_inoffer'
                , '')
            Spritju_num = request.POST.get(
                'customer_spritju'
                , '')
            # Define to/from
            sender = 'support@spritju.com'
            recipient = customer.email

            # Create message
            html_message = loader.render_to_string(
                'spritju/email.html',
                {
                    'PRODUCED_ENERGY': str(produced_energy),
                    'ARRAY_POWER': str(array_power),
                    'SPRITJU': str(Spritju_num),
                    'INCLUDING': str(offer_including),
                    'EXCLUDING': str(customer.offer)
                }
            )
            msgRoot = MIMEMultipart('related')
            msg = MIMEText(html_message.encode('utf-8'), 'html', 'utf-8')
            msgRoot.attach(msg)
            fp = open('static/images/Email/mail.png', 'rb')
            msgImage = MIMEImage(fp.read())
            fp.close()
            msgImage.add_header('Content-ID', '<mailpng>')
            msgRoot.attach(msgImage)
            fp = open('static/images/Email/logo.png', 'rb')
            msgImage = MIMEImage(fp.read())
            fp.close()
            msgImage.add_header('Content-ID', '<logopng>')
            msgRoot.attach(msgImage)
            fp = open('static/images/Email/phone.png', 'rb')
            msgImage = MIMEImage(fp.read())
            fp.close()
            msgImage.add_header('Content-ID', '<phonepng>')
            msgRoot.attach(msgImage)
            msgRoot['Subject'] = "Sent from spritju"
            msgRoot['From'] = sender
            msgRoot['To'] = recipient
            # Create server object with SSL option
            server = smtplib.SMTP_SSL('smtp.zoho.eu', 465)

            # Perform operations via server
            server.login('admin@spritju.com', '09365410044qw')
            server.sendmail(sender, [recipient], msgRoot.as_string())
            server.quit()
            return redirect('index')
        return redirect('index')
    return redirect('index')


def ServiceDetailView(request, slug):
    SD = get_object_or_404(Service, slug=slug)
    Categories = Category.objects.all()
    Services = Service.objects.all()
    return render(request, 'spritju/service.html', context={'SD': SD, 'Categories': Categories, 'Services': Services})


def __create_email(customer_email, customer_name):
    # Define to/from
    sender = 'support@spritju.com'

    # Create message
    html_message = loader.render_to_string(
        'spritju/email_templates/email_mvp.html',
        {
            'PRODUCER_NAME': customer_name,
        }
    )
    msgRoot = MIMEMultipart('related')
    msg = MIMEText(html_message.encode('utf-8'), 'html', 'utf-8')
    msgRoot.attach(msg)
    fp = open('static/images/Email/mail.png', 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()
    msgImage.add_header('Content-ID', '<mailpng>')
    msgRoot.attach(msgImage)
    fp = open('static/images/Email/logo.png', 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()
    msgImage.add_header('Content-ID', '<logopng>')
    msgRoot.attach(msgImage)
    fp = open('static/images/Email/phone.png', 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()
    msgImage.add_header('Content-ID', '<phonepng>')
    msgRoot.attach(msgImage)
    msgRoot['Subject'] = "Sent from spritju"
    msgRoot['From'] = sender
    msgRoot['To'] = customer_email
    # Create server object with SSL option
    server = smtplib.SMTP_SSL('smtp.zoho.eu', 465)

    # Perform operations via server
    server.login('admin@spritju.com', '09365410044qw')
    server.sendmail(sender, [customer_email], msgRoot.as_string())
    server.quit()


class MatchingStone:
    def __init__(self, name, color_code, values):
        self.name = name
        self.color_code = color_code
        self.values = values


class MatchingOrb:
    def __init__(self, name, color_code, values):
        self.name = name
        self.color_code = color_code
        self.values = values
