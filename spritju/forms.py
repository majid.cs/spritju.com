from django import forms
from .models import Comment, Generator, Consumer


class CommentForm(forms.ModelForm):
    Message = forms.CharField(required=True,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'rows': '7',
                'placeholder': 'Write your comment here ...'
            }
        )
    )
    Email = forms.EmailField(required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'type':'email',
                'placeholder': 'Email Adress*'
            }
        )
    )
    Name = forms.CharField(required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Your Name*'
            }
        )
    )
    Phone_Number = forms.CharField(required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Phone Number'
            }
        )
    )
    Website_Url = forms.URLField(required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Website URL'
            }
        )
    )
    class Meta:
        model = Comment
        fields = ['Name', 'Email', 'Phone_Number', 'Message','Website_Url']
        help_texts = {'Name': ('Enter your first name and last name.'), 'Email': ('Enter your Email Address.'), 'Phone_Number': ('Enter your Phone Number.'), 'Message': ('Write your comment here ...')}

# our new form
class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Name*'}))
    contact_email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Email*'}))
    contact_phone = forms.CharField(required=False, widget=forms.TextInput(attrs={'type':'number', 'class':'form-control','placeholder':'Phone'}))
    content = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={'class':'form-control','placeholder':'Message*'})    )

class newcustomerForm(forms.Form):
    customer_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'id':'name','name':'name','type':'text','class':'form-control input-md','placeholder':'Name*'}))
    customer_email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'id':'email','name':'email','type':'email','class':'form-control input-md','placeholder':'Email*'}))
    customer_phone = forms.CharField(required=True, widget=forms.TextInput(attrs={'id':'phone','name':'phone','type':'text','class':'form-control input-md','placeholder':'your phone number*'}))
    customer_lon = forms.FloatField(widget=forms.NumberInput(attrs={'id':'lon','name':'lon','value':'0','class':'form-control input-md','type':'hidden'}))
    customer_Lat = forms.FloatField(widget=forms.NumberInput(attrs={'id':'lat','name':'lat','value':'0','class':'form-control input-md','type':'hidden'}))
    customer_offer = forms.FloatField(widget=forms.NumberInput(attrs={'id':'offer','name':'offer','value':'0','class':'form-control input-md','type':'hidden'}))
    customer_address = forms.CharField(widget=forms.TextInput(attrs={'id':'address','name':'address','value':'default','class':'form-control input-md','type':'hidden'}))
    customer_inoffer = forms.FloatField(widget=forms.NumberInput(attrs={'id':'inoffer','name':'inoffer','value':'0','class':'form-control input-md','type':'hidden'}))
    customer_array = forms.FloatField(widget=forms.NumberInput(attrs={'id':'array','name':'array','value':'0','class':'form-control input-md','type':'hidden'}))
    customer_produced = forms.FloatField(widget=forms.NumberInput(attrs={'id':'produced','name':'produced','value':'0','class':'form-control input-md','type':'hidden'}))
    customer_spritju = forms.FloatField(widget=forms.NumberInput(attrs={'id':'cu_spritju','name':'spritju','value':'0','class':'form-control input-md','type':'hidden'}))

class GeneratorRegisterForm(forms.ModelForm):
    full_name = forms.CharField(widget=forms.TextInput(attrs={'id':'name','name':'name','type':'text','class':'form-control input-sm','placeholder':'Name*'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'id':'email','name':'email','type':'email','class':'form-control input-sm','placeholder':'Email*'}))
    address = forms.CharField(widget=forms.TextInput(attrs={'id':'address','name':'address','type':'text', 'class':'form-control input-sm', 'placeholder':'Address*'}))
    phone = forms.CharField(widget=forms.TextInput(attrs={'id':'phone','name':'phone','type':'text','class':'form-control input-sm','placeholder':'your phone number*'}))
    energy_type = forms.ChoiceField(choices=Generator.EnergyType, widget=forms.Select())
    check = forms.BooleanField(required = True)

    class Meta:
        model = Generator
        fields = ['full_name', 'email', 'address', 'phone', 'energy_type']

class ConsumerRegistrationForm(forms.ModelForm):
    full_name = forms.CharField(widget=forms.TextInput(attrs={'id':'name','name':'name','type':'text','class':'form-control input-sm','placeholder':'Name*'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'id':'email','name':'email','type':'email','class':'form-control input-sm','placeholder':'Email*'}))
    address = forms.CharField(widget=forms.TextInput(attrs={'id':'address','name':'address','type':'text', 'class':'form-control input-sm', 'placeholder':'Address*'}))
    phone = forms.CharField(widget=forms.TextInput(attrs={'id':'phone','name':'phone','type':'text','class':'form-control input-sm','placeholder':'your phone number*'}))
    consumption_size = forms.IntegerField(widget=forms.NumberInput(attrs={'id':'consumption_size','name':'consumption_size','type':'number','class':'form-control input-sm','placeholder':'estimated yearly consumption (Kwh)*'}))
    energy_preference = forms.ChoiceField(choices=Consumer.EnergyType, widget=forms.Select())
    check = forms.BooleanField(required = True)

    class Meta:
        model = Consumer
        fields = ['full_name', 'email', 'address', 'phone', 'consumption_size', 'energy_preference']