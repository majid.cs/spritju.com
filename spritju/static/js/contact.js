$(document).ready(function () {
    let urlParams = new URLSearchParams(window.location.search);
    let demo = urlParams.get('bookdemo');

    if(demo) {
        $('html,body').animate({scrollTop: $('#contact').offset().top},'slow');
        $("#contact-message textarea[name='content']").text('[Demo request]: \n');
    }

    $("form.contact-form").validate({
        errorPlacement: function (label, element) {
            label.addClass('validation-error');
            label.insertAfter(element);
        },
        wrapper: 'span',
        rules: {
            contact_name: {
                required: true,
            },
            contact_email: {
                required: true,
                email: true
            },
            content: {
                required: true,
                minlength: 5,
                maxlength: 1000,
            },
            contact_phone: {
                required: false
            }
        },
        messages: {
            contact_name: {
                required: "Please enter your name",
            },
            contact_email: {
                required: "Please enter your email address",
            },
            content: {
                required: "Please provide a message",
                minlength: "We would like to hear more from you, at least 5 characters ;-)",
                maxlength: "Let's keep the conversation short and sweet, maximum 1000 characters ;-)"
            },
        },
        submitHandler: function (form) {
            $(".contact .contact-form .loading").show();
            $(form).ajaxSubmit({
                url: form.action,
                type: "post",
                success: function () {
                    $(".contact .contact-form .error-message").hide();
                    $(".contact .contact-form .sent-message").show();
                    $(".contact .contact-form .sent-message").fadeOut(3000);
                    form.reset();
                },
                statusCode: {
                    400: function (data) {
                        $(".contact .contact-form .error-message").text(data.responseJSON.errors);
                        $(".contact .contact-form .error-message").show();
                    }
                }
            });
        }
    });

    // ajaxStop
    $(document).ajaxStop(function () {
        $(".contact .contact-form .loading").hide();
    });
})