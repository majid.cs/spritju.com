let generationBarChartElement = document.getElementById("generationBarChart")
let generationContext = generationBarChartElement != null ? generationBarChartElement.getContext('2d') : null;
let consumptionBarChartElement = document.getElementById("consumptionBarChart");
let consumptionContext = consumptionBarChartElement != null ? consumptionBarChartElement.getContext('2d') : null;
let consumptionChart = null;
let generationChart = null;
if (generationContext != null ) { generationChart = new Chart(generationContext, {
    type: 'bar',
    data: {
        labels: ["01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"],
        datasets: [{
            label: '',
            backgroundColor: "#008d93",
            data: [],
        },
            {
                label: '',
                backgroundColor: "#caf270",
                data: [],
            }],
    },
    options: {
        tooltips: {
            displayColors: true,
            callbacks: {
                mode: 'x',
            },
        },
        scales: {
            xAxes: [{
                stacked: true,
                gridLines: {
                    display: false,
                }
            }],
            yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero: true,
                },
                type: 'linear',
            }]
        },
        responsive: true,
        maintainAspectRatio: false,
        legend: {position: 'bottom'},
    }
});}
if (consumptionContext != null ) { consumptionChart = new Chart(consumptionContext, {
    type: 'bar',
    data: {
        labels: ["01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"],
        datasets: [{
            label: '',
            backgroundColor: "#008d93",
            data: [],
        }],
    },
    options: {
        tooltips: {
            displayColors: true,
            callbacks: {
                mode: 'x',
            },
        },
        scales: {
            xAxes: [{
                stacked: true,
                gridLines: {
                    display: false,
                }
            }],
            yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero: true,
                },
                type: 'linear',
            }]
        },
        responsive: true,
        maintainAspectRatio: false,
        legend: {position: 'bottom'},
    }
});
}