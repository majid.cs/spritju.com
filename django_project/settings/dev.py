from django_project.settings.defaults import *

DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'spritjuweb',
        'USER': 'django',
        'PASSWORD': '56aa77f5563297fbd5d991b45243264a',
        'HOST': 'localhost',
        'PORT': '',
    }
}

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'


STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "staticfiles"),
)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'