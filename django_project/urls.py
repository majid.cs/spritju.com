"""spritju URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
import spritju.views as spritju_views


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^webapp/$', spritju_views.webapp, name='webapp'),
    url(r'^newcustomer/$', spritju_views.newcustomer, name='newcustomer'),
    url(r'^search/$', spritju_views.search, name='search'),
    url(r'^$', spritju_views.index, name='index'),
    url(r'^contact/$', spritju_views.contact, name='contact'),
    url(r'^career/$', spritju_views.contact, name='career'),
    url(r'^blog/$', spritju_views.blog, name='blog'),
    url(r'^blog/(?P<slug>[-\w]+)/$', spritju_views.PostDetailView, name='post'),
    url(r'^service/(?P<slug>[-\w]+)/$', spritju_views.ServiceDetailView, name='Service'),
    url(r'^cat/$', spritju_views.Categories, name='Categories'),
    url(r'^cat/(?P<slug>[-\w]+)/$', spritju_views.Category_Posts, name='Category_Posts'),
    url(r'^about/$', spritju_views.about, name='about'),
    url(r'^esvaas/$', spritju_views.esvaas, name='esvaas'),
    url(r'^esvaas/consumption/$', spritju_views.consumption, name='consumption'),
    url(r'^esvaas/generation/$', spritju_views.generation, name='generation'),
    url(r'^esvaas/matching/$', spritju_views.matching, name='matching'),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


handler404 = spritju_views.handler404
handler500 = spritju_views.handler500
