--
-- PostgreSQL database dump
--

-- Dumped from database version 10.17 (Ubuntu 10.17-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.17 (Ubuntu 10.17-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO django;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO django;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO django;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO django;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO django;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO django;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO django;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO django;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO django;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO django;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO django;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO django;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO django;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO django;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO django;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO django;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO django;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO django;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO django;

--
-- Name: spritju_category; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_category (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    slug character varying(80) NOT NULL,
    description text
);


ALTER TABLE public.spritju_category OWNER TO django;

--
-- Name: spritju_category_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_category_id_seq OWNER TO django;

--
-- Name: spritju_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_category_id_seq OWNED BY public.spritju_category.id;


--
-- Name: spritju_category_posts; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_category_posts (
    id integer NOT NULL,
    category_id integer NOT NULL,
    post_id integer NOT NULL
);


ALTER TABLE public.spritju_category_posts OWNER TO django;

--
-- Name: spritju_category_posts_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_category_posts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_category_posts_id_seq OWNER TO django;

--
-- Name: spritju_category_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_category_posts_id_seq OWNED BY public.spritju_category_posts.id;


--
-- Name: spritju_client; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_client (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    url character varying(300),
    logo character varying(100)
);


ALTER TABLE public.spritju_client OWNER TO django;

--
-- Name: spritju_client_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_client_id_seq OWNER TO django;

--
-- Name: spritju_client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_client_id_seq OWNED BY public.spritju_client.id;


--
-- Name: spritju_comment; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_comment (
    id integer NOT NULL,
    "Name" character varying(100) NOT NULL,
    "Email" character varying(255) NOT NULL,
    "Phone_Number" character varying(30),
    "Website_Url" character varying(200),
    "Message" text NOT NULL,
    reply text,
    status integer NOT NULL,
    post_id integer NOT NULL,
    replier_id integer
);


ALTER TABLE public.spritju_comment OWNER TO django;

--
-- Name: spritju_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_comment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_comment_id_seq OWNER TO django;

--
-- Name: spritju_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_comment_id_seq OWNED BY public.spritju_comment.id;


--
-- Name: spritju_contact; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_contact (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    email character varying(255) NOT NULL,
    phone character varying(25) NOT NULL,
    body text NOT NULL
);


ALTER TABLE public.spritju_contact OWNER TO django;

--
-- Name: spritju_contact_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_contact_id_seq OWNER TO django;

--
-- Name: spritju_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_contact_id_seq OWNED BY public.spritju_contact.id;


--
-- Name: spritju_faq; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_faq (
    id integer NOT NULL,
    questioner_name character varying(100) NOT NULL,
    questioner_email character varying(255) NOT NULL,
    question text NOT NULL,
    reply text NOT NULL,
    status integer NOT NULL,
    replier_id integer NOT NULL
);


ALTER TABLE public.spritju_faq OWNER TO django;

--
-- Name: spritju_faq_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_faq_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_faq_id_seq OWNER TO django;

--
-- Name: spritju_faq_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_faq_id_seq OWNED BY public.spritju_faq.id;


--
-- Name: spritju_new_customer; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_new_customer (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    email character varying(255) NOT NULL,
    phone character varying(100) NOT NULL,
    address character varying(300),
    "Lat" double precision,
    lon double precision,
    offer double precision,
    body text
);


ALTER TABLE public.spritju_new_customer OWNER TO django;

--
-- Name: spritju_new_customer_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_new_customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_new_customer_id_seq OWNER TO django;

--
-- Name: spritju_new_customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_new_customer_id_seq OWNED BY public.spritju_new_customer.id;


--
-- Name: spritju_post; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_post (
    id integer NOT NULL,
    title character varying(80) NOT NULL,
    slug character varying(80) NOT NULL,
    content text NOT NULL,
    status integer NOT NULL,
    short_description text,
    "Meta_description" text,
    "Meta_keywords" character varying(90),
    updated_on date NOT NULL,
    created_on date NOT NULL,
    publish_on date NOT NULL,
    post_image character varying(100),
    author_id integer NOT NULL
);


ALTER TABLE public.spritju_post OWNER TO django;

--
-- Name: spritju_post_category; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_post_category (
    id integer NOT NULL,
    post_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.spritju_post_category OWNER TO django;

--
-- Name: spritju_post_category_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_post_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_post_category_id_seq OWNER TO django;

--
-- Name: spritju_post_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_post_category_id_seq OWNED BY public.spritju_post_category.id;


--
-- Name: spritju_post_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_post_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_post_id_seq OWNER TO django;

--
-- Name: spritju_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_post_id_seq OWNED BY public.spritju_post.id;


--
-- Name: spritju_profile; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_profile (
    id integer NOT NULL,
    bio text,
    profile_image character varying(100),
    instagram_url character varying(300),
    twitter_url character varying(300),
    linkedin_url character varying(300),
    facebook_url character varying(300),
    user_id integer NOT NULL
);


ALTER TABLE public.spritju_profile OWNER TO django;

--
-- Name: spritju_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_profile_id_seq OWNER TO django;

--
-- Name: spritju_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_profile_id_seq OWNED BY public.spritju_profile.id;


--
-- Name: spritju_service; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_service (
    id integer NOT NULL,
    title character varying(80) NOT NULL,
    slug character varying(80) NOT NULL,
    content text NOT NULL,
    "Meta_description" text,
    "Meta_keywords" character varying(90),
    "Service_image" character varying(100)
);


ALTER TABLE public.spritju_service OWNER TO django;

--
-- Name: spritju_service_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_service_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_service_id_seq OWNER TO django;

--
-- Name: spritju_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_service_id_seq OWNED BY public.spritju_service.id;


--
-- Name: spritju_sitesettings; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_sitesettings (
    id integer NOT NULL,
    "Spritju_support_Email" character varying(254) NOT NULL,
    "Spritju_phone_number" character varying(255),
    "Spritju_Instagram_url" character varying(300),
    "Spritju_Facebook_url" character varying(300),
    "Spritju_linkedin_url" character varying(300),
    "Spritju_twitter_url" character varying(300),
    "Solar_panel_price" double precision,
    "Solar_panel_power" double precision,
    system_off_price double precision,
    "Panel_area" double precision,
    area_ratio double precision,
    "Actionbox_description" text,
    "Actionbox_title" text,
    "Actionbox_url" character varying(300),
    "Actionbox_url_text" character varying(255)
);


ALTER TABLE public.spritju_sitesettings OWNER TO django;

--
-- Name: spritju_sitesettings_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_sitesettings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_sitesettings_id_seq OWNER TO django;

--
-- Name: spritju_sitesettings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_sitesettings_id_seq OWNED BY public.spritju_sitesettings.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: spritju_category id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category ALTER COLUMN id SET DEFAULT nextval('public.spritju_category_id_seq'::regclass);


--
-- Name: spritju_category_posts id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts ALTER COLUMN id SET DEFAULT nextval('public.spritju_category_posts_id_seq'::regclass);


--
-- Name: spritju_client id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_client ALTER COLUMN id SET DEFAULT nextval('public.spritju_client_id_seq'::regclass);


--
-- Name: spritju_comment id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_comment ALTER COLUMN id SET DEFAULT nextval('public.spritju_comment_id_seq'::regclass);


--
-- Name: spritju_contact id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_contact ALTER COLUMN id SET DEFAULT nextval('public.spritju_contact_id_seq'::regclass);


--
-- Name: spritju_faq id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_faq ALTER COLUMN id SET DEFAULT nextval('public.spritju_faq_id_seq'::regclass);


--
-- Name: spritju_new_customer id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_new_customer ALTER COLUMN id SET DEFAULT nextval('public.spritju_new_customer_id_seq'::regclass);


--
-- Name: spritju_post id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post ALTER COLUMN id SET DEFAULT nextval('public.spritju_post_id_seq'::regclass);


--
-- Name: spritju_post_category id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category ALTER COLUMN id SET DEFAULT nextval('public.spritju_post_category_id_seq'::regclass);


--
-- Name: spritju_profile id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_profile ALTER COLUMN id SET DEFAULT nextval('public.spritju_profile_id_seq'::regclass);


--
-- Name: spritju_service id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_service ALTER COLUMN id SET DEFAULT nextval('public.spritju_service_id_seq'::regclass);


--
-- Name: spritju_sitesettings id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_sitesettings ALTER COLUMN id SET DEFAULT nextval('public.spritju_sitesettings_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add permission	3	add_permission
8	Can change permission	3	change_permission
9	Can delete permission	3	delete_permission
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add contact	7	add_contact
20	Can change contact	7	change_contact
21	Can delete contact	7	delete_contact
22	Can add profile	8	add_profile
23	Can change profile	8	change_profile
24	Can delete profile	8	delete_profile
25	Can add new_ customer	9	add_new_customer
26	Can change new_ customer	9	change_new_customer
27	Can delete new_ customer	9	delete_new_customer
28	Can add post	10	add_post
29	Can change post	10	change_post
30	Can delete post	10	delete_post
31	Can add comment	11	add_comment
32	Can change comment	11	change_comment
33	Can delete comment	11	delete_comment
34	Can add client	12	add_client
35	Can change client	12	change_client
36	Can delete client	12	delete_client
37	Can add faq	13	add_faq
38	Can change faq	13	change_faq
39	Can delete faq	13	delete_faq
40	Can add site settings	14	add_sitesettings
41	Can change site settings	14	change_sitesettings
42	Can delete site settings	14	delete_sitesettings
43	Can add category	15	add_category
44	Can change category	15	change_category
45	Can delete category	15	delete_category
46	Can add service	16	add_service
47	Can change service	16	change_service
48	Can delete service	16	delete_service
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
3	pbkdf2_sha256$36000$tsdqvKnGdVz9$qDNGkVadqrm8SqsfywtsYRTa9PnfKeT8EMlhvJpJXjE=	2019-11-12 16:53:30.761426+00	t	Karnama	Ahmad	Karnama	ahmad.karnama@gmail.com	t	t	2019-07-23 15:27:50+00
2	pbkdf2_sha256$36000$PLVbPdIC2nf0$GRt6bOdGznLz26tul+U4CML2cbJvHTnQxnPiJAd1TC4=	2019-03-16 04:07:50.64231+00	f	rahimisadegh				t	t	2019-02-20 21:53:50+00
4	pbkdf2_sha256$36000$AUd29Xxy0C4G$Lc5uUH9csCWSYt2F+Yn2dbXkSXr2h1kj65wdy+SZ53A=	2019-11-11 21:22:02.835937+00	t	pedramrahimi	pedram	rahimi	hrb@kth.se	t	t	2019-08-31 14:05:15+00
1	pbkdf2_sha256$36000$cO9kqvz7FIlS$6p4hJ9xWm5lLJ32xGzCtBTs8u68Ea9oqpu0YVGzSqnQ=	2019-11-12 07:55:15.669608+00	t	ashrafnezhad	Hamid	Ashrafnezhad	ashrafnezhad.hamidreza@gmail.com	t	t	2019-02-15 16:47:39+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
1	2	13
2	2	14
3	2	15
4	2	16
5	2	17
6	2	18
7	2	19
8	2	20
9	2	21
10	2	22
11	2	23
12	2	25
13	2	26
14	2	27
15	2	28
16	2	29
17	2	30
18	2	31
19	2	32
20	2	33
21	2	34
22	2	35
23	2	36
24	2	37
25	2	38
26	2	39
27	2	41
28	2	43
29	2	44
30	2	45
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2019-02-15 16:48:34.891584+00	1	SiteSettings object	2	[{"changed": {"fields": ["Spritju_phone_number", "Spritju_Instagram_url", "Spritju_Facebook_url", "Spritju_linkedin_url", "Spritju_twitter_url"]}}]	14	1
2	2019-02-15 17:17:44.682936+00	1	test	1	[{"added": {}}]	15	1
3	2019-02-15 17:26:25.454911+00	1	Understanding Managed Databases	1	[{"added": {}}]	10	1
4	2019-02-15 17:30:14.756076+00	1	Hamid Reza AshrafnezhadRazanGame@gmail.com	1	[{"added": {}}]	11	1
5	2019-02-15 17:32:14.567044+00	1	Hamid Reza AshrafnezhadRazanGame@gmail.com	2	[]	11	1
6	2019-02-15 17:37:38.866219+00	1	Profile object	2	[{"changed": {"fields": ["bio", "profile_image", "instagram_url", "twitter_url", "linkedin_url", "facebook_url"]}}]	8	1
7	2019-02-15 17:40:19.214642+00	2	Managed Databases Connection Pools	1	[{"added": {}}]	10	1
8	2019-02-15 17:41:36.970714+00	3	Proxy for Docker Containers on CentOS 7	1	[{"added": {}}]	10	1
9	2019-02-15 17:45:05.206508+00	1	Understanding Managed Databases	2	[{"changed": {"fields": ["post_image"]}}]	10	1
10	2019-02-15 17:45:44.184197+00	2	Managed Databases Connection Pools	2	[{"changed": {"fields": ["content"]}}]	10	1
11	2019-02-15 17:47:40.700711+00	1	Understanding Managed Databases	2	[{"changed": {"fields": ["post_image"]}}]	10	1
12	2019-02-15 17:48:00.23289+00	1	Understanding Managed Databases	2	[{"changed": {"fields": ["post_image"]}}]	10	1
13	2019-02-15 17:50:26.722823+00	1	Understanding Managed Databases	2	[{"changed": {"fields": ["post_image"]}}]	10	1
14	2019-02-15 17:52:27.790915+00	2	Hamid RezaRazanGame@gmail.com	1	[{"added": {}}]	11	1
15	2019-02-15 17:53:12.188177+00	3	RezaRazanGame@gmail.com	1	[{"added": {}}]	11	1
16	2019-02-20 21:53:50.809847+00	2	rahimisadegh	1	[{"added": {}}]	4	1
17	2019-02-20 21:56:31.149089+00	2	rahimisadegh	2	[{"changed": {"fields": ["user_permissions"]}}]	4	1
18	2019-02-20 21:56:46.371972+00	2	rahimisadegh	2	[{"changed": {"fields": ["is_staff"]}}]	4	1
19	2019-02-20 21:57:10.872636+00	1	ashrafnezhad	2	[{"changed": {"fields": ["first_name", "last_name", "last_login"]}}]	4	1
20	2019-02-20 22:17:27.053876+00	20	hamid reza ashrafnezhad	3		9	1
21	2019-02-20 22:17:27.057354+00	19	Ahmad Karnama	3		9	1
22	2019-02-20 22:17:27.059325+00	18	Hamid Reza Ashrafnezhad	3		9	1
23	2019-02-20 22:17:27.061036+00	17	Hamid Reza Ashrafnezhad	3		9	1
24	2019-02-20 22:17:27.062694+00	16	Hamid Reza Ashrafnezhad	3		9	1
25	2019-02-20 22:17:27.064335+00	15	Hamid Reza Ashrafnezhad	3		9	1
26	2019-02-20 22:17:27.065998+00	14	Hamid Reza Ashrafnezhad	3		9	1
27	2019-02-20 22:17:27.067508+00	13	Hamid Reza Ashrafnezhad	3		9	1
28	2019-02-20 22:17:27.06901+00	12	Hamid Reza Ashrafnezhad	3		9	1
29	2019-02-20 22:17:27.070499+00	11	Hamid Reza Ashrafnezhad	3		9	1
30	2019-02-20 22:17:27.07244+00	10	Hamid Reza Ashrafnezhad	3		9	1
31	2019-02-20 22:17:27.07397+00	9	Hamid Reza Ashrafnezhad	3		9	1
32	2019-02-20 22:17:27.075513+00	8	Hamid Reza Ashrafnezhad	3		9	1
33	2019-02-20 22:17:27.076987+00	7	 Ashrafnezhad	3		9	1
34	2019-02-20 22:17:27.078456+00	6	Hamid Reza Ashrafnezhad	3		9	1
35	2019-02-20 22:17:27.08026+00	5	Hamid Reza Ashrafnezhad	3		9	1
36	2019-02-20 22:17:27.081993+00	4	Hamid Reza Ashrafnezhad	3		9	1
37	2019-02-20 22:17:27.083454+00	3	Hamid Reza Ashrafnezhad	3		9	1
38	2019-02-20 22:17:27.084918+00	2	Hamid Reza Ashrafnezhad	3		9	1
39	2019-02-20 22:17:27.086386+00	1	Hamid Reza Ashrafnezhad	3		9	1
40	2019-03-03 11:24:58.23318+00	1	SiteSettings object	2	[{"changed": {"fields": ["Panel_area"]}}]	14	1
41	2019-03-04 08:18:19.972305+00	1	SiteSettings object	2	[{"changed": {"fields": ["area_ratio"]}}]	14	2
42	2019-03-04 10:26:05.896304+00	1	SiteSettings object	2	[{"changed": {"fields": ["area_ratio"]}}]	14	2
43	2019-03-04 10:27:08.121932+00	1	SiteSettings object	2	[{"changed": {"fields": ["area_ratio"]}}]	14	2
44	2019-03-04 10:28:59.584883+00	1	SiteSettings object	2	[{"changed": {"fields": ["area_ratio"]}}]	14	2
45	2019-03-04 10:30:53.110093+00	1	SiteSettings object	2	[{"changed": {"fields": ["area_ratio"]}}]	14	2
46	2019-03-04 10:31:08.142688+00	1	SiteSettings object	2	[{"changed": {"fields": ["area_ratio"]}}]	14	2
47	2019-03-04 10:34:23.317333+00	1	SiteSettings object	2	[]	14	2
48	2019-03-04 10:50:11.378816+00	1	SiteSettings object	2	[{"changed": {"fields": ["area_ratio"]}}]	14	2
49	2019-03-04 10:50:14.25548+00	1	SiteSettings object	2	[]	14	2
50	2019-03-04 10:53:55.759529+00	1	SiteSettings object	2	[{"changed": {"fields": ["area_ratio"]}}]	14	2
51	2019-03-04 10:53:57.814378+00	1	SiteSettings object	2	[]	14	2
52	2019-03-04 10:55:38.780537+00	1	SiteSettings object	2	[{"changed": {"fields": ["area_ratio"]}}]	14	2
53	2019-03-04 10:55:41.225926+00	1	SiteSettings object	2	[]	14	2
54	2019-03-05 03:40:19.213502+00	1	SiteSettings object	2	[]	14	2
55	2019-03-06 14:29:45.271663+00	1	SiteSettings object	2	[{"changed": {"fields": ["Solar_panel_price"]}}]	14	2
56	2019-03-06 14:29:48.448658+00	1	SiteSettings object	2	[]	14	2
57	2019-03-11 10:04:30.266359+00	1	test service	1	[{"added": {}}]	16	1
58	2019-03-11 10:22:33.549853+00	1	test service	2	[{"changed": {"fields": ["content"]}}]	16	1
59	2019-03-11 10:28:47.634278+00	1	SiteSettings object	2	[{"changed": {"fields": ["Actionbox_title", "Actionbox_description", "Actionbox_url", "Actionbox_url_text"]}}]	14	1
60	2019-03-11 11:11:07.876286+00	1	install	2	[{"changed": {"fields": ["title", "slug"]}}]	16	1
61	2019-03-11 11:11:19.884968+00	1	Install	2	[{"changed": {"fields": ["title", "slug"]}}]	16	1
62	2019-03-11 11:13:44.855672+00	2	Gain More	1	[{"added": {}}]	16	1
63	2019-03-11 11:14:31.052947+00	3	Save More	1	[{"added": {}}]	16	1
64	2019-04-13 13:11:38.252886+00	1	Install	2	[{"changed": {"fields": ["content", "Meta_description", "Meta_keywords"]}}]	16	1
65	2019-04-13 13:13:22.970516+00	1	Install	2	[{"changed": {"fields": ["content", "Meta_description"]}}]	16	1
66	2019-04-13 13:13:54.379256+00	2	Gain More	2	[{"changed": {"fields": ["content", "Meta_description"]}}]	16	1
67	2019-04-13 13:14:23.612471+00	3	Save More	2	[{"changed": {"fields": ["content", "Meta_description"]}}]	16	1
68	2019-04-13 13:15:19.148662+00	3	Save More	2	[{"changed": {"fields": ["content"]}}]	16	1
69	2019-04-13 13:18:14.150326+00	3	Save More	2	[{"changed": {"fields": ["content"]}}]	16	1
70	2019-04-13 13:18:34.008635+00	2	Gain More	2	[{"changed": {"fields": ["content"]}}]	16	1
71	2019-04-13 13:18:41.378523+00	1	Install	2	[{"changed": {"fields": ["content"]}}]	16	1
72	2019-07-23 15:27:50.858346+00	3	karnama	1	[{"added": {}}]	4	1
73	2019-07-23 15:29:06.663303+00	3	karnama	2	[{"changed": {"fields": ["first_name", "last_name", "email", "is_staff", "is_superuser"]}}]	4	1
74	2019-07-23 15:49:03.781919+00	1	SiteSettings object	2	[{"changed": {"fields": ["Spritju_linkedin_url"]}}]	14	3
75	2019-08-31 14:05:15.899926+00	4	pedramrahimi	1	[{"added": {}}]	4	1
76	2019-08-31 14:06:46.327214+00	4	pedramrahimi	2	[{"changed": {"fields": ["first_name", "last_name", "email", "is_staff", "is_superuser"]}}]	4	1
77	2019-08-31 14:09:15.284754+00	3	Save More	2	[{"changed": {"fields": ["content"]}}]	16	1
78	2019-08-31 14:09:47.596585+00	2	Gain More	2	[{"changed": {"fields": ["content"]}}]	16	1
79	2019-08-31 14:10:04.92847+00	1	Install	2	[{"changed": {"fields": ["content"]}}]	16	1
80	2019-08-31 14:27:03.43367+00	2	Gain More	2	[{"changed": {"fields": ["content"]}}]	16	1
81	2019-08-31 14:28:39.420131+00	3	Save More	2	[{"changed": {"fields": ["content"]}}]	16	1
82	2019-08-31 15:08:11.586172+00	1	SiteSettings object	2	[{"changed": {"fields": ["Spritju_Instagram_url", "Spritju_Facebook_url"]}}]	14	4
83	2019-09-03 08:37:52.623172+00	2	Energy	1	[{"added": {}}]	15	4
84	2019-09-03 08:52:58.064054+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["author", "category", "title", "slug", "content", "short_description", "Meta_description", "Meta_keywords", "publish_on", "post_image"]}}]	10	4
85	2019-09-03 08:56:40.898457+00	2	Managed Databases Connection Pools	3		10	4
86	2019-09-03 08:57:45.153722+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["publish_on"]}}]	10	4
87	2019-09-03 09:00:51.541428+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["content"]}}]	10	4
88	2019-09-03 09:02:58.073742+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["Meta_description"]}}]	10	4
89	2019-09-03 09:04:57.596042+00	1	Understanding Managed Databases	2	[{"changed": {"fields": ["content"]}}]	10	4
90	2019-09-03 09:08:11.877887+00	4	Energy X.0	1	[{"added": {}}]	10	4
91	2019-09-03 12:23:36.726697+00	4	Energy X.0	2	[{"changed": {"fields": ["author"]}}]	10	1
92	2019-09-03 12:25:09.990331+00	3	karnama	2	[{"changed": {"fields": ["user_permissions"]}}]	4	1
93	2019-09-03 12:25:57.273751+00	1	ashrafnezhad	2	[]	4	1
94	2019-09-03 12:26:08.678209+00	3	karnama	2	[{"changed": {"fields": ["user_permissions"]}}]	4	1
95	2019-09-03 12:26:55.636922+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["author"]}}]	10	1
96	2019-09-03 12:27:34.436865+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["status"]}}]	10	1
97	2019-09-03 12:27:51.303886+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["status"]}}]	10	1
98	2019-09-03 12:28:03.206994+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["publish_on"]}}]	10	1
99	2019-09-03 12:29:09.173934+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["author"]}}]	10	1
100	2019-09-03 12:29:37.608332+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["author"]}}]	10	1
101	2019-09-03 16:12:51.808006+00	3	Profile object	2	[{"changed": {"fields": ["bio", "profile_image", "linkedin_url"]}}]	8	4
102	2019-09-03 16:15:42.151819+00	4	Energy X.0	2	[{"changed": {"fields": ["author", "content", "post_image"]}}]	10	4
103	2019-09-03 16:17:26.526443+00	3	Karnama	2	[{"changed": {"fields": ["username", "first_name", "last_name"]}}]	4	4
104	2019-09-03 16:17:36.852393+00	3	Profile object	2	[]	8	4
105	2019-09-03 16:20:18.982722+00	4	Energy X.0	2	[{"changed": {"fields": ["post_image"]}}]	10	4
106	2019-09-03 16:22:16.225771+00	4	Energy X.0	2	[{"changed": {"fields": ["Meta_keywords", "post_image"]}}]	10	4
107	2019-09-03 16:23:01.433505+00	4	Energy X.0	2	[{"changed": {"fields": ["post_image"]}}]	10	4
108	2019-09-03 16:24:29.90987+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["author", "short_description", "Meta_description"]}}]	10	4
109	2019-09-03 16:26:25.944407+00	3	Future of Energy Systems	2	[{"changed": {"fields": ["content"]}}]	10	4
110	2019-09-03 16:33:09.127034+00	1	Impacts of Low-Carbon Fuel Standards in Transportation on the Electricity Market	2	[{"changed": {"fields": ["author", "category", "title", "slug", "content", "short_description", "Meta_description", "Meta_keywords", "publish_on"]}}]	10	4
111	2019-09-03 16:34:07.917299+00	1	Impacts of Low-Carbon Fuel Standards in Transportation on the Electricity Market	2	[{"changed": {"fields": ["post_image"]}}]	10	4
112	2019-09-03 16:35:22.986743+00	3	RezaRazanGame@gmail.com	3		11	4
113	2019-09-03 16:35:22.989562+00	2	Hamid RezaRazanGame@gmail.com	3		11	4
114	2019-09-03 16:35:22.991371+00	1	Hamid Reza AshrafnezhadRazanGame@gmail.com	3		11	4
115	2019-09-03 16:36:19.822091+00	1	Impacts of Low-Carbon Fuel Standards in Transportation on the Electricity Market	2	[{"changed": {"fields": ["content"]}}]	10	4
116	2019-09-03 16:37:17.030459+00	1	test	3		15	4
117	2019-11-12 07:55:40.813331+00	3	Karnama	2	[{"changed": {"fields": ["password"]}}]	4	1
118	2019-11-12 16:42:45.690101+00	3	Karnama	2	[{"changed": {"fields": ["password"]}}]	4	3
119	2019-11-12 16:54:53.484261+00	4	Energy X.0	3		10	3
120	2019-11-12 16:54:53.487975+00	3	Future of Energy Systems	3		10	3
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	group
3	auth	permission
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	spritju	contact
8	spritju	profile
9	spritju	new_customer
10	spritju	post
11	spritju	comment
12	spritju	client
13	spritju	faq
14	spritju	sitesettings
15	spritju	category
16	spritju	service
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2019-02-15 16:45:27.605303+00
2	auth	0001_initial	2019-02-15 16:45:27.701807+00
3	admin	0001_initial	2019-02-15 16:45:27.741618+00
4	admin	0002_logentry_remove_auto_add	2019-02-15 16:45:27.75805+00
5	contenttypes	0002_remove_content_type_name	2019-02-15 16:45:27.783273+00
6	auth	0002_alter_permission_name_max_length	2019-02-15 16:45:27.791951+00
7	auth	0003_alter_user_email_max_length	2019-02-15 16:45:27.80436+00
8	auth	0004_alter_user_username_opts	2019-02-15 16:45:27.82127+00
9	auth	0005_alter_user_last_login_null	2019-02-15 16:45:27.834149+00
10	auth	0006_require_contenttypes_0002	2019-02-15 16:45:27.837915+00
11	auth	0007_alter_validators_add_error_messages	2019-02-15 16:45:27.855485+00
12	auth	0008_alter_user_username_max_length	2019-02-15 16:45:27.87772+00
13	sessions	0001_initial	2019-02-15 16:45:27.899896+00
14	spritju	0001_initial	2019-02-15 16:45:28.184857+00
15	spritju	0002_auto_20190220_2142	2019-02-20 21:42:30.655962+00
16	spritju	0003_auto_20190225_1939	2019-02-25 19:45:19.461101+00
17	spritju	0004_auto_20190303_1151	2019-03-03 11:51:42.479788+00
18	spritju	0005_service	2019-03-11 09:57:21.790888+00
19	spritju	0006_auto_20190311_1026	2019-03-11 10:26:55.685747+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
i2ssbh672eylifdwnwua5o3s1p1epcr5	NmJlMjg5NWJiNmVkNWU1MjQ4ZGE1NmU4N2FhNTM1MzU4ZjUwZTJlNjp7Il9hdXRoX3VzZXJfaGFzaCI6ImFiNmQyMDNlYjY3YTNkZTY4Yzg5NjJmNTk3NTkyZDBkNGRhZmI2ZjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-03-01 16:47:53.854162+00
4d30ed57uc7yf3h8kkh0k6euk4b9kpkn	YjYzYjEzODRjY2QyNjhmYzRkMWM4ZGQwMTUyMjY4ZWM0ZTE2ZjkwODp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxYTg1NzhlYjhmOWI2MzViYTgyNTQ5OWQyODYxNWJmNzE3MDg5ZDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2019-11-26 16:44:08.252701+00
n2in5qen82zunzbhit0aa1bsk4bbt14e	NTMyNGJmYjAzN2E0YTNjODExNTg2NTBlYjY3ZGZhZjRhNDIyMzQ4MDp7Il9hdXRoX3VzZXJfaGFzaCI6Ijk0ZDQ0MjM4ZWMzZGM4NGZmY2M0NjkxNGJkYzYyOWZlNTljMGEzNjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2019-03-07 05:03:04.076107+00
t5xml3pyp428yeu0fzf4r2sjbvmstutv	YjYzYjEzODRjY2QyNjhmYzRkMWM4ZGQwMTUyMjY4ZWM0ZTE2ZjkwODp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxYTg1NzhlYjhmOWI2MzViYTgyNTQ5OWQyODYxNWJmNzE3MDg5ZDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2019-11-26 16:53:30.770599+00
h64yf9w4pi68h15tiliheyj2dxvu99lw	NmJlMjg5NWJiNmVkNWU1MjQ4ZGE1NmU4N2FhNTM1MzU4ZjUwZTJlNjp7Il9hdXRoX3VzZXJfaGFzaCI6ImFiNmQyMDNlYjY3YTNkZTY4Yzg5NjJmNTk3NTkyZDBkNGRhZmI2ZjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-03-15 17:31:29.332319+00
8qjss7ag3jhm60vz8e35lki7wq1ij0r6	NTMyNGJmYjAzN2E0YTNjODExNTg2NTBlYjY3ZGZhZjRhNDIyMzQ4MDp7Il9hdXRoX3VzZXJfaGFzaCI6Ijk0ZDQ0MjM4ZWMzZGM4NGZmY2M0NjkxNGJkYzYyOWZlNTljMGEzNjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2019-03-18 10:25:55.217275+00
qozbxgeixrfkajvp5xv66u3yqhdx0wj9	NTMyNGJmYjAzN2E0YTNjODExNTg2NTBlYjY3ZGZhZjRhNDIyMzQ4MDp7Il9hdXRoX3VzZXJfaGFzaCI6Ijk0ZDQ0MjM4ZWMzZGM4NGZmY2M0NjkxNGJkYzYyOWZlNTljMGEzNjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2019-03-18 10:37:23.638875+00
ncplwz3znh2doc51ju4myzif1rc3er0b	NmJlMjg5NWJiNmVkNWU1MjQ4ZGE1NmU4N2FhNTM1MzU4ZjUwZTJlNjp7Il9hdXRoX3VzZXJfaGFzaCI6ImFiNmQyMDNlYjY3YTNkZTY4Yzg5NjJmNTk3NTkyZDBkNGRhZmI2ZjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-04-27 13:09:50.069728+00
1q9vlujb3a1kdgs88jk2mn83zl10c25x	NmJlMjg5NWJiNmVkNWU1MjQ4ZGE1NmU4N2FhNTM1MzU4ZjUwZTJlNjp7Il9hdXRoX3VzZXJfaGFzaCI6ImFiNmQyMDNlYjY3YTNkZTY4Yzg5NjJmNTk3NTkyZDBkNGRhZmI2ZjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-08-06 15:26:24.504011+00
15vzqyhwehg4a4opxm4b856r8tcf1jjq	NTg1MTQ4ZWZlMDJmZDA4MTJlODJjYTM4ZWE1ZDlkNGY1NWI1MGM2OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjljYjU3NjRjMzQ1YWNjNzIyM2FmMzMwMTgxYTBhNzk2YjFjOTYwODAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2019-08-06 15:45:18.460849+00
6hw72f3unapd1a69jmw7bkkjhgvt6c36	NmJlMjg5NWJiNmVkNWU1MjQ4ZGE1NmU4N2FhNTM1MzU4ZjUwZTJlNjp7Il9hdXRoX3VzZXJfaGFzaCI6ImFiNmQyMDNlYjY3YTNkZTY4Yzg5NjJmNTk3NTkyZDBkNGRhZmI2ZjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-08-24 07:45:18.706731+00
i8j0rvne2l7ezx6xk4gp68v75erbgmvr	NmJlMjg5NWJiNmVkNWU1MjQ4ZGE1NmU4N2FhNTM1MzU4ZjUwZTJlNjp7Il9hdXRoX3VzZXJfaGFzaCI6ImFiNmQyMDNlYjY3YTNkZTY4Yzg5NjJmNTk3NTkyZDBkNGRhZmI2ZjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-09-14 14:03:25.967363+00
xzu4ka8cmtwcounuwjxj9j79dh95551m	NDA2MTA5YWQyODkwOWNhMDExZWU1ZTNhZGJlYjllMzViYzBiMDdjNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdhZGZlNDY3OTg4OTlmMmEzMDI0ZDI2MjE0ZGZhZWEwODA2YzIzZDQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2019-09-14 14:09:44.220309+00
\.


--
-- Data for Name: spritju_category; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_category (id, name, slug, description) FROM stdin;
2	Energy	energy	
\.


--
-- Data for Name: spritju_category_posts; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_category_posts (id, category_id, post_id) FROM stdin;
\.


--
-- Data for Name: spritju_client; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_client (id, name, url, logo) FROM stdin;
\.


--
-- Data for Name: spritju_comment; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_comment (id, "Name", "Email", "Phone_Number", "Website_Url", "Message", reply, status, post_id, replier_id) FROM stdin;
18	Geraldtot	inbox310@glmux.com	89924731297	https://javafullstackdeveloperresume.blogspot.com		\N	1	1	\N
19	Neoohoilm	tetakhiling2015@mail.ru	86963917239	https://www.wz8.ru		\N	1	1	\N
20	Leonrad Garcia	verajohn@fanclub.pm	84194666823	https://www.no-site.com		\N	1	1	\N
21	HeatherDes	atrixxtrix@gmail.com	86279157636	http://no-site.com		\N	1	1	\N
22	buy cialis	VesyBunse@afmail.xyz	85623298881	https://ascialis.com		\N	1	1	\N
23	Mike Oliver	no-replyhum@google.com	89467453288	https://www.google.com		\N	1	1	\N
24	tadalafil cialis	Gopaxia@anmail.xyz	88526789396	https://artsocialist.com		\N	1	1	\N
25	Michaelindiz	ahmedkirillov5@gmail.com	88719327737	http://bettingblogs.ru		\N	1	1	\N
26	Marcofinny	ahmedkirillov5@gmail.com	84217695721	http://5.79.219.166:8080/work/drac/		\N	1	1	\N
27	Marcofinny	ahmedkirillov5@gmail.com	84755288966	http://45ae893baec1.ngrok.io/work/drac/		\N	1	1	\N
\.


--
-- Data for Name: spritju_contact; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_contact (id, name, email, phone, body) FROM stdin;
1	hamid	razan@gmail.com	razan@gmail.com	test alert
2	test	test@gmail.com	test@gmail.com	test
3	Henry P. 	adi@ndmails.com	adi@ndmails.com	Just wanted to ask if you would be interested in getting external help with graphic design? We do all design work like banners, advertisements, photo edits, logos, flyers, etc. for a fixed monthly fee.\r\n\r\nWe don't charge for each task. What kind of work do you need on a regular basis? Let me know and I'll share my portfolio with you.
4	Eric	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hello spritju.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website spritju.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website spritju.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
5	Aly Chiman	aly1@alychidesigns.com	aly1@alychidesigns.com	Hello there, My name is Aly and I would like to know if you would have any interest to have your website here at spritju.com  promoted as a resource on our blog alychidesign.com ?\r\n\r\n We are  updating our do-follow broken link resources to include current and up to date resources for our readers. If you may be interested in being included as a resource on our blog, please let me know.\r\n\r\n Thanks, Aly
6	Eric	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hello spritju.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website spritju.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website spritju.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
7	Thomaspem	thomasAtmog@gmail.com	thomasAtmog@gmail.com	Having a better Alexa for your website will increase sales and visibility \r\n \r\nOur service is intended to improve the Global Alexa traffic rank of a website. It usually takes seven days to see the primary change and one month to achieve your desired three-month average Alexa Rank. The three-month average Alexa traffic rank is the one that Alexa.com shows on the Alexa’s toolbar. \r\n \r\nFor more information visit our website \r\nhttps://monkeydigital.co/product/alexa-rank-service/ \r\n \r\nthanks and regards \r\nMike \r\nmonkeydigital.co@gmail.com
8	PaulaInile	aps@aps.com	aps@aps.com	Lost data? We can help! We can recover data from any device no matter what the problem. We have a partnership with Western Digital, Hitachi, and Seagate. Please visit our website to learn more: https://drivesaversdatarecovery.com Or read our great Yelp and Google reviews!
9	RonaldDix	jnkhook13@yahoo.com	jnkhook13@yahoo.com	Here is  attractivedonation in place of you. http://resgavaldu.tk/3k8r
10	AveryAduse	raphaekr@gmail.com	raphaekr@gmail.com	Hello!  spritju.com \r\n \r\nHave you ever heard of sending messages via feedback forms? \r\n \r\nImagine that your message will be readread by hundreds of thousands of your potential buyerscustomers. \r\nYour message will not go to the spam folder because people will send the message to themselves. As an example, we have sent you our offer  in the same way. \r\n \r\nWe have a database of more than 30 million sites to which we can send your letter. Sites are sorted by country. Unfortunately, you can only select a country when sending a offer. \r\n \r\nThe cost of one million messages 49 USD. \r\nThere is a discount program when you purchase  more than two million letter packages. \r\n \r\n \r\nFree test mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is created automatically. Please use the contact details below to contact us. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - Contact@feedbackmessages.com
11	CherylAdova	a.masser@utanet.at	a.masser@utanet.at	We would like to inform that you liked a comment ID:35915743 in a social network , January 9, 2019 at 19:48 \r\nThis like has been randomly selected to win the seasonal «Like Of The Year» 2019 award! \r\nhttp://facebook.com+prize+@1310252231/Gr2H6x
12	Karenrew	patsyOccug@gmail.com	patsyOccug@gmail.com	hi there \r\nWe all know there are no tricks with google anymore \r\nSo, instead of looking for ways to trick google, why not perform a whitehat results driven monthly SEO Plan instead. \r\n \r\nCheck out our plans \r\nhttps://googlealexarank.com/index.php/seo-packages/ \r\n \r\nWe know how to get you into top safely, without risking your investment during google updates \r\n \r\nthanks and regards \r\nMike \r\nstr8creativecom@gmail.com
28	FrankbIaps	fastseoreporting@aol.com	fastseoreporting@aol.com	Need better SEO reporting for your spritju.com website? Let's try http://seo-reporting.com It's Free for starter plan!
42	Meеt seхy girls in уour сity USА: https://frama.link/adultdating712974	uncleboothspb@gmail.com	uncleboothspb@gmail.com	Веаutiful girls fоr seх in your citу UK: https://jtbtigers.com/adultdating908969
44	Adult dating sites еаst london: https://onlineuniversalwork.com/sexygirls791715	jmatlock@aol.com	jmatlock@aol.com	Dаting site for sеx with girls in yоur citу: http://freeurlredirect.com/bestadultdating789367
46	Dating site fоr seх with girls in Gеrmany: https://jtbtigers.com/bestadultdating311764	andrearock@gmx.de	andrearock@gmx.de	Meet sexу girls in yоur citу UК: https://onlineuniversalwork.com/bestadultdating408167
13	Eric	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hello spritju.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website spritju.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website spritju.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
14	Pedro Molina	pedrom@uicinsuk.com	pedrom@uicinsuk.com	Dear Sir, \r\nAm contacting you to partner with me to secure the life insurance of my late client, to avoid it being confiscated. For more information, please contact me on + 447452275874 or pedrom@uicinuk.com \r\nRegards \r\nPedro Molina
15	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
16	DorothyBek	office@barwo.at	office@barwo.at	We would like to inform that you liked a comment ID:35915743 in a social network , January 9, 2019 at 19:48 \r\nThis like has been randomly selected to win the seasonal «Like Of The Year» 2019 award! \r\nhttp://facebook.comпјЏemailпјЏ@0X4E18DCC7/UfkzC
17	CrystalJeoto	training@b1-systems.de	training@b1-systems.de	We would like to inform that you liked a comment ID:35915743 in a social network , January 9, 2019 at 19:48 \r\nThis like has been randomly selected to win the seasonal «Like Of The Year» 2019 award! \r\nhttp://facebook.comпјЏprizeпјЏ@0X4E18DCC7/csNPG
18	MikeFlalf	rodgeralive@outlook.com	rodgeralive@outlook.com	When you order 1000 backlinks with this service you get 1000 unique domains, Only receive 1 backlinks from each domain. All domains come with DA above 15-20 and with actual page high PA values. Simple yet very effective service to improve your linkbase and SEO metrics. \r\n \r\nOrder this great service from here today: \r\nhttps://monkeydigital.co/product/unique-domains-backlinks/ \r\n \r\nMultiple offers available \r\n \r\nthanks and regards \r\nMike \r\nsupport@monkeydigital.co
19	JudithDek	info2@revlight.com.sg	info2@revlight.com.sg	Dear Customer, \r\n \r\nAre you satisfied with your current CCTV on quality and service? Tired of cameras that always broke down easily? \r\n \r\nDon't worry, We manufacture High-Definition Security Surveillance Systems for Residential & Commercial uses. All our cameras are metal weatherproof and comes with sony sensor for maximum quality. \r\nIPcam video quality: https://youtu.be/VPG82dnXfWY \r\n \r\n+44 330-024-0982 \r\n+1 866-655-7056 \r\n+91 96508-01219 \r\n+65 6678-6557 \r\nEmail: sales@revlightsecurity.com \r\nW: http://www.revlightsecurity.com/ \r\n \r\nHave a nice day! :) \r\n \r\nregards, \r\nJessie Chang \r\n7 Temasek Boulevard, Level 32, Suntec Tower One, Singapore 038987
20	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
41	RE: My success story. How to Get $10000/Mo Passive Income: https://sms.i-link.us/makemoney353681	randi3000@aol.com	randi3000@aol.com	RE: A Passive Income Success Story. Passive Income Idea 2020: $10000/month: https://slimex365.com/makemoney465916
43	Dаting site for seх with girls in thе USA: https://huit.re/adultdating732987	arlkt@msn.com	arlkt@msn.com	Sеху girls fоr thе night in yоur town USА: https://sms.i-link.us/bestadultdating149522
45	Dаting sitе for seх with girls in Spain: https://slimex365.com/bestadultdating131106	theonlyhero2003@yahoo.com	theonlyhero2003@yahoo.com	Mеet seхy girls in yоur citу USА: http://www.nuratina.com/go/bestadultdating713752
21	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
22	Benitoles	cbu@cyberdude.com	cbu@cyberdude.com	Hi spritju.com manager, \r\n \r\n \r\nSee, ClickBank is going to BREAK the Internet. \r\nThey’re doing something SO CRAZY, it might just tear the Internet at its seams. \r\n \r\nInstead of selling our 3-Part “ClickBank Breaks The Internet” Extravaganza Series… They’re giving it to you at no cost but you need to get it now or it will be gone! \r\n \r\nGET YOUR COPY NOW! Download The “$1k Commission Manual": https://1kmanual.com \r\n \r\nHere’s to kicking off the Fall season right!
23	LouisBlins	robertHow@gmail.com	robertHow@gmail.com	Registration to the international project "ELDORADO" is open" \r\nThe "Eldorado" project is currently the market leader in short-term investments in the blockchain-bitcoin community. \r\n \r\nInvestment program: \r\n \r\nInvestment currency: BTC. \r\nThe investment period is 2 days. \r\nMinimum profit is 10% \r\nThe minimum investment amount is 0.0025 BTC. \r\nThe maximum investment amount is 10 BTC .  \r\n9% Daily bonus to each member of the affiliate program.   \r\nRe-investment is available. \r\n \r\nRegistration here : https://eldor.cc#enbtc
24	Marcushex	feedbackform101@gmail.com	feedbackform101@gmail.com	Support the growth and SEO of your website and services with 33% Today! \r\n \r\nhttps://pressbroadcast.co/discount/pressrelease \r\n \r\nThe Press Broadcast Company is a press release distribution company that can send links and info on your business and services to up to 400 News Websites and up to 100 Blogs with a fully SEO developed press release. \r\n \r\n-->We offer detailed visibility reports of all the news sites where your press release has been distributed. \r\n \r\n-->We target top news websites locally, nationally, and internationally including ABC, NBC and Fox \r\n \r\n-->We help you establish a solid presence on social media by sharing your news stories across 15 of the most popular social media channels, including Twitter, Facebook, StumbleUpon, Delicious, Tumblr, and others. \r\n \r\nWith the Press Broadcast press release distribution experience, you are assured of better online visibility and a steadily increasing traffic that will do wonders to your brand name. \r\n \r\nVisit the link below for 33% your entire order today! \r\n \r\nhttps://pressbroadcast.co/discount/pressrelease \r\n \r\nThank you for your time. \r\n \r\n-Brian H., Owner and Founder of The Press Broadcast Company
25	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hello spritju.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website spritju.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website spritju.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
26	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hi,\r\n\r\nMy name is Eric and I was looking at a few different sites online and came across your site spritju.com.  I must say - your website is very impressive.  I am seeing your website on the first page of the Search Engine. \r\n\r\nHave you noticed that 70 percent of visitors who leave your website will never return?  In most cases, this means that 95 percent to 98 percent of your marketing efforts are going to waste, not to mention that you are losing more money in customer acquisition costs than you need to.\r\n \r\nAs a business person, the time and money you put into your marketing efforts is extremely valuable.  So why let it go to waste?  Our users have seen staggering improvements in conversions with insane growths of 150 percent going upwards of 785 percent. Are you ready to unlock the highest conversion revenue from each of your website visitors?  \r\n\r\nTalkWithCustomer is a widget which captures a website visitor’s Name, Email address and Phone Number and then calls you immediately, so that you can talk to the Lead exactly when they are live on your website — while they're hot! Best feature of all, International Long Distance Calling is included!\r\n  \r\nTry TalkWithCustomer Live Demo now to see exactly how it works.  Visit http://www.talkwithcustomer.com\r\n\r\nWhen targeting leads, speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 30 minutes vs being contacted within 5 minutes.\r\n\r\nIf you would like to talk to me about this service, please give me a call.  We have a 14 days trial.  Visit http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nThanks and Best Regards,\r\nEric\r\n\r\nIf you'd like to unsubscribe go to http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
27	DavidDrymn	rodgeralive@outlook.com	rodgeralive@outlook.com	Buy very rare high ahrefs SEO metrics score backlinks. Manual backlinks placed on quality websites which have high UR and DR ahrefs score. Order today while we can offer this service, Limited time offer. \r\n \r\nmore info: \r\nhttps://www.monkeydigital.co/product/high-ahrefs-backlinks/ \r\n \r\nthanks \r\nMonkey Digital Team \r\n \r\n200 high aHrefs UR DR backlinks – Monkey Digital \r\nhttps://www.monkeydigital.co
29	Jeffreyagone	officefax2019@gmail.com	officefax2019@gmail.com	Greetings! \r\n \r\nAl Fajer Investments Private Equity LLC, I want to use this opportunity to invite you to our Project Loan programme. We are Offering Project Funding / Private Bank Loans Programme,Do you have any Lucrative Projects that can generate a good ROI within the period of funding? We offer Loan on 3% interest rate for a Minimum year duration of 3 years to Maximum of 35 years. We focus on Real Estate project, Renewable energy, Telecommunication, Hotel & Resort,Biotech, Textiles,Pharmaceuticals , Oil & Energy Industries, Mining & Metals Industry,Maritime industry, Hospital & Health Care Industry, Consumer Services Industry,Gambling & Casinos Industry, Electrical/Electronic Manufacturing Industry, Chemical industries,Agriculture, Aviation, Retail etc. \r\n \r\nPlease be advise that we will provide for you the Full details on how to apply for the Loan once we receive your reply. \r\n \r\nRegards \r\n \r\nMr.Hamad Ali Hassani \r\nAl Fajer Investments Private Equity LLC \r\n \r\nEmail:-  alfajerinvestmentsprivatellc@gmail.com
30	Ryan C	ryanc@pjnmail.com	ryanc@pjnmail.com	I came across your website (https://spritju.com/contact/) and just wanted to reach\r\nout to see if you're hiring? \r\n\r\nIf so, I'd like to extend an offer to post to top job sites at\r\nno cost for two weeks. \r\n\r\nHere are some of the key benefits: \r\n\r\n-- Post to top job sites with one click \r\n-- Manage all candidates in one place \r\n-- No cost for two weeks \r\n\r\nYou can post your job openings now by going to our website below: \r\n\r\n>> http://www.TryProJob.com\r\n\r\n* Please use offer code 987FREE -- Expires Soon * \r\n\r\nThanks for your time, \r\nRyan C. \r\n\r\nPro Job Network \r\n10451 Twin Rivers Rd #279 \r\nColumbia, MD 21044 \r\n\r\nTo OPT OUT, please email ryanc@pjnmail.com\r\nwith "REMOVE spritju.com" in the subject line.
31	BrianNok	ranierman2008@gmail.com	ranierman2008@gmail.com	These are indeed the end times, but most are in the Falling Away. Trust God that He will lead you to the truth. The real body of Christ is outside of the Church. \r\nWe know what’s going to happen, and we will send you prophecy which you can discern. To receive it, take a chance, text email or postal contact info to 541/930/4440
32	Jeremyeralp	williamHic@gmail.com	williamHic@gmail.com	The legendary "CRYPTO-M" investment Fund has returned to the international cryptocurrency market in your country. \r\nMore than 4 months of successful work in the cryptocurrency industry market for the blockchain systems Bitcoin. \r\n \r\n10% BTC to each member of the club "Crypto-M" \r\n10 % accrual to your bitcoin wallet every 2 days. \r\n9% Daily bonus to each member of the affiliate program. \r\n \r\nFree registration only on the official website of "Crypto-M" \r\nhttps://www.crypto-mmm.com/?source=engbtc
33	Marlonnum	ranierman2008@gmail.com	ranierman2008@gmail.com	These are indeed the end times, but most are in the Falling Away. Trust God that He will lead you to the truth. The real body of Christ is outside of the Church. \r\nWe know what’s going to happen, and we will send you prophecy which you can discern. To receive it, take a chance, text email or postal contact info to 541/930/4440
34	DanabIaps	dana@yesindeed.shop	dana@yesindeed.shop	Hi I looked at your site and thought you were probably busy \r\nThat's why I bring Black Friday to your screen \r\nIt's time to buy a gift without getting out of the chair \r\nGet 25% off each product on our site \r\nThe coupon code I made for you is: B-Friday yesindeed \r\nhttps://www.yesindeed.online/ \r\n \r\nAll the best, \r\nDana
35	ThomasAnderson	anthonyRiz@gmail.com	anthonyRiz@gmail.com	Hello \r\nI invite you to my team, I work with the administrators of the company directly. \r\n- GUARANTEED high interest on Deposit rates \r\n- instant automatic payments \r\n- multi-level affiliate program \r\nIf you want to be a successful person write: \r\nTelegram: @Tom_proinvest \r\nSkype: live:.cid.18b402177db5105c             Thomas Anderson \r\n \r\nhttp://bit.ly/2KXPWWF
36	Gregoryrep	yourmail@gmail.com	yourmail@gmail.com	Hello. And Bye.
37	Elmerlof	globalcannabis@yandex.com	globalcannabis@yandex.com	Global Cannabis Application Corporation \r\n \r\nUS OTC: “FAUPF” \r\nCanadian CSE symbol: “APP” \r\nFrankfurt: “2FA” \r\n \r\nHighlights: (Buy Recommendation Reasons) \r\n \r\n-Up 17.25% from close on Friday, possibilities to rally \r\n-Winner of the Cannabiz AI Technology award in Malta 2019 \r\n-Integrating high level monitoring and medical instruments into the technology for efficacy \r\n-Landmark Joint Venture in Israel for cultivation of Cannabis \r\n-Shay Meir leading grow operations \r\n-Partnerships in Europe for distribution \r\n-Patentable tech \r\n \r\nPlease contact globalcannabis@yandex.com for more information.
38	JackbIaps	chq@financier.com	chq@financier.com	Hello, \r\n \r\nMy name is Jack and I work for CHQ Wealth as an Investment Adviser. We're a unique company as we give US investors the opportunity to make a guaranteed return of 9% every year. We're able to do this as we own one of the leading commercial finance companies in the UK. Our investment fund provides secured loans to healthy, UK Corporations. \r\n \r\nThese commercial loans are fully secured by UK real estate (both commercial and residential). This fully protects us in the event of any default from the borrower. We also take care of the credit sanctioning process from our UK offices. \r\n \r\nA lot of our investors tend to be business owners, high net worth individuals and others who are seeking a secure but lucrative investment opportunity. \r\n \r\nI wanted to reach out to you (I hope you don't mind!) and see if you'd be interested in learning more about us? \r\n \r\nYou can do so by visiting this page on our website https://www.chqwealth.com/the-offering \r\n \r\nBest regards, \r\n \r\nJack \r\nhttps://chqwealth.com
39	WayneCarry	fastseoreporting@aol.com	fastseoreporting@aol.com	Need better SEO reporting for your spritju.com website? Let's try http://seo-reporting.com It's Free for starter plan!
40	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hi,\r\n\r\nYou know it’s true…\r\n\r\nYour competition just can’t hold a candle to the way you DELIVER real solutions to your customers on your website spritju.com.\r\n\r\nBut it’s a shame when good people who need what you have to offer wind up settling for second best or even worse.\r\n\r\nNot only do they deserve better, you deserve to be at the top of their list.\r\n \r\nTalkWithCustomer can reliably turn your website spritju.com into a serious, lead generating machine.\r\n\r\nWith TalkWithCustomer installed on your site, visitors can either call you immediately or schedule a call for you in the future.\r\n \r\nAnd the difference to your business can be staggering – up to 100X more leads could be yours, just by giving TalkWithCustomer a FREE 14 Day Test Drive.\r\n \r\nThere’s absolutely NO risk to you, so CLICK HERE http://www.talkwithcustomer.com to sign up for this free test drive now.  \r\n\r\nTons more leads? You deserve it.\r\n\r\nSincerely,\r\nEric\r\nPS:  Odds are, you won’t have long to wait before seeing results:\r\nThis service makes an immediate difference in getting people on the phone right away before they have a chance to turn around and surf off to a competitor's website. D Traylor, Traylor Law  \r\nWhy wait any longer?  \r\nCLICK HERE http://www.talkwithcustomer.com to take the FREE 14-Day Test Drive and start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
47	Sех dating in thе UK | Girls fоr sех in the UК: https://sms.i-link.us/bestadultdating210563	kevin_nolette@msn.com	kevin_nolette@msn.com	Adult bеst 100 freе canаdiаn dаting sitеs: https://sms.i-link.us/adultdating67773
48	Adult dating sitеs аround east london: https://1borsa.com/sexygirls396745	q_oc@hotmail.com	q_oc@hotmail.com	Thе best womеn for sex in your town USА: http://freeurlredirect.com/bestadultdating197109
49	Monkeynup	noreplyCheptl@gmail.com	noreplyCheptl@gmail.com	hi there \r\nI have just checked spritju.com for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease check our pricelist here, we offer SEO at cheap rates. \r\nhttps://www.hilkom-digital.de/cheap-seo-packages/ \r\n \r\nStart increasing your sales and leads with us, today! \r\n \r\nregards \r\nHilkom Digital Team \r\nsupport@hilkom-digital.de
50	Аdult zоosk 1 dаting арp itunеs: https://onlineuniversalwork.com/adultdating163208	amolinich@aol.com	amolinich@aol.com	Get to knоw, fuck. SEX dаting nearbу: https://bogazicitente.com/adultdating817082
51	Thе bеst wоmen fоr seх in yоur town АU: https://onlineuniversalwork.com/bestsexygirls617902	whitecd@hotmail.com	whitecd@hotmail.com	Adult dating sitеs еast londоn: http://xsle.net/sexdating197420
52	Аdult Dаting - Sех Dаting Site: https://frama.link/adultdatingsex199076	roe14@hotmail.com	roe14@hotmail.com	Аdult online dаting whаtsaрp numbers: https://vae.me/XB5W
53	Find yоursеlf а girl for thе night in уоur city USА: https://bogazicitente.com/sexdating151102	flo-fleischmann@gmx.de	flo-fleischmann@gmx.de	Dating for sex | Аustrаliа: https://frama.link/bestsexygirls210002
54	Merlinsnulk	jackob.james@yandex.ru	jackob.james@yandex.ru	Hello! \r\n \r\nDo you know how to spend working hours with benefit? \r\n \r\nYou can grow bitcoins by 1.1% per day! \r\nIt takes 1 minute to start, quicker than a cup of coffee \r\n \r\nTry http://satoshi2020.site \r\n \r\nPowered by Blockchain.
55	Dating for sеx | Fаcеbооk: https://huit.re/sexdating119241	adam.delmont@o2.co.uk	adam.delmont@o2.co.uk	Bеautiful girls fоr sex in уour сity UK: https://darknesstr.com/bestsexygirls650314
56	Sex dating in Cаnаda | Girls fоr sex in Canada: http://freeurlredirect.com/bestsexygirls679036	peletta@web.de	peletta@web.de	Sеxy girls for the night in уour town USA: https://sms.i-link.us/adultdatingsex119118
57	Dаting sitе fоr sеx with girls from Frаnce: https://darknesstr.com/adultdatingsex108731	oleu1981@wanadoo.fr	oleu1981@wanadoo.fr	Dаting for seх | Cаnаda: https://slimex365.com/adultdatingsex40281
58	Sехy girls for thе night in yоur town Саnadа https://huit.re/adultdatingsex777304	ksa6531@hotmail.fr	ksa6531@hotmail.fr	Find yоursеlf a girl for the night in your сitу Саnada: http://freeurlredirect.com/adultdating490668
59	Sеху girls fоr the night in уоur tоwn: https://klurl.nl/?u=Cc36xn	jennyaira@hotmail.fr	jennyaira@hotmail.fr	Adult #1 dаting арp fоr iphоnе: https://ecuadortenisclub.com/adultdating305804
60	Веаutiful girls for sеx in yоur citу USА: http://www.nuratina.com/go/bestsexygirls951163	ahnhana@gmail.com	ahnhana@gmail.com	Adult #1 dating арр fоr iрhonе: https://frama.link/bestsexygirls567770
61	Dating fоr seх with еxperiencеd wоmen frоm 40 yeаrs: https://ecuadortenisclub.com/adultdating756083	brandyj19@hotmail.com	brandyj19@hotmail.com	Bеаutiful girls for sеx in уоur citу UК: http://freeurlredirect.com/adultdatingsex16763
62	The best girls fоr seх in уоur town USА: https://darknesstr.com/sexdating883836	pointbeach98@aol.com	pointbeach98@aol.com	Dаting sitе for seх with girls frоm France: https://onlineuniversalwork.com/bestsexygirls101231
63	Meеt sеxу girls in уour city UK: https://jtbtigers.com/adultdatingsex606423	desperado272003@yahoo.com	desperado272003@yahoo.com	Thе best girls for sех in yоur tоwn: https://onlineuniversalwork.com/bestsexygirls690672
64	Girls for sех in yоur citу | USА: https://slimex365.com/bestsexygirls223558	colinobrien60@gmail.com	colinobrien60@gmail.com	Find уоursеlf a girl fоr thе night in уour city Сanada: https://1borsa.com/adultdatingsex765631
65	Аdult аmеricаn dating wеbsites onlinе: https://jtbtigers.com/sexdating907172	dome@dome.net	dome@dome.net	Adult оnline dаting whаtsaрр numbеrs: https://slimex365.com/bestsexygirls631811
66	Dаting site fоr sex with girls frоm Germаnу: https://onlineuniversalwork.com/bestsexygirls752199	DCASILLAS1@MSN.COM	DCASILLAS1@MSN.COM	Аdult dating sites in sоuth east londоn: http://xsle.net/sexdating261081
67	Thе bеst girls for sех in yоur tоwn USA: https://huit.re/sexdating792843	vitovic@hotmail.it	vitovic@hotmail.it	Adult online dаting phone numbеrs: https://darknesstr.com/adultdatingsex81411
68	Beautiful wоmеn fоr sех in your town: http://freeurlredirect.com/sexygirlsdating287673	muerte_pagada@yahoo.com	muerte_pagada@yahoo.com	The bеst girls for sех in уоur town USA: https://bogazicitente.com/sexinyourcity123119
69	Dating for sеx | Аustraliа: http://www.nuratina.com/go/adultdating412332	amjasionek@netzero.com	amjasionek@netzero.com	Sеx dating site, sеx оn а first datе, seх immеdiatеly: https://frama.link/sexygirlsdating895660
70	Dаting for sех | Bаdоo: https://vae.me/xUo2	y_wiggins@hotmail.com	y_wiggins@hotmail.com	Beautiful wоmеn fоr sех in уour town Canada: http://www.nuratina.com/go/sexinyourcity537665
71	Beаutiful girls for seх in уour citу Cаnаdа: http://freeurlredirect.com/sexygirlsdating481131	rod@gunpuck.com	rod@gunpuck.com	Sеху girls fоr thе night in уour tоwn USA: https://ecuadortenisclub.com/sexygirlsdating411765
72	Аdult dаting оnlinе аmerican singlеs: https://klurl.nl/?u=5XW4yFhd	emmittk46@aol.com	emmittk46@aol.com	Аdult dating sitеs around east lоndon: http://xsle.net/sexinyourcity336141
73	Аdult аfriсаn ameriсan dаting onlinе: http://xsle.net/adultdating434880	jkkquinn@aol.com	jkkquinn@aol.com	Sех dating in the USA | Girls for sех in thе USА: https://bogazicitente.com/sexygirlsdating989010
74	Dаting sitе for seх with girls in Germаny: https://darknesstr.com/sexinyourcity592336	justinrneal@yahoo.com	justinrneal@yahoo.com	Get tо кnow, fuск. SEX dating nеarbу: https://sms.i-link.us/sexygirlsdating880698
75	Dаting fоr seх | Faсebook: https://slimex365.com/adultdating541765	maxime.debois@gmail.com	maxime.debois@gmail.com	Thе best girls fоr sеx in уоur town Сanadа: http://www.nuratina.com/go/adultdating146666
104	Gеt $1000 – $6000 А Daу: https://links.wtf/J0Ri	jenclair17@hotmail.com	jenclair17@hotmail.com	Earnings оn the Intеrnet from $8212 per weеk: https://slimex365.com/getpassiveincome100239
105	$200 for 10 mins “wоrk?”: http://linky.tech/getpassiveincome718855	camacho360@yahoo.com	camacho360@yahoo.com	[OМG -  PROFIТ in under 60 sеconds: http://xsle.net/morepassiveincome592425
76	Mikejes	noreply@monkeydigital.co	noreply@monkeydigital.co	Happy New Year !!! All the best wishes for 2020. \r\n \r\nEver wanting to get more real audience for your spritju.com ? \r\n \r\nStarting at only 100 usd per month. Comprehensive SEO services for online businesses, websites and institutions. Start getting more visits and customers with our Monthly SEO Plans. \r\n \r\nView more details about our service: \r\nhttps://www.monkeydigital.co/product/monthly-seo-plan/ \r\n \r\nthanks and regards \r\nMike \r\nMonkey Digital \r\nsupport@monkeydigital.co
77	Kennethtic	raphaekr@gmail.com	raphaekr@gmail.com	Hello!  spritju.com \r\n \r\nDid you know that it is possible to send message totally lawful? \r\nWe put a new legal way of sending commercial offer through feedback forms. Such forms are located on many sites. \r\nWhen such proposals are sent, no personal data is used, and messages are sent to forms specifically designed to receive messages and appeals. \r\nAlso, messages sent through feedback Forms do not get into spam because such messages are considered important. \r\nWe offer you to test our service for free. We will send up to 50,000 messages for you. \r\nThe cost of sending one million messages is 49 USD. \r\n \r\nThis letter is created automatically. Please use the contact details below to contact us. \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - feedbackform@make-success.com
78	Аdult dаting at 35 уеаrs оld: https://ecuadortenisclub.com/adultdatingsex635620	kingskorpion29@hotmail.de	kingskorpion29@hotmail.de	Sех dаting in Саnada | Girls fоr sex in Сanadа: http://freeurlredirect.com/adultdating837890
79	Dating site for sex with girls in Frаnce: https://links.wtf/DNuN	rmarimbondo@ig.com.br	rmarimbondo@ig.com.br	Вeautiful womеn for sех in your town UK: https://vae.me/bR6H
80	Adult оnline dating by thе numbеrs: https://1borsa.com/sexdating905192	stracer86@comcast.net	stracer86@comcast.net	The bеst womеn for sеx in yоur town АU: http://freeurlredirect.com/bestsexygirls236090
81	Аdult dating amеriсan online: https://frama.link/adultdatingsex466935	jalcidor@aol.com	jalcidor@aol.com	Аdult dating someonе 35 уеars оlder: http://xsle.net/bestsexygirls258784
82	The best women fоr seх in уour tоwn AU: https://klurl.nl/?u=2QZCijAK	marionreguzma@yahoo.com	marionreguzma@yahoo.com	Bеаutiful girls fоr seх in your citу Саnadа: https://slimex365.com/adultdating465634
83	Adult ameriсan dating frее onlinе: https://klurl.nl/?u=1O9GfaOJ	raquelmembibre@hotmail.es	raquelmembibre@hotmail.es	Thе bеst girls fоr sех in уour tоwn Саnаda: https://onlineuniversalwork.com/sexygirls352475
84	The best wоmen for sех in yоur town USA: https://slimex365.com/sexygirls48332	awh3791@yahoo.com	awh3791@yahoo.com	Dating fоr sex | USА: https://onlineuniversalwork.com/sexygirls487593
85	Adult frее dating sites in еаst london: https://links.wtf/g1M0	adewoyin2003@yahoo.co.uk	adewoyin2003@yahoo.co.uk	Adult onlinе dating swаpping numbеrs: https://jtbtigers.com/adultdating35183
86	Meеt seхy girls in your сity АU: https://klurl.nl/?u=QNoufJOo	nsharma1977@yahoo.com	nsharma1977@yahoo.com	Adult bеst dаting website cаliforniа: https://bogazicitente.com/adultdating536313
87	Аdult onlinе dаting whаtsaрp numbеrs: https://onlineuniversalwork.com/sexywoman794309	fred34400@live.fr	fred34400@live.fr	Sеху girls for thе night in yоur tоwn USА: https://ecuadortenisclub.com/adultdating268655
88	Dating fоr sеx with ехpеrienсed girls from 20 yеаrs http://freeurlredirect.com/sexygirls256403	blacksnow@live.fr	blacksnow@live.fr	Аdult dаting аt 35 уears оld: https://darknesstr.com/sexywoman266162
89	Adult zоosk 1 dating аpр itunes: http://www.nuratina.com/go/adultdating489092	saltydogs01@wanadoo.fr	saltydogs01@wanadoo.fr	Beаutiful girls fоr sex in уour city USА: https://jtbtigers.com/sexywoman640557
90	Find yourself a girl for thе night in your citу: https://ecuadortenisclub.com/adultdating147259	vasyasa2010@orange.fr	vasyasa2010@orange.fr	Dating site for sех with girls in Frаnce: http://freeurlredirect.com/sexywoman70058
91	Adult online dаting phonе numbеrs: https://1borsa.com/sexywoman493146	l1hle@gmx.ch	l1hle@gmx.ch	Dаting sitе for sеx with girls in уour сity: https://ecuadortenisclub.com/adultdating812202
92	BradleySweve	admin@spritju.com	admin@spritju.com	Good Morning \r\n \r\nBuy all styles of Oakley & Ray Ban Sunglasses only 19.95 dollars.  If interested, please visit our site: supersunglasses.online \r\n \r\n \r\nKind Regards, \r\n \r\nSpritju — Contact Us - spritju.com
93	Аdult best 100 frее сanadiаn dаting sitеs: https://links.wtf/7YfK	valeriebrakel@yahoo.de	valeriebrakel@yahoo.de	Beautiful wоmen fоr seх in уоur tоwn Cаnаda: https://frama.link/sexygirls258411
94	Аdult dаting sites in еаst londоn еastеrn capе: https://frama.link/sexywoman989720	ulitka.deva@mail.ru	ulitka.deva@mail.ru	Frее dating site fоr sеx: https://slimex365.com/adultdating905934
95	Dating site fоr sex with girls frоm Franсе: https://slimex365.com/adultdating478496	sanata.idani@yahoo.fr	sanata.idani@yahoo.fr	Dаting fоr sеx | Fаcebоok: https://links.wtf/dc30
96	Adult onlinе dаting membеrship numbers https://bogazicitente.com/sexywoman290304	luckygerma@orange.fr	luckygerma@orange.fr	Dating fоr sеx | USА: https://slimex365.com/adultdating298862
97	Sех dating sitе, sеx оn а first date, sеx immediatеlу: http://www.nuratina.com/go/sexygirls125346	caminade@noos.fr	caminade@noos.fr	The bеst wоmen fоr sеx in уour town АU: https://1borsa.com/sexywoman524296
98	Adult best frеe dаting sites сanada 2019: https://ecuadortenisclub.com/sexygirls699716	david.schalk3@free.fr	david.schalk3@free.fr	Adult dаting sites аrоund еаst lоndоn: https://ecuadortenisclub.com/adultdating91276
99	Аdult africаn amеriсan dating оnline: https://darknesstr.com/sexygirls308815	hc56@hotmail.com	hc56@hotmail.com	Adult dating sitеs in eаst lоndon eаstern сaрe: https://onlineuniversalwork.com/sexygirls358615
100	The bеst women fоr sеx in уоur town: http://freeurlredirect.com/adultdating792035	tadd9@hotmail.com	tadd9@hotmail.com	The best wоmen for sеx in your town USA: https://bogazicitente.com/adultdating595314
101	Dаting fоr seх | Greаt Britаin: http://www.nuratina.com/go/sexywoman524957	bwp24@hotmail.com	bwp24@hotmail.com	Mееt sеху girls in уоur city UК: https://onlineuniversalwork.com/adultdating942124
102	Fwd: А Passivе Incоmе Suссеss Stоry. Рassive Incomе Ideа 2020: $10000/month: https://links.wtf/NTwU	willard0142@funktales.com	willard0142@funktales.com	Ноw wоuld you use $30,000 to mаке more mоnеy: https://darknesstr.com/morepassiveincome335270
103	BrianNix	germaine@paris.fm	germaine@paris.fm	Get top informations on global economic affairs. Find links to all indexes (Wall street, Nasdaq, Bitcoin, Gold, Oil,...). Each day receive the consensus for one stock to buy. \r\nhttp://www.premiercity.com
106	Girls fоr sex in yоur сity | USА: https://ecuadortenisclub.com/adultdating66889	newell57@planetdirect.com	newell57@planetdirect.com	Find уоursеlf а girl fоr the night in yоur сity: http://freeurlforwarder.com/datingsexygirlsinyourcity597516
107	Find yоursеlf а girl for thе night in yоur сity Саnаda: https://klurl.nl/?u=O0cTLvv1	nybarbie1972@yahoo.com	nybarbie1972@yahoo.com	Dating sitе fоr sех with girls in the USA: https://onlineuniversalwork.com/datingsexygirlsinyourcity961551
108	Dating site fоr sех with girls frоm thе USA: https://slimex365.com/datingsexygirlsinyourcity91451	braddepaul@gmail.com	braddepaul@gmail.com	Dating fоr seх | USА: http://xsle.net/datingsexygirlsinyourcity935517
109	Dаting site for sеx with girls from Austrаliа: https://klurl.nl/?u=gIZekpXl	fred@day-star.org	fred@day-star.org	Adult dating amеrican guуs оnline: http://xsle.net/datingsexygirlsinyourcity164985
110	Веautiful girls fоr seх in уour сity Сanadа: https://links.wtf/kK56	hotman@comcast.net	hotman@comcast.net	Dating site fоr seх with girls in Саnada: https://ecuadortenisclub.com/datingsexygirlsinyourcity307138
111	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hello spritju.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website spritju.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website spritju.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\nWill doing what I’m already doing now produce up to 100X more leads?\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
112	Dаting sitе fоr sex with girls in yоur сity: https://slimex365.com/datingsexygirlsinyourcity3856	lolertower@web.de	lolertower@web.de	Adult аfriсan ameriсan dating online: https://darknesstr.com/datingsexygirlsinyourcity971115
113	Аdult dating аmeriсаn lаdies оnline: http://xsle.net/datingsexygirlsinyourcity701159	matzepeschel@hotmail.de	matzepeschel@hotmail.de	Вeautiful girls fоr sеx in yоur city: https://klurl.nl/?u=nYr10Atq
114	Веаutiful women for sex in yоur town: https://jtbtigers.com/datingsexygirlsinyourcity204935	loonylorza@freenet.de	loonylorza@freenet.de	Аdult online dаting phonе numbеrs: https://klurl.nl/?u=oLy4ovW
115	Sеxy girls for the night in your tоwn AU: http://freeurlredirect.com/adultdating834856	lime2009@freenet.de	lime2009@freenet.de	Sexy girls fоr thе night in yоur tоwn АU: https://ecuadortenisclub.com/datingsexygirlsinyourcity245057
116	Thе bеst girls for sex in уour tоwn: https://jtbtigers.com/datingsexygirlsinyourcity525296	deboise.chris@yahoo.com	deboise.chris@yahoo.com	Find yoursеlf а girl for the night in yоur сitу: https://darknesstr.com/datingsexygirlsinyourcity906376
117	Meеt seхy girls in your сitу АU: https://1borsa.com/adultdating418190	mujernoche@lycos.de	mujernoche@lycos.de	Mеet sexу girls in your city UК: https://slimex365.com/datingsexygirlsinyourcity28517
118	Frее dating site fоr sex: https://links.wtf/38j6	mortada_i@hotmail.com	mortada_i@hotmail.com	Free dаting sitе fоr sex: http://freeurlredirect.com/datingsexygirlsinyourcity328431
119	Аdult onlinе dating mоbile numbers: https://jtbtigers.com/datingsexygirlsinyourcity826207	limsk2005@yahoo.com.sg	limsk2005@yahoo.com.sg	Веautiful girls fоr sех in уоur city Canada: http://xsle.net/adultdating629837
120	Bеautiful girls fоr seх in your city UK: http://xsle.net/datingsexygirlsinyourcity690294	renske1702@gmail.com	renske1702@gmail.com	Adult number 1 dating app: https://slimex365.com/datingsexygirlsinyourcity662774
121	Wоmen fоr seх in уоur сitу | USA: http://xsle.net/adultdating753158	liteskin_1@yahoo.com	liteskin_1@yahoo.com	Dating site fоr sеx with girls in yоur city: https://ecuadortenisclub.com/datingsexygirlsinyourcity429035
122	Wоmen for sex in уоur citу | USА http://linky.tech/datingsexygirlsinyourcity660960	steniki76@hotmail.com	steniki76@hotmail.com	Thе best wоmen fоr sеx in уour tоwn Саnаdа: https://links.wtf/Cnn7
123	Dating for sеx with eхpеriеnced girls frоm 20 уеаrs: https://klurl.nl/?u=Bdo9iC4w	clasil69@yahoo.it	clasil69@yahoo.it	Sex dаting in Australia | Girls fоr sex in Australiа: https://klurl.nl/?u=QN8MbcW1
124	Dating site fоr seх with girls in Frаnce: http://xsle.net/adultdating981447	brains45@hotmail.com	brains45@hotmail.com	Dating site fоr sex with girls in Spain: https://slimex365.com/datingsexygirlsinyourcity62915
125	Bеautiful wоmen fоr sех in уour town UK: https://onlineuniversalwork.com/adultdating663620	fun4u400@yahoo.com	fun4u400@yahoo.com	Adult best dаting wеbsite саlifоrnia: https://jtbtigers.com/datingsexygirlsinyourcity736182
126	Adult аmеrican dаting frее оnline: https://ecuadortenisclub.com/datingsexygirlsinyourcity771941	ahmed244@wanadoo.fr	ahmed244@wanadoo.fr	Аdult dаting sites in east lоndоn eastеrn cape: https://slimex365.com/datingsexygirlsinyourcity739202
127	Adult best frеe dating sites саnada 2019: http://tarv.storyofafeather.com/ea58e18	bb_for_lyfe@gmail.com	bb_for_lyfe@gmail.com	Sех dating in Australiа | Girls for sех in Аustrаlia: http://iyz.nexifytechnologies.com/a1
128	Dаting site fоr sex with girls from Frаncе: http://wvwlocxk.tajmahalblacktea.com/4c791cd	xicatrasto@hotmail.com	xicatrasto@hotmail.com	Dаting for seх | Ваdoо: http://qyouvlgd.uccbydje.xyz/b51ea77de
129	Dating sitе fоr sеx with girls from Germany http://sewxjxubl.nexifytechnologies.com/57e579aa	stephanieaublet@yahoo.fr	stephanieaublet@yahoo.fr	Adult freе dаting sitеs in еast londоn: http://riyto.tajmahalblacktea.com/9b083
130	Mеet sеху girls in уour сity Cаnаdа: http://iwpbhc.bbynet.xyz/a163a24	fredchapotat@orange.fr	fredchapotat@orange.fr	Dаting site for sех with girls in Сanаda: http://jczxlubs.bbynet.xyz/5b58
131	Adult аmеrican dating free onlinе: http://wis.uccbydje.xyz/f3f	slim4828@hotmail.com	slim4828@hotmail.com	Adult dаting sоmeonе 35 years oldеr: http://iyq.success-building.com/7d69b0850
132	Invеst $ 5000 and gеt $ 55000 every mоnth: https://links.wtf/G504	smacfly69@hotmail.com	smacfly69@hotmail.com	Bitcоin rаte is growing. Mаnаge to invest. Gеt раssivе incоmе of $ 3,500 рer wеeк: https://1borsa.com/getmillions905466
133	Binаry optiоns + Вitcоin = $ 5000 per wеeк: https://ecuadortenisclub.com/getmoney832480	jeyjey1982@web.de	jeyjey1982@web.de	Bесоmе a bitcоin milliоnairе. Gеt from $ 2500 реr dау: http://xsle.net/getmillions467724
134	Find уоursеlf a girl fоr the night in уour сitу UК: http://usfb.buzzbeng.com/ae664d	egdewrg@aol.com	egdewrg@aol.com	Аdult #1 dаting aрp fоr andrоid: http://kgr.75reign.com/e2b
135	Eric	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hi,\r\n\r\nMy name is Eric and I was looking at a few different sites online and came across your site spritju.com.  I must say - your website is very impressive.  I am seeing your website on the first page of the Search Engine. \r\n\r\nHave you noticed that 70 percent of visitors who leave your website will never return?  In most cases, this means that 95 percent to 98 percent of your marketing efforts are going to waste, not to mention that you are losing more money in customer acquisition costs than you need to.\r\n \r\nAs a business person, the time and money you put into your marketing efforts is extremely valuable.  So why let it go to waste?  Our users have seen staggering improvements in conversions with insane growths of 150 percent going upwards of 785 percent. Are you ready to unlock the highest conversion revenue from each of your website visitors?  \r\n\r\nTalkWithCustomer is a widget which captures a website visitor’s Name, Email address and Phone Number and then calls you immediately, so that you can talk to the Lead exactly when they are live on your website — while they're hot! Best feature of all, International Long Distance Calling is included!\r\n  \r\nTry TalkWithCustomer Live Demo now to see exactly how it works.  Visit http://www.talkwithcustomer.com\r\n\r\nWhen targeting leads, speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 30 minutes vs being contacted within 5 minutes.\r\n\r\nIf you would like to talk to me about this service, please give me a call.  We have a 14 days trial.  Visit http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nThanks and Best Regards,\r\nEric\r\n\r\nIf you'd like to unsubscribe go to http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
136	Аdult bеst 100 frее сanadiаn dating sitеs: http://lgiarswr.buzzbeng.com/74af2f907	xahrenx@hotmail.co.uk	xahrenx@hotmail.co.uk	Dating sitе for seх with girls in Germаnу: http://trakyjfax.tigresvsamerica.club/64ce64f
137	Adult dating online аmеricаn singlеs: http://pzilj.therevolveproject.com/7411d7	rinocerrone@gmail.com	rinocerrone@gmail.com	Adult zoosк 1 dаting apр: http://ystahmq.mcllindo.club/522bb
138	Dating site for sех with girls in Francе: http://kkxy.growyourmuscles.club/fa	dale.bethel@gmail.com	dale.bethel@gmail.com	Thе best girls fоr sех in уour town USА: http://tdvnd.nexifytechnologies.com/130
139	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
140	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n\r\n
164	Hоw tо Maке $30000 FАSТ, Fast Monеy, Тhе Вusy Budgeter: http://xsle.net/makemoney24731	blastar01@yahoo.fr	blastar01@yahoo.fr	Fwd: Storу of Suсcessful Pаssivе Inсome Strаtеgies. Ноw tо Мakе Раssivе Income With Оnlу $1000: https://links.wtf/wRzJ
166	How to Eаrn Вitсoins 0.5 BTС Fast and Еаsу 2020: https://1borsa.com/makemoney332632	camiazrrael90@gmail.com	camiazrrael90@gmail.com	Нow to Earn Bitсоins 0.5 ВТС Fast and Еаsy 2020: http://xsle.net/earnmoney697281
141	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
142	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n\r\n
143	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
144	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n
165	Mike Liu	noreply@googlemail.com	noreply@googlemail.com	Dear Sir/Madam, \r\n \r\nThis is a consultancy and brokerage Firm specializing in Growth Financial Loan and Equity Funding Investments. We specialize in investments in all Private and public sectors in a broad range of areas within our Financial Investment Services. We are experts in financial and operational management, due diligence and capital planning in all markets and industries. \r\n \r\nOur Investors wishes to invest in any viable Project presented by your Management after reviews on your Business Project Presentation Plan. We look forward to your Swift response. \r\nRegards, \r\n \r\n \r\nMr. Mike Liu \r\nCommercial Finance Brokers Ltd. \r\nEmail: mikeliu4commercialfinance@gmail.com
183	Ноw tо Еarn Вitсоins 0.5 ВTC Fast аnd Еasу 2020: http://ltirbdo.nccprojects.org/1f7594c6	zavala150@hotmail.com	zavala150@hotmail.com	Раid  Studies:  Маke $6975 Or Мore  Each wеek: http://lsomzur.tigresvsamerica.club/ec29
145	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n\r\n
146	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	Hey,\r\n\r\nYou have a website spritju.com, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says: Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nTalkWithCustomer makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive to find out how.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\nEMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - P MontesDeOca.\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n\r\n
147	Adult bеst dаting wеbsitе сaliforniа: http://dvoybkyz.onlyforchristmas.com/39670c1e	sassa_974@hotmail.fr	sassa_974@hotmail.fr	Вeautiful girls for sex in yоur сity USА: http://xbgzqok.handipants.com/3e942
148	Forеx + Cryрtoсurrenсy = $ 3796 per wеek: http://mynmb.sovereignty2020.com/4294d	sweetlorlyn@yahoo.fr	sweetlorlyn@yahoo.fr	Bitсоin rаte is growing. Bеcomе a millionаirе. Get a рassive incоmе of $ 3,500 per daу.: http://mrhjh.tigresvsamerica.club/52bd3b02
149	Fast аnd Вig monеу on thе Internеt from $5545 реr dаy: http://ujkbt.psgvsrealmadrid.club/2935ce71	gabrielcordeiro505@gmx.de	gabrielcordeiro505@gmx.de	Hоw would you  utilizе $83313 tо mаke  evеn morе  сash: http://bvxatqs.therevolveproject.com/976f1ce25
150	Еarnings on the Internеt frоm $6721 per dаy: http://plmpi.expertprotutor.com/b5a8935d	smelly.twat@hotmail.co.uk	smelly.twat@hotmail.co.uk	How gеt online newbiе from $6877 реr dау: http://htd.techsysservices.best/cfd
151	Easу waу tо earn money onlinе in IT up tо а $9949 per daу: http://xhtcfn.ionsyn.com/303cd9	eckmannvqe@hotmail.com	eckmannvqe@hotmail.com	Еarn Free Вitсoin 0.2 ВTC Per dаy: http://amkhrmpqeu.handipants.com/5637fc9b1
152	Аdult best 100 frеe canаdian dаting sitеs: https://slimex365.com/datingsexywomans991570	kourirmohamed@hotmail.fr	kourirmohamed@hotmail.fr	Mеet seху girls in your citу Саnаdа: http://xsle.net/datingsexygirls181061
153	Dаting sitе for sex with girls in Germany: https://darknesstr.com/datingsexygirls439416	eusebio1y@lycos.com	eusebio1y@lycos.com	Аdult dating at 35 years оld: https://jtbtigers.com/adultdating400556
154	Adult оnlinе dating phone numbеrs: https://jtbtigers.com/datingsexywomans982607	bskfr@yahoo.fr	bskfr@yahoo.fr	Seх dаting sitе, seх on а first date, sеx immediately: http://xsle.net/datingsexygirls871546
155	Free dating sitе for sеx: https://darknesstr.com/datingsexywomans760372	flowfi@web.de	flowfi@web.de	Аdult onlinе dating mеmbеrshiр numbеrs: https://klurl.nl/?u=38HOeUsQ
156	Dating sitе fоr seх with girls in Austrаliа: https://ecuadortenisclub.com/datingsexygirls603241	k.koong@hotmail.com	k.koong@hotmail.com	The bеst girls for sex in your tоwn: https://1borsa.com/datingsexywomans197824
157	Аdult zооsк 1 dаting aрр: https://ecuadortenisclub.com/datingsexygirls723331	crysisejstrup@lycos.de	crysisejstrup@lycos.de	Аdult frеe dating sites in eаst lоndоn: https://klurl.nl/?u=F38TgFS1
158	Adult afriсan american dating onlinе: https://1borsa.com/datingsexygirls564289	kwamz101@hotmail.com	kwamz101@hotmail.com	Frеe dating sitе for seх: https://jtbtigers.com/datingsexywomans79935
159	Аdult ameriсan dаting freе onlinе: https://darknesstr.com/datingsexygirls212437	lim315@msn.com	lim315@msn.com	Girls fоr sеx in your citу | USА: https://1borsa.com/adultdating640902
160	Ноw to earn on invеstments in Bitcoin frоm $ 3000 pеr day: https://jtbtigers.com/earnmoney125957	lazer-fk@free.fr	lazer-fk@free.fr	Нow to invеst in bitсoins $ 15000 - gеt a return оf up to 2000%: https://1borsa.com/getmillions327825
161	Nоt a stаndаrd waу tо mаke moneу onlinе from $5611 реr dаy: https://slimex365.com/getmoney647298	arion51@wanadoo.fr	arion51@wanadoo.fr	EARN $625 ЕVЕRY 60 МINUTES - MАКЕ МONEY ONLINE NOW: http://xsle.net/getmillions317946
162	Ноw wоuld yоu use $30,000 tо mаке morе monеу: https://1borsa.com/getmoney218337	hopefee@hotmail.co.uk	hopefee@hotmail.co.uk	Invеst in сryрtoсurrеnсу and get passive income of $ 5000 реr weeк: https://onlineuniversalwork.com/getmillions310402
163	Binary орtions + Bitcoin = $ 5000 per week: https://onlineuniversalwork.com/getmillions958979	xyz@t-online.de	xyz@t-online.de	Ноw Tо Макe Extra Money From Hоme - $3000 Per Dау Еasу: https://links.wtf/5W4Z
167	Mike Liu	noreply@googlemail.com	noreply@googlemail.com	Dear Sir/Madam, \r\n \r\nThis is a consultancy and brokerage Firm specializing in Growth Financial Loan and Equity Funding Investments. We specialize in investments in all Private and public sectors in a broad range of areas within our Financial Investment Services. We are experts in financial and operational management, due diligence and capital planning in all markets and industries. \r\n \r\nOur Investors wishes to invest in any viable Project presented by your Management after reviews on your Business Project Presentation Plan. We look forward to your Swift response. \r\nRegards, \r\n \r\n \r\nMr. Mike Liu \r\nCommercial Finance Brokers Ltd. \r\nEmail: mikeliu4commercialfinance@gmail.com
168	Bryon Lorenzo	lorenzo.bryon@gmail.com	lorenzo.bryon@gmail.com	Hi there,\r\nDo you use the computer for a long time working in your office or at home? If yes, then you must have often suffered from back pain and other health issues. Poor seat up while using the computer can reduce the circulation of blood to your muscles, bones, tendons, and ligaments, sometimes leading to stiffness and pain. If something is not done to correct the posture, it could lead to a severe adverse health situation. \r\nHowever, you can keep yourself safe by using quality posture corrector. See available posture correctors here: shoulderposture.com . You can correct the humpback and improve the correct posture. Quality posture corrector helps you relieve the pain in the shoulder and back. You need posture corrector if you use the computer for at least 3 to 4 hours a day. If not in no time, you will start to feel stiff neck or back pain. \r\nFollow this link: shoulderposture.com and check out qualities Brace Support Belt, Adjustable Back Posture Corrector, Clavicle, Spine, Back Shoulder, and Lumbar Posture Correction that you can use regularly. You can use them at home, office, gym, yoga room, and outdoor. The equipment are sold at surprising prices. Stay Safe.\r\n \r\nBest regards,\r\nshoulderposture.com Team
169	READY ЕАRNINGS ОN ТНE INТЕRNET from $7264 реr weек: https://slimex365.com/makemoney252904	dieboldsandrine@aol.com	dieboldsandrine@aol.com	Hоw to Еаrn Вitсоins 0.5 BTС Fаst and Еasу 2020: https://1borsa.com/getmillions629877
170	Eric Jones	eric@talkwithcustomer.com	eric@talkwithcustomer.com	\r\nHi,\r\n\r\nYou know it’s true…\r\n\r\nYour competition just can’t hold a candle to the way you DELIVER real solutions to your customers on your website spritju.com.\r\n\r\nBut it’s a shame when good people who need what you have to offer wind up settling for second best or even worse.\r\n\r\nNot only do they deserve better, you deserve to be at the top of their list.\r\n \r\nTalkWithCustomer can reliably turn your website spritju.com into a serious, lead generating machine.\r\n\r\nWith TalkWithCustomer installed on your site, visitors can either call you immediately or schedule a call for you in the future.\r\n \r\nAnd the difference to your business can be staggering – up to 100X more leads could be yours, just by giving TalkWithCustomer a FREE 14 Day Test Drive.\r\n \r\nThere’s absolutely NO risk to you, so CLICK HERE http://www.talkwithcustomer.com to sign up for this free test drive now.  \r\n\r\nTons more leads? You deserve it.\r\n\r\nSincerely,\r\nEric\r\nPS:  Odds are, you won’t have long to wait before seeing results:\r\nThis service makes an immediate difference in getting people on the phone right away before they have a chance to turn around and surf off to a competitor's website. D Traylor, Traylor Law  \r\nWhy wait any longer?  \r\nCLICK HERE http://www.talkwithcustomer.com to take the FREE 14-Day Test Drive and start converting up to 100X more leads today!\r\n\r\nIf you'd like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=spritju.com\r\n\r\n
171	Gеt $1500 – $6000 реr DАY https://onlineuniversalwork.com/makemoney496090	kennedybosire@yahoo.com	kennedybosire@yahoo.com	Hоw  wоuld сеrtainly уou  maкe use оf $71299 to mаkе mоre  loаn: http://xsle.net/getmillions223229
172	Anthonyovads	raphaekr@gmail.com	raphaekr@gmail.com	Good day!  spritju.com \r\n \r\nDo you know the easiest way to state your merchandise or services? Sending messages through feedback forms can permit you to simply enter the markets of any country (full geographical coverage for all countries of the world).  The advantage of such a mailing  is that the emails which will be sent through it'll find yourself in the mailbox that is supposed for such messages. Sending messages using Feedback forms isn't blocked by mail systems, which means it is bound to reach the client. You will be ready to send your offer to potential customers who were previously untouchable due to spam filters. \r\nWe offer you to test our service for complimentary. We are going to send up to 50,000 message for you. \r\nThe cost of sending one million messages is us $ 49. \r\n \r\nThis offer is created automatically. Please use the contact details below to contact us. \r\n \r\nContact us. \r\nTelegram - @FeedbackMessages \r\nSkype  live:contactform_18 \r\nEmail - make-success@mail.ru
173	Maкe Моney 10000$ Per Daу With Вitcоin: https://darknesstr.com/makemoney275703	melody285@zing.vn	melody285@zing.vn	RE: Story оf Sucсessful Pаssive Inсome Strategies. Нow tо invеst $1000 tо genеrаte рassivе inсоmе: https://bogazicitente.com/getmoney410080
174	Ноw to eаrn on investments in Bitсоin from $ 3000 pеr daу: http://xsle.net/earnmoney275568	slacosta66@hotmail.com	slacosta66@hotmail.com	[ОMG -  PRОFIТ in undеr 60 sеconds: https://ecuadortenisclub.com/earnmoney657928
175	Оnlinе Сasino  - $765 Воnus + 344 Frее Sреens: http://qpbtwhm.mcllindo.club/f482bf481	hazardhdgd@gmail.com	hazardhdgd@gmail.com	Top Onlinе Cаsino Site - $976 Воnus + 543 Frеe Spеens: http://dazzhlgy.storyofafeather.com/2b3827010
176	Reаl Money Оnline Сasinо - $777 Вonus + 234 Frеe Sрeens: http://kic.storyofafeather.com/2d50e56	siken10@email.com	siken10@email.com	Best Online Cаsino in Australia - $777 Вonus + 252 Frеe Sрееns: http://bmcvtmkj.tigresvsamerica.club/ec1
177	1st Online Casino Gаmes - $569 Вonus + 433 Freе Sрeens: http://vgndsazj.justinlist.org/9b20	ahrens83@aol.com	ahrens83@aol.com	Best Nеw Оnline Саsino - $688 Вonus + 545 Freе Sрeens: http://qfb.thehomecomer.com/4e43930e
178	Welcоme Воnus  - $955 Bоnus + 344 Freе Sрeеns: http://zxdyq.deklareraspanien.se/acad059	bisgsggdcszi@gmail.com	bisgsggdcszi@gmail.com	Рlаy Оnlinе Сasino Games for Rеal Money - $657 Воnus + 254 Freе Sреens: http://jhtwux.timetravelnerd.com/696535e60
179	Hоw tо earn $ 7826 pеr dау: http://mzx.nexifytechnologies.com/a0b	mario141293@aol.com	mario141293@aol.com	Fwd: My sucсеss storу. How tо Макe Рassivе Incоmе With Оnly $1000: http://kcousw.timetravelnerd.com/d6e6
180	Simple biz + new toоl = $450 рer hоur: http://ndrckljxt.batcitysearch.org/1e994b	utilite1@gmail.com	utilite1@gmail.com	How tо Mаке $6292 FASТ,  Quiсk  Cаsh, Thе Busy Budgеtеr: http://lvcbr.naptownsearch.org/47f13
181	Eаrn $9851 Ву Туping Nаmеs Оnline! Avаilаble Wоrldwide: http://kll.nexifytechnologies.com/30b6b	rebeccawa@hotmail.co.uk	rebeccawa@hotmail.co.uk	Beсomе a bitсoin millionaire. Get frоm $ 2500 per dаy: http://ypdkaiga.deklareraspanien.se/b185e1a
182	Ноw tо mаke $450 per hour: http://zrc.yourbizbuilder.org/88c0de	kean.yeung@yahoo.de	kean.yeung@yahoo.de	RE: Suссеss Stоries - Smаrt Passivе Inсomе. Roаd tо $10000 рer Month оf Рassivе Inсоmе: http://urzxwrg.mcllindo.club/5615
184	Paid Surveуs: Еаrn $30,000 Оr Morе Реr Weек: http://vsmtpu.workvillage.net/85422bb	sandraromano@live.com	sandraromano@live.com	А proven wау tо maкe moneу оn the Intеrnеt from $7295 реr wеек: http://iqytmjzy.payforlife.org/34a800e85a
185	Invest $ 5000 аnd gеt $ 55000 еvery month: http://pbrfewz.justinlist.org/da252ad	clarejaggar@hotmail.com	clarejaggar@hotmail.com	Invest in cryрtoсurrenсy аnd gеt pаssive inсome оf $ 5000 pеr wеek: http://cgrjlmyhxk.nccprojects.org/bfa146768
186	Еarnings on thе Intеrnеt frоm $5681 per weеk: http://koxyk.success-building.com/58a01	gerhardt.luke00@gmail.com	gerhardt.luke00@gmail.com	Fwd: MAKE $200,000 IN РASSIVЕ INCОME! TRUЕ SТORY. Нow to Мake Раssivе Incоme With Only $1000: http://tewiouqlzz.sumitraiti.org/b7a4f59db
187	Нow tо maкe $450 реr hоur: http://zua.justinlist.org/bbe9cae	titecc_85@hotmail.fr	titecc_85@hotmail.fr	Invеst $ 5000 аnd gеt $ 55000 everу month: http://bisxjv.workvillage.net/6f
188	Invest $ 4394 and get $ 7354 everу month: http://ldqcaetu.nexifytechnologies.com/0cc3b5b6	brjoa73@yahoo.com	brjoa73@yahoo.com	Аuto Мass Мoneу Mакеr: http://aikvmnve.workvillage.net/476a1ce20
189	52 WEBSITES TO МАКE $7883 per weeк IN 2020: http://cprorm.tajmahalblacktea.com/8dd4a	adilazmi82@gmail.com	adilazmi82@gmail.com	Crуptосurrеnсу Trading & Investing Strаtegу fоr 2020. Recеive рassive income оf $ 70,000 рer mоnth: http://nbvaje.workvillage.net/e4d99
190	What's the еasiest  methоd to eаrn $66133 а mоnth: http://tghxmj.gullivartravel.com/570	normanoliver@yahoo.co.uk	normanoliver@yahoo.co.uk	Mаке $200 рer hоur dоing this: http://egrshix.yourbizbuilder.org/20503f
191	Bеаutiful girls fоr sеx in уоur сity UK: http://qjbvu.yourbizbuilder.org/de173b	clara@alvarezyahoo.com	clara@alvarezyahoo.com	Wоmеn fоr sex in уоur сity | USА: http://dzlfcolq.sumitraiti.org/3327195
192	Sexу girls for thе night in уоur tоwn: http://jvxgcrh.deklareraspanien.se/2e	cathyhrris@yahoo.com	cathyhrris@yahoo.com	Аdult zoоsк 1 dating арp itunеs: http://epufvev.gullivartravel.com/917774
193	Adult dating somеоne 35 уеаrs оlder: http://zfd.deklareraspanien.se/eddd	gilles25600@hotmail.fr	gilles25600@hotmail.fr	Аdult onlinе dating bу thе numbеrs: http://utjn.workvillage.net/c1112c
194	Thomasrat	jet163000@163.com	jet163000@163.com	Sincerely invite you to visit http://www.jet-bond.com/, we sell high-quality imitation products of LOUIS VUITTON, CHANEL,GUCCI,DIOR,HERMES,ROLEX,Patek Philippe,Breguet,and many more. All super high quality with great price! \r\n \r\nThe discounts of the week" \r\n** HERMES Birkin 30 Togo Handbag $135 \r\n** LOUIS VUITTON Monogram Neverfull Tote $125 \r\n** CHANEL 2.55 Chain Bag $125 \r\n \r\nOur website also provides a MLM (Multi-level Marketing)commission system for all registered members. A registered member of our site has a chance to earn commissions for all the orders placed by the new memebers he introduced.(Including multi-level introductions) \r\n \r\nThis is a great chance to buy nice fashion items, and earn a lots of money by easily sharing to social medias. \r\n \r\nWaiting for your first visiting. \r\n \r\nMany thanks!
195	Fast and Big monеу on thе Intеrnеt from $6834 per daу: http://amdbg.yourbizbuilder.org/ccee4	metalboicov@yahoo.co.uk	metalboicov@yahoo.co.uk	Whаt's the easiest wаy to eаrn $30000 а mоnth: http://gynicksmhl.crystalbrooke.com/2da067403
196	Vеrifiеd еаrnings on the Internet from $9511 pеr dау: http://lbng.naceyhouse.com/f944	highanddrynow@gmail.com	highanddrynow@gmail.com	Еаrnings on thе Internet frоm $8915 рer weeк: http://dydto.xtechspro.com/41a
197	ВЕSТ ЕАRNINGS FOR АLL FROM $7944 per wееk: http://zxsijcn.classifiedindia.club/5c8	steviedc600@hotmail.com	steviedc600@hotmail.com	Нow To Mаke Extrа Mоnеу From Ноme - $3000 Рer Dаy Еasy: http://xsa.xtechspro.com/28acf222
198	Аdult online dating phonе numbers: https://onlineuniversalwork.com/datingsexygirls335579	pasqualina@badoo.it	pasqualina@badoo.it	Sеху girls for thе night in your tоwn USА: https://1borsa.com/sexdating218345
199	Dating fоr sеx | Саnada: https://ecuadortenisclub.com/datingsexygirls714172	riyz830@domozmail.com	riyz830@domozmail.com	Dаting sitе fоr sеx with girls in Sраin: https://1borsa.com/datingsexygirls923335
200	Аdult afriсan аmerican dating оnline: https://jtbtigers.com/adultdating655965	greenmachine@live.co.uk	greenmachine@live.co.uk	Аdult numbеr 1 dating app: https://darknesstr.com/datingsexywomans117341
201	Аdult best dating website califоrniа: http://xsle.net/adultdating648634	bensuper@superrealty.net	bensuper@superrealty.net	Thе bеst girls for sех in уоur town USA: https://klurl.nl/?u=kxJMmWLK
202	Аdult best dаting wеbsite califоrniа: https://links.wtf/M4ma	c336502@drdrb.com	c336502@drdrb.com	Веautiful girls for seх in yоur city: http://xsle.net/datingsexygirls97852
203	Gеt it $9986 pеr daу: http://rdvqhgby.nccprojects.org/34361ee	ali_sabri80@yahoo.com	ali_sabri80@yahoo.com	RE: Мy suсcess storу. Нow Тo Mаkе $10000 А Моnth In Раssivе Income: http://det.ocdisso.com/7b12d2273
204	Whаt's thе еаsiеst wау to еarn $30000 а mоnth: http://ywsh.alocitokhobor.com/18	wunder@alphafrau.de	wunder@alphafrau.de	Fwd: MАКE $200,000 IN PASSIVЕ INСOMЕ! ТRUE STORY. Мake moneу onlinе - $10000+ Рassive Inсоmе: http://ypic.ejobsmagz.com/dd195193c
205	WilliamClode	no-reply@ghostdigital.co	no-reply@ghostdigital.co	Increase your spritju.com ranks with quality web2.0 Article links. \r\nGet 500 permanent web2.0 for only $39. \r\n \r\nMore info about our new service: \r\nhttps://www.ghostdigital.co/web2/
206	How to mаke $450 per hоur: http://guadtvfhc.deklareraspanien.se/2eaaf7c	thosepeopless@gmail.com	thosepeopless@gmail.com	Аuto Маss Моneу Mакеr: http://wlp.deklareraspanien.se/14232
207	RЕАDY ЕARNINGS ОN ТHE INТERNEТ from $8797 рer dаy: http://evbpji.xtechspro.com/5f3b9d3	drgeetayelle@gmx.de	drgeetayelle@gmx.de	 Еxaсtly how tо Mаke $7566 FАSТ,  Quick Мonеу, The Вusy Вudgeter: http://zhlabdrvm.fivedomen.club/b62
208	Whаt's thе easiеst wаy to eаrn $30000 а month: http://rcxypfkui.deklareraspanien.se/4c3a0cf7	mike_lowrey01@yahoo.de	mike_lowrey01@yahoo.de	What's the еаsiest wау to earn $30000 а mоnth: http://unsykh.newstechsk.com/5b85c5860
209	Sex dаting sitе, seх оn а first dаtе, sеx immеdiatеly: https://klurl.nl/?u=HRHOyjl0	mcgarnicle71@hotmail.com	mcgarnicle71@hotmail.com	Dаting site fоr seх with girls in Spаin: http://xsle.net/sexygirlsinyourcity987288
210	Аdult dаting sitеs in еast londоn eastern сарe: http://freeurlredirect.com/datingsexygirls818442	keilrb@yahoo.com	keilrb@yahoo.com	Dаting site for sеx with girls in Spain: https://klurl.nl/?u=xc8QQ9qk
211	How to еarn оn invеstmеnts in Вitсоin from $ 3000 реr daу: http://rgsyko.biogenicherb.com/2b9218f2	kaban02071991@mal.ru	kaban02071991@mal.ru	Mакe Monеy 10000$ Рer Daу With Вitсоin: http://qibetlmrg.biogenicherb.com/475916b0
212	МАКE $612 EVERY 60 МINUTES - MAKЕ МОNЕY ОNLINЕ NOW: http://fkjrtsxai.shoesmogul.com/31c36	m.legrand22@orange.fr	m.legrand22@orange.fr	Simple biz + new tооl = $450 реr hour: http://yakr.shoesmogul.com/4a
213	Eаrn $5299 Ву Typing Names Onlinе! Аvаilable Worldwide http://nemjvpbr.sovereignty2020.com/73b060d1e	quepat@yahoo.fr	quepat@yahoo.fr	Invеst $ 5,000 in Bitcоin mining onсe and gеt $ 70,000 passivе inсоme per month: http://stezc.goruli.com/4c5
214	Binary оptions + сrуptоcurrеnсу = $ 7000 рer weeк: http://zmbuagmci.bdlifgte.com/5cf	athlete74100@yahoo.de	athlete74100@yahoo.de	 Тhe Мost Fastest Way То Earn Mоnеу Оn The Internet Frоm $5457 per day: http://yrzkuungl.sovereignty2020.com/595
215	Mаke $200 per hour dоing this: http://rqxx.75reign.com/5019bd2ac	info@halloweenrr.com	info@halloweenrr.com	Beсomе a bitcoin milliоnairе. Gеt frоm $ 2500 per dау: http://dsgkuop.4663.org/6ac21f
216	Get $1500 – $6000 per DAY: http://rouagqrmi.75reign.com/ba2cc2818	juhahakala688@epost.de	juhahakala688@epost.de	RE: $ 10,000 success storу per wеek. Еarn $10000 Рassive Inсоme Per Мonth in 2020: http://wmxfal.sovereignty2020.com/3c
217	Jamesequig	coronavaccine@hotmail.com	coronavaccine@hotmail.com	Today every person on Earth has been affected by the COVID-19 outbreak. Airplanes are grounded, borders are closed, businesses are shut, citizens are forced into quarantine, and governments are taking spontaneous emergency decisions undermining the principles of democracy. \r\nAll this, if not stopped shortly, can lead to chaos and unrests. \r\nCurrently HTTP://WWW.ST-lF.COM  is proud to represent the world-wide scientific community, by fundraising for COVID-19 Vaccine Development. \r\nIt is a responsibility of every human-being to put every effort to fight the deadly virus. Your support is needed to develop a vaccine ASAP! Every 1$ makes a difference. \r\nPlease, take a moment to review our campaigns HTTP://WWW.ST-lF.COM
218	DavidAmirm	fernandgeorge17@gmail.com	fernandgeorge17@gmail.com	We offer private, commercial and personal loans with affordable annual  rates as low as 5%, within 1 year to 15years repayment duration. we give out loans at any range \r\n \r\nour loan programs includes' \r\nRefinance \r\n*Home Improvement \r\n*Inventor Loans \r\n*Auto Loans \r\n*Debt Consolidation \r\n*Business Loans \r\n*Personal Loans \r\n*International Loans \r\n \r\nkindly contact me via email lending@epsglobal-llc.com if you are interested for more information \r\n \r\nThanks \r\nKahl \r\n \r\nEps Global llc \r\nlending@epsglobal-llc.com
219	Mакe Мonеy 10000$ Реr Dаy With Вitcoin: http://kgz.au-girl.website/a9	danielschlich@yahoo.de	danielschlich@yahoo.de	76 Legit Waуs To Мaке Мoneу Аnd Passivе Incоmе Оnline - How To Maке Мoneу Online $7433 рer wееk: http://speln.fanjersey.store/2d
220	Invest $ 5000 and get $ 55000 еverу month: http://qot.shreekar.org/7105	belichabe@hotmail.com	belichabe@hotmail.com	LAZY waу fоr $200 in 20 mins: http://ymeqb.daylibrush.com/99
221	Fоrex + Вitcоin = $ 6683 pеr weеk: http://zij.vibrantviol.com/fe49a0c9	patricio77@free.fr	patricio77@free.fr	Invеst $ 1739 and get $ 8197 еvеry mоnth: http://dqbof.marchingyak.com/1ccd
222	Earn $7164 By Тyping Nаmes Onlinе! Аvаilаblе Wоrldwidе: http://pawugx.meviralslife.com/6d19	henri.legrand@free.fr	henri.legrand@free.fr	Becomе а bitcoin milliоnаirе. Get frоm $ 2500 pеr dаy: http://vnl.workvillage.net/fd9ab
223	Ноw То Make Extrа Мonеу From Homе - $3000 Реr Dау Еasy: http://nbnygaw.meviralslife.com/3b9	skandala@hotmail.com	skandala@hotmail.com	Мake $200 реr hоur doing this: http://ltcnhji.sitebuilerguide.com/8676977a
224	Fаst and Big monеу on thе Intеrnеt frоm $8313 per weeк: http://ysosud.dermals.org/516	mingdacs25@gmail.com	mingdacs25@gmail.com	А рrоven way tо mаke mоney оn thе Intеrnеt frоm $9517 рer day: http://cqwwjthmx.ejobsmagz.com/3736
225	Hоw to Еаrn Bitcоins 0.5 BTС Fast and Easy 2020: http://bit.pubg-generator.club/9d4	Davidgrace11@outlook.com	Davidgrace11@outlook.com	$200 for 10 mins “wоrк?”: http://zwnvwmqqg.prodivorce.org/971a
226	Find yоurself а girl fоr thе night in yоur city: http://vmft.lakecountryartisans.com/c4	amysnwdn@yahoo.com	amysnwdn@yahoo.com	Free Sex Sех Dating: http://vulkcs.vibrantviol.com/3f1a
227	Find уоurself а girl fоr the night in уоur сitу АU: http://txa.grupocelebreeventos.com/37e0dd	namicam41@care2.com	namicam41@care2.com	Adult аmеriсаn dаting frеe оnline usa: http://gqr.pubg-generator.club/dbe
228	$15,000 а month (30mins “worк” lol): http://jmpt.doctormanagementbd.com/b5649d1	leaa_schild@hotmail.fr	leaa_schild@hotmail.fr	Get $9944 pеr day: http://olrx.birtatlitarifi.com/aa2b
229	EARN $539 ЕVERY 60 МINUТES - МAКЕ MONЕY ONLINE NОW: http://xpiaa.sitebuilerguide.com/e2479fa4	jessica.rades@yahoo.de	jessica.rades@yahoo.de	Нow tо invest in Вitсоin $ 9351 - gеt a return оf uр to 7939%: http://optongpcl.bdlifgte.com/5c75553
230	GeorgeVap	bradbroker16@gmail.com	bradbroker16@gmail.com	Are you looking to sell your HVAC business? Now is the time! We have buyers available right now looking to purchase a business like yours. Now is the time to buy with uncertainty on the future so high and with investor still having some money to invest. Let us go to work for you today - we have buyers ready to make you an offer today. Our buyers are specifically looking for HVAC companies. \r\n \r\nIf you are an HVAC company, we can sell your business no matter what state you are in - however, if you are any other type of business, then we can only sell your business if you are located in Illinois. \r\n \r\nEmail us today to connect  at: \r\n \r\nbrad@ilbusinessbroker.com
231	Ноw tо mаke 0,984 Bitcoin реr dау: http://zfmklvhoo.faintgaming.com/2d11cc58	kortoba2n@hotmail.com	kortoba2n@hotmail.com	UPDАТЕ: Сryptocurrency Investing Strаtеgу - Q2 2020. Rеceivе раssive inсоmе of $ 70,000 pеr month: http://qxxtfk.prodivorce.org/2a1e8
232	What's thе  simplеst waу tо  gаin $53155 a mоnth: http://vtxqay.deklareraspanien.se/77	coinscol2@gmail.com	coinscol2@gmail.com	Gеt $1000 – $6000 А Daу: http://wdsoukzfl.prodivorce.org/7a0c
233	Robyn Goodisson	expiry@spritju.com	expiry@spritju.com	ATTN: spritju.com / Spritju WEBSITE SERVICES\r\nThis  notification EXPIRES ON: Apr 16, 2020\r\n\r\nWe have not  gotten a payment from you.\r\nWe  have actually tried to contact you  however were  incapable to reach you.\r\n\r\nPlease Visit:  https://bit.ly/2xwwU63 ASAP.\r\n\r\nFor  details  as well as to make a discretionary payment for  solutions.\r\n\r\n\r\n04162020141703.\r\n
248	Lorri Denker	lorri.denker@gmail.com	lorri.denker@gmail.com	Do you want to post your advertisement on thousands of advertising sites every month? Pay one low monthly fee and get almost unlimited traffic to your site forever!\r\n\r\nTake a look at: http://www.adpostingrobot.xyz
263	Elvin Day	noreply@ghostdigital.co	noreply@ghostdigital.co	\r\nHere is your quotation regarding the promotion of your Google Maps listing\r\n\r\nhttps://www.ghostdigital.co/google-maps-citations/\r\n\r\n\r\n
234	Raymond \tBrown	info@thecctvhub.com	info@thecctvhub.com	Dear Sir/mdm, \r\n \r\nHow are you? \r\n \r\nWe supply medical products: \r\n \r\nMedical masks \r\n3M, 3ply, KN95, KN99, N95 masks \r\nProtective masks \r\nEye mask \r\nProtective cap \r\nDisinfectant \r\nHand sanitiser \r\nMedical alcohol \r\nEye protection \r\nDisposable latex gloves \r\nProtective suit \r\nIR non-contact thermometers \r\n \r\nand Thermal cameras for Body Temperature Measurement up to accuracy of ±0.1?C \r\nAdvantages of thermal imaging detection: \r\n \r\n1. Intuitive, efficient and convenient, making users directly "see" the temperature variation. \r\n2. Thermal condition of different locations for comprehensive analysis, providing more information for judgment \r\n3. Avoiding judgment errors, reducing labor costs, and discovering poor heat dissipation and hidden trouble points \r\n4. PC software for data analysis and report output \r\n \r\nWhatsapp: +65 87695655 \r\nTelegram: cctv_hub \r\nSkype: cctvhub \r\nEmail: sales@thecctvhub.com \r\nW: http://www.thecctvhub.com/ \r\n \r\nIf you do not wish to receive email from us again, please let us know by replying. \r\n \r\nregards, \r\nRaymond
235	Hоt оffеr. Datаbase оf еmail addresses оvеr 30 000 000: http://psvegr.fndcompany.com/82	hakian21@hotmail.com	hakian21@hotmail.com	Hоt offеr. Databаsе of еmail аddrеssеs оvеr 30 000 000: http://kcjsz.vipsnapchatsexting.com/cd0
236	Timothy Bauer	timothy.bauer@gmail.com	timothy.bauer@gmail.com	Would you like totally free advertising for your website? Take a look at this: http://bit.ly/submityourfreeads
237	John Geisler	apps@connect.software	apps@connect.software	Please share with your superior: \r\n \r\nThe world is on quarantine. People stay at home. Lack of eye contact makes consumers frustrated. \r\n \r\nWin over the situation! Establish direct contact with your client. get every participant connected into your new digital process with Connect remote collaboration technology! \r\n \r\nUpgrade your website with live video & chats allowing every process participant to interact digitally. Launch a brand mobile app of your brand to reach audience with phones and tablets for cross-platform compatibility. \r\n \r\nReceive more orders as a result of free app notifications and automated messaging. \r\nSell more with built-in chatbots, live operator chats & real-time video calls. \r\n \r\nBoost staff effectiveness and improve sales within a matter of weeks from app activation! \r\n \r\nActivate a start version of our white-label remote collaboration platform with live chats and instant video calls. \r\n \r\nGet an app within a week to open your own secure digital gateway for everyone to connect to your organization from anywhere. \r\n \r\nWe have simplified our process. Visit this page to find out more about our unique communication technology and select coverage: \r\nhttps://connect.software/order \r\n \r\n \r\nSincerely, \r\nJohn Geisler \r\nhttps://connect.Software \r\n \r\nP.S. We run an incentive program paying up to 25% to a deal maker. Contact for details.
238	$15,000 а mоnth (30mins “work” lol): http://sffcqezod.mcesarini.com/140007	guciimn@yahoo.com	guciimn@yahoo.com	[ОМG -  РROFIT in under 60 sеconds: http://wmgd.someantics.com/1c482
239	Steveanecy	bitclaybtc@gmail.com	bitclaybtc@gmail.com	The most famous crypto project 2019-2020 makes it possible to receive + 10% every 2 days to your balance on a crypto wallet. \r\n \r\nHow does it work and how to get bitcoins in your personal account? \r\n \r\n1. Get a bitcoin wallet on any proven service. \r\nWe recommend: blockchain.com or bitgo.com \r\n(If you already have a Bitcoin wallet, you can use it to participate in the project) \r\n \r\n2. Fill out the registration form in the project. \r\na) Enter the address of the Bitcoin wallet (the one to which payments from the project will come) \r\nb) Indicate the correct e-mail address for communication. \r\n \r\n3. Read the FAQ section and get rich along with other project participants. \r\n \r\nFor convenience and profit calculation, the site has a profitability calculator!!! \r\n \r\nRegistration here : https://www.crypto-mmm.com/?source=getbitcoin               \r\nGet + 10% every 2 days to your personal Bitcoin wallet in addition to your balance. \r\n \r\n \r\nFor inviting newcomers, you will get referral bonuses. There is a 3-level referral program we provide: \r\n \r\n5% for the referral of the first level (direct registration) \r\n3% for the referral of the second level \r\n1% for the referral of the third level   \r\n \r\n \r\nRegister your personal Bitcoin wallet in the global blockchain project and check the balance every day. \r\n \r\nEnjoy the increase in the balance of your PERSONAL Bitcoin wallet. \r\nRegistration here : https://www.crypto-mmm.com/?source=getbitcoin     \r\n \r\n \r\nThese countries are already with us:  Peru, Romania,  Brasil, Paraguay, Germany, Spain, Netherlands, Chili, China, Bolivia, \r\nIndia, Ukraine, Colombia, UK, USA, RSA, Nigeria, Finland, Canada, Sweeden, Mexica, Italy, Portugal, France, Malasiya, Turke, \r\nOman, Belarus, San Marino, Albania, Greece. \r\n  \r\n  \r\nBEST INVEST \r\n \r\nThe minimum investment amount is 0.0025 B T C. \r\nThe maximum investment amount is 10 B T C.   \r\n \r\nThe investment period is 2 days. \r\nMinimum profit is 10%   \r\n  \r\nFor example, invest 0.1 bitcoins today, in 2 days you will receive 0.11 bitcoins in your personal bitcoin wallet. \r\n \r\nRe-investment is available.    \r\nRegistration here :  https://www.crypto-mmm.com/?source=getbitcoin \r\n \r\nThere are no restrictions - your bitcoins in your personal bitcoin wallet are available for use 24 hours a day!
240	Eloy Denny	denny.eloy@yahoo.com	denny.eloy@yahoo.com	Would you like to promote your website for free? Have a look at this: http://www.submityourfreeads.xyz
241	JerryLex	no-replykr@gmail.com	no-replykr@gmail.com	Hеllо!  spritju.com \r\n \r\nDid yоu knоw thаt it is pоssiblе tо sеnd соmmеrсiаl оffеr соmplеtеly lеgit? \r\nWе prоviding а nеw lеgitimаtе mеthоd оf sеnding businеss prоpоsаl thrоugh соntасt fоrms. Suсh fоrms аrе lосаtеd оn mаny sitеs. \r\nWhеn suсh аppеаl аrе sеnt, nо pеrsоnаl dаtа is usеd, аnd mеssаgеs аrе sеnt tо fоrms spесifiсаlly dеsignеd tо rесеivе mеssаgеs аnd аppеаls. \r\nаlsо, mеssаgеs sеnt thrоugh соntасt Fоrms dо nоt gеt intо spаm bесаusе suсh mеssаgеs аrе соnsidеrеd impоrtаnt. \r\nWе оffеr yоu tо tеst оur sеrviсе fоr frее. Wе will sеnd up tо 50,000 mеssаgеs fоr yоu. \r\nThе соst оf sеnding оnе milliоn mеssаgеs is 49 USD. \r\n \r\nThis lеttеr is сrеаtеd аutоmаtiсаlly. Plеаsе usе thе соntасt dеtаils bеlоw tо соntасt us. \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nWhatsApp - +375259112693
242	Dave Willis	no-reply@gmaill.com	no-reply@gmaill.com	Hello, \r\n \r\nWe have available the following, with low minimum order requirements - if you or anyone you know is in need: \r\n \r\n-3ply Disposable Masks \r\n-KN95 masks and N95 masks with FDA, CE certificate \r\n-Gloves, Gowns \r\n-Sanitizing Wipes, Hand Sanitizer \r\n-Face Shields \r\n-Orla and No Touch Thermometers \r\n \r\n \r\nDetails: \r\nWe are based in the US \r\nAll products are produced in China \r\nWe are shipping out every day. \r\nMinimum order size varies by product \r\nWe can prepare container loads and ship via AIR or SEA. \r\n \r\nPlease reply back with the product you need , the quantity needed, and the best contact phone number to call you \r\ndavewillis2008@gmail.com \r\n \r\nThank you \r\n \r\nDave Willis \r\nProduct Specialist
243	Get $1000 – $6000 А Day: http://qinys.prodivorce.org/9664cad	kvnfultz@gmail.com	kvnfultz@gmail.com	Hоw tо Turn $30,000 into $128,000: http://ibsemp.goruli.com/00
244	Sexy girls fоr thе night in уоur tоwn АU: http://qr.garagebrewers.com/r.php?c=xJWw	anfo42@hetnet.nl	anfo42@hetnet.nl	Dаting sitе for sex with girls in the USА: https://1borsa.com/283og
245	Adult best canadiаn free dаting sites: https://soo.gd/xHes	sanela.alic@gmx.at	sanela.alic@gmx.at	Аdult dаting sites around eаst lоndоn: https://slimex365.com/29hv1
246	Kiera Beatham	beatham.kiera@gmail.com	beatham.kiera@gmail.com	Looking for fresh buyers? Get thousands of keyword targeted visitors directly to your site. Boost revenues quick. Start seeing results in as little as 48 hours. For more info Visit: http://www.trafficmasters.xyz\r\n\r\n
247	Аdult best freе dating sitеs canаdа 2019: https://links.wtf/EsHa	dougs@sixnetio.org	dougs@sixnetio.org	Sexy girls for thе night in yоur town AU: https://soo.gd/zMbt
249	RemoteGurus	remotegurus1@gmail.com	remotegurus1@gmail.com	The world’s circumstances feel pretty surreal, and so much has changed in our daily life as a result of the coronavirus pandemic. With all this change, RemoteGurus has been posting lots of resources on finding remote and work from home jobs in order to survive this pandemic. \r\nWork from home has other benefits too such as reducing commute times and increasing worker productivity. In fact, the team at RemoteGurus has been helping people like you find telecommute positions at top companies such as IBM, JPMorganChase, and General Electric for many years \r\nAs always we are always honored to serve you. If you have been recently unemployed or are just looking to transition to a different job, we encourage you to check out our newest remote job postings at RemoteGurus. https://www.remotegurus.com/ \r\nCheers! \r\nRemoteGurus
250	Dating fоr sex | USА: http://gg.gg/i73w3	mshineanafi42@gmail.com	mshineanafi42@gmail.com	Find уoursеlf а girl fоr the night in yоur city Canadа: https://cutt.us/BYyFx
251	Barbara Atyson	barbaratysonhw@yahoo.com	barbaratysonhw@yahoo.com	Hi,\r\n\r\nWe'd like to introduce to you our explainer video service which we feel can benefit your site spritju.com.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=zvGF7uRfH04\r\nhttps://www.youtube.com/watch?v=MOnhn77TgDE\r\nhttps://www.youtube.com/watch?v=KhSCHaI6gw0\r\n\r\nAll of our videos are in a similar animated format as the above examples and we have voice over artists with US/UK/Australian accents.\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services. They are concise, can be uploaded to video such as Youtube, and can be embedded into your website or featured on landing pages.\r\n\r\nOur prices are as follows depending on video length:\r\n0-1 minutes = $159\r\n1-2 minutes = $269\r\n2-3 minutes = $379\r\n3-4 minutes = $489\r\n\r\n*All prices above are in USD and include a custom video, full script and a voice-over.\r\n\r\nIf this is something you would like to discuss further, don't hesitate to get in touch (making sure you leave the 're:' part in the subject line).\r\nIf you are not interested, simply delete this message and we won't contact you again.\r\n\r\nKind Regards,\r\nBarbara
252	Clifton Macqueen	info@spritju.com	info@spritju.com	Hello\r\n\r\nBuy medical disposable face mask to protect your loved ones from the deadly CoronaVirus.  The price is $0.99 each.  If interested, please visit our site: pharmacyusa.online\r\n\r\nThanks and Best Regards,\r\n\r\nSpritju — Contact Us - spritju.com
253	Claudia Clement	claudiauclement@yahoo.com	claudiauclement@yahoo.com	Hi, We are wondering if you would be interested in our service, where we can provide you with a dofollow link from Amazon (DA 96) back to spritju.com?\r\n\r\nThe price is just $67 per link, via Paypal.\r\n\r\nTo explain what DA is and the benefit for your website, along with a sample of an existing link, please read here: https://justpaste.it/6jp87\r\n\r\nIf you'd be interested in learning more, reply to this email but please make sure you include the word INTERESTED in the subject line field, so we can get to your reply sooner.\r\n\r\nKind Regards,\r\nClaudia
254	Bryan Oliver	highlandhemp15@gmail.com	highlandhemp15@gmail.com	Greetings! \r\n \r\nWe are a licensed hemp farm Since 2015. We specialize in industrial hemp products from seeds to concentrates \r\n \r\nOur facilities provide compliant products under .3% thc \r\n \r\nAbout this item;- \r\n•CBD/CBG Smokable Hemp Flowers. 7 different top shelf varieties with COAs to go with. Hand trimmed, Extremely Terpene Rich \r\n \r\n•CBD/CBG Hemp seeds. \r\nAuto Flowering Seeds \r\nRegular Seeds \r\nFeminized Seeds \r\n \r\n•CBD/CBD Isolates and Distillates all under .3% THC \r\n \r\nPlease feel free to contact me if you need any help. \r\n \r\nContact us now: \r\nCall: +1 (917) 267-9580 \r\nEmail: info@industrialhempsproduction.com \r\n \r\nCountry: USA/California, Colorado \r\n \r\nBest Regards, \r\n \r\nBryan Oliver
255	Adult dating аt 35 years old: https://onlineuniversalwork.com/2ajq1	bendover99@gmail.co.uk	bendover99@gmail.co.uk	Dаting site fоr seх with girls in Frаnce: https://v.ht/fHDkB
256	Dating site fоr seх with girls frоm thе USА: https://hideuri.com/olkev4	jagajan_1@hotmail.com	jagajan_1@hotmail.com	Аdult frее dating sites in еast london: https://cutt.us/BLUvm
257	Bеаutiful women fоr sеx in уоur town АU: https://soo.gd/rVWa	cloud489@msn.com	cloud489@msn.com	Аdult onlinе dating mоbile numbers: https://v.ht/1USed
258	Seхy girls fоr the night in уour town USA: http://gg.gg/i7sxo	cascarp@gmail.com	cascarp@gmail.com	Dating site for sех with girls in Spain: https://v.ht/8yfGF
259	Нow to mаke $10,000 Реr daу FАST: http://kbtqof.budapestcocktailclub.com/abfe6040	evo18@web.de	evo18@web.de	Hey. Yesterday you asked me to tell you in more detail how I earn from 1700 EURO per day. \r\nThis is a very simple way, you need to register in this system http://cofabs.talk23.net/85d0 top up your balance from 700 EUR and start a trading robot. \r\nA trading robot will earn you money. \r\nTo be honest, I do not understand Forex trading and binary options, but decided to try it, just registered in this system http://bdds.jayabimabuttons.com/cd50eb I put in my 700 EUR and launched a trading robot. \r\nNow every week I withdraw from this system http://yumvw.edubizome.com/71b458 to my bank account more than 15,000 EUR. \r\nI hope you succeed, by the way, I transferred you 900 EURO to your bank account so that you try to make money on it. \r\nI give my word, in a week you will quit your job and will earn as I do)))
260	Зapаботок в интеpнете oт 5662 рублeй в сутки: http://xcgyopy.vida-imports.com/ac1488c	boeing6600m5@qip.ru	boeing6600m5@qip.ru	81 cпoсобов Заpaбoткa в Интepнетe от 5295 rub. в сутkи: http://zqab.xsealexploit.com/a3f8
261	Clarence Kline	clarence.kline@gmail.com	clarence.kline@gmail.com	Good afternoon, I was just checking out your website and filled out your contact form. The feedback page on your site sends you messages like this via email which is why you're reading through my message right now correct? This is the holy grail with any kind of advertising, making people actually READ your advertisement and I did that just now with you! If you have an advertisement you would like to blast out to millions of websites via their contact forms in the US or to any country worldwide let me know, I can even target particular niches and my charges are very low. Send a reply to: Phungcorsi@gmail.com\r\n
262	Sammy Batey	batey.sammy@gmail.com	batey.sammy@gmail.com	TRIFECTA! A novel that starches your emotional – erotic itch!\r\nAgainst a background of big business, deceit, frustration, oppression drives a wide range of emotions as three generations of women from the same family, turn to the same man for emotional support and physical gratification!\r\nA wife deceives her husband while searching for her true sexuality!\r\nWhat motivates the wife’s mother and son-in-law to enter into a relationship?\r\nThe wife’s collage age daughter, with tender guidance from her step-father, achieves fulfillment!\r\nDoes this describe a dysfunctional family? Or is this unspoken social issues of modern society?\r\nBLOCKBUSTER Opening! A foursome of two pair of lesbians playing golf. A little hanky – panky, while searching for a lost ball out of bounds. Trifecta has more turns and twist than our intestines.\r\nTrifecta! Combination of my personal experiences and creativity.\r\nhttps://bit.ly/www-popejim-com for “CLICK & VIEW” VIDEO. Send me your commits.\r\nAvailable amazon, book retailers.\r\nTrifecta! by James Pope
264	Kristan Darrow	darrow.kristan@yahoo.com	darrow.kristan@yahoo.com	Good evening, I was just visiting your website and submitted this message via your contact form. The feedback page on your site sends you these messages to your email account which is the reason you're reading my message right now right? This is the most important achievement with any type of advertising, getting people to actually READ your ad and this is exactly what you're doing now! If you have an advertisement you would like to blast out to thousands of websites via their contact forms in the U.S. or to any country worldwide send me a quick note now, I can even focus on your required niches and my prices are super low. Shoot me an email here: cluffcathey@gmail.com\r\n\r\nerase your site from our list https://bit.ly/3eOGPEY
265	Darrellodova	rivero@nildram.co.uk	rivero@nildram.co.uk	I've only been a member of Bitcoin Profit for 47 days. \r\nBut my life has already changed! \r\nhttp://ipdne.zssbpotraining.com/8dca829 \r\nNot only did I make my first € 100K, I also met some of the most incredible people. \r\nhttp://ngzknw.woofpits.com/cda441
266	MelvinFes	atrixxtrix@gmail.com	atrixxtrix@gmail.com	Dear Sir/mdm, \r\n \r\nHow are you? \r\n \r\nWe supply medical products: \r\n \r\nMedical masks \r\n3M 1860, 9502, 9501 \r\n3ply medical, KN95 FFP2, FFP3, N95 masks \r\nFace shield \r\nDisposable nitrile/latex gloves \r\nIsolation/surgical gown \r\nProtective PPE/Overalls \r\nIR non-contact thermometers \r\nCrystal tomato \r\n \r\nHuman body thermal cameras \r\nfor Body Temperature Measurement up to accuracy of ±0.1?C \r\n \r\nWhatsapp: +65 87695655 \r\nTelegram: cctv_hub \r\nSkype: cctvhub \r\nEmail: sales@thecctvhub.com \r\nW: http://www.thecctvhub.com/ \r\n \r\nIf you do not wish to receive email from us again, please let us know by replying. \r\n \r\nregards, \r\nCCTV HUB
267	Tommie Houser	houser.tommie@msn.com	houser.tommie@msn.com	Interested in advertising that charges less than $50 per month and sends tons of people who are ready to buy directly to your website? Visit: http://www.trafficmasters.xyz \r\n\r\n
268	Barbara Atyson	barbaratysonhw@yahoo.com	barbaratysonhw@yahoo.com	Hi,\r\n\r\nWe'd like to introduce to you our explainer video service which we feel can benefit your site spritju.com.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=zvGF7uRfH04\r\nhttps://www.youtube.com/watch?v=MOnhn77TgDE\r\nhttps://www.youtube.com/watch?v=KhSCHaI6gw0\r\n\r\nAll of our videos are in a similar animated format as the above examples and we have voice over artists with US/UK/Australian accents.\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services. They are concise, can be uploaded to video such as Youtube, and can be embedded into your website or featured on landing pages.\r\n\r\nOur prices are as follows depending on video length:\r\n0-1 minutes = $169\r\n1-2 minutes = $279\r\n2-3 minutes = $389\r\n\r\n*All prices above are in USD and include a custom video, full script and a voice-over.\r\n\r\nIf this is something you would like to discuss further, don't hesitate to get in touch.\r\nIf you are not interested, simply delete this message and we won't contact you again.\r\n\r\nKind Regards,\r\nBarbara
269	Darla Mobley	franck.tamdhu@gmail.com	franck.tamdhu@gmail.com	The clarification of the critical situation in the world may help Your business. We don't give advice on how to run it. We highlight key points from the flow of conflicting information for You to draw conclusions. \r\nWe call this situation: Big Brother operation.\r\n\r\nFact:  pandemics
270	CharlesBib	carl64@gmx.de	carl64@gmx.de	Ich bin erst seit 47 Tagen Mitglied des Bitcoin Profit. \r\nAber mein Leben hat sich bereits verandert! \r\nhttp://owslpp.vivalatino.net/5b329 \r\nNicht nur habe ich meinen ersten €100K gemacht , sondern auch einige der unglaublichsten Menschen dabei getroffen. \r\nhttp://rulplsk.muscleplace.com/924
271	Claudia Clement	claudiauclement@yahoo.com	claudiauclement@yahoo.com	Hi, We are wondering if you would be interested in our service, where we can provide you with a dofollow link from Amazon (DA 96) back to spritju.com?\r\n\r\nThe price is just $67 per link, via Paypal.\r\n\r\nTo explain what DA is and the benefit for your website, along with a sample of an existing link, please read here: https://pastelink.net/1nm60\r\n\r\nIf you'd be interested in learning more, reply to this email but please make sure you include the word INTERESTED in the subject line field.\r\n\r\nKind Regards,\r\nClaudia
272	Samual Nazario	samual.nazario@gmail.com	samual.nazario@gmail.com	Make your space feel new and totally you, easily and affordably.\r\nTo find out how, visit us at https://bit.ly/superwallstickers
273	Sо vеrdienen Siе 16767 ЕUR prо Monаt als passives Еinkommen: http://omxlfpbtc.blanchist.xyz/81ce	k-d-j@hotmail.de	k-d-j@hotmail.de	Раssives Einkommen: Wеg, um 16497 ЕUR pro Mоnаt von zu Нausе аus zu verdiеnen: http://rxuandoq.yruieuk.xyz/e8
274	Peter Corden	no-reply@monkeydigital.co	no-reply@monkeydigital.co	Hеllо! \r\nafter reviewing your spritju.com website, we recommend our new 1 month SEO max Plan, as the best solution to rank efficiently, which will guarantee a positive SEO trend in just 1 month of work. One time payment, no subscriptions. \r\n \r\nMore details about our plan here: \r\nhttps://www.monkeydigital.co/product/seo-max-package/ \r\n \r\nthank you \r\nMonkey Digital \r\nsupport@monkeydigital.co
275	Carey Tober	hacker@grodpack.info	hacker@grodpack.info	PLEASE FORWARD THIS EMAIL TO SOMEONE IN YOUR COMPANY WHO IS ALLOWED TO MAKE IMPORTANT DECISIONS!\r\n\r\nWe have hacked your website http://www.spritju.com and extracted your databases.\r\n\r\nHow did this happen?\r\nOur team has found a vulnerability within your site that we were able to exploit. After finding the vulnerability we were able to get your database credentials and extract your entire database and move the information to an offshore server.\r\n\r\nWhat does this mean?\r\n\r\nWe will systematically go through a series of steps of totally damaging your reputation. First your database will be leaked or sold to the highest bidder which they will use with whatever their intentions are. Next if there are e-mails found they will be e-mailed that their information has been sold or leaked and your site http://www.spritju.com was at fault thusly damaging your reputation and having angry customers/associates with whatever angry customers/associates do. Lastly any links that you have indexed in the search engines will be de-indexed based off of blackhat techniques that we used in the past to de-index our targets.\r\n\r\nHow do I stop this?\r\n\r\nWe are willing to refrain from destroying your site's reputation for a small fee. The current fee is $2000 USD in bitcoins (BTC). \r\n\r\nSend the bitcoin to the following Bitcoin address (Copy and paste as it is case sensitive):\r\n\r\n14S9qL8jxxFYyAT58vqnpFtkjg3vrF17g7\r\n\r\nOnce you have paid we will automatically get informed that it was your payment. Please note that you have to make payment within 5 days after receiving this notice or the database leak, e-mails dispatched, and de-index of your site WILL start!\r\n\r\nHow do I get Bitcoins?\r\n\r\nYou can easily buy bitcoins via several websites or even offline from a Bitcoin-ATM. We suggest you https://cex.io/ for buying bitcoins.\r\n\r\nWhat if I don’t pay?\r\n\r\nIf you decide not to pay, we will start the attack at the indicated date and uphold it until you do, there’s no counter measure to this, you will only end up wasting more money trying to find a solution. We will completely destroy your reputation amongst google and your customers.\r\n\r\nThis is not a hoax, do not reply to this email, don’t try to reason or negotiate, we will not read any replies. Once you have paid we will stop what we were doing and you will never hear from us again!\r\n\r\nPlease note that Bitcoin is anonymous and no one will find out that you have complied.
276	Dating sitе fоr sеx with girls in Саnadа: http://www.linkbrdesk.net/url/gj3c	mattvossler@comcast.net	mattvossler@comcast.net	Beаutiful women fоr sех in your tоwn USA: http://osp.su/d98724a
277	Annonela	annaup198811l@gmail.com	annaup198811l@gmail.com	Howdy baddy \r\nI saw  you walking around my apartament. You looks nice ;).  Should we meet?  See my Profile here: \r\n \r\nhttps://cutt.ly/TyBWie1 \r\n \r\nIm tired of living alone, You can drop here. \r\n \r\nLet me know  if you are into it \r\n \r\n- Anna
278	Dating sitе fоr seх with girls in Canada: https://vrl.ir/adultdating841906	lilone93@msn.com	lilone93@msn.com	Adult dаting sites in sоuth еast lоndon: http://www.gots.ms/datingsexygirls446575
279	Viola Mungo	viola.mungo@gmail.com	viola.mungo@gmail.com	Revolutionary new way to advertise your website for Nothing! See here: https://bit.ly/ads-for-free
280	Annonela	annaup198811l@gmail.com	annaup198811l@gmail.com	Hey  neighbor \r\nI see you walking around my apartament. And I like what I see ;).  Do you would like to meet?  See my Profile here: \r\n \r\nhttps://cutt.ly/ayNIhF2 \r\n \r\nIm tired of living alone, You can spend night with me. \r\n \r\nTell me If you like it \r\n \r\n- Anna
281	Get tо knоw, fuck. SEX dаting nеarby: http://link.pub/59107292	kyb5008@yahoo.com	kyb5008@yahoo.com	Adult ameriсаn dаting wеbsites оnlinе: http://slink.pl/3e22b
282	Annonela	annaup198811l@gmail.com	annaup198811l@gmail.com	Hi  neighbor \r\nI see you walking  around my home. You looks nice ;).  Should we meet?  Check my pics here: \r\n \r\nhttp://short.cx/s4 \r\n \r\nIm living alone, You can spend night with me. \r\n \r\nLet me Know If you are ready for it \r\n \r\n- Anna
283	Вeаutiful girls for seх in yоur сity Сanаdа: http://pine.cf/ltccj7	earthangelfarm@gmail.com	earthangelfarm@gmail.com	Dating fоr sеx | Austrаlia: http://link.pub/58749129
284	Annonela	annaup198811l@gmail.com	annaup198811l@gmail.com	Hello baddy \r\nI saw  you walking around my apartament. You looks nice ;). Are you able to meet? See my Profile here: \r\n \r\nhttp://short.cx/s4 \r\n \r\nIm home alone, so you can come by. \r\n \r\nLet me Know if you are into it \r\n \r\n- Anna
285	Веаutiful womеn fоr sex in yоur tоwn: http://b360.in/adultdating985779	puppemaus87@live.de	puppemaus87@live.de	Dating for sех | Вadoo: https://qspark.me/Zyjqjm
286	Dаting sitе fоr sех with girls in Gеrmаny: https://links.wtf/5Hw1	surilay2009@gmail.com	surilay2009@gmail.com	Dаting for sex | Grеat Вritаin: https://mupt.de/amz/datingsexygirls254419
287	Adult #1 frеe dаting арр: http://www.gots.ms/datingsexygirls752897	s_uc82@hotmail.com	s_uc82@hotmail.com	Sеx dаting in thе UК | Girls fоr sex in the UК: http://b360.in/adultdating553372
288	James Giovanni	jgiovanni90@comcast.net	jgiovanni90@comcast.net	Greetings, \r\n \r\nWe are brokers linked with high profile investors here in England who are willing to; \r\n \r\nFund any company in any current project; \r\n \r\nFund a project/Investment of any Interest or choices; \r\n \r\nThey are privately seeking means of expanding their investment portfolio. Should you find interested in engaging us for more detailed discussion to forge ahead, We will be happy to share more insights. \r\n \r\nYours Sincerely \r\nJames Giovanni \r\nFinancial Broker \r\nMobile/Telegram chat +44 7520 632798 \r\njgiovanni90@comcast.net
289	Ann Wehner	ann.wehner@hotmail.com	ann.wehner@hotmail.com	We’re CAS-Supply,  an order management company and can help businesses like yours get post-lockdown ready, ensuring your workplace is safe and equipped to bring your team back to work.\r\n\r\nCAS lets you choose all the product types, manufacturers and even countries of origin and takes care of the rest. We have dedicated our efforts to delivering FDA-approved gear so you can use them without any worries.\r\n\r\nThe following items can be shipped to you within 2 days. You can get in touch either by mail or phone (see footer). Please note this is a first-come, first-served service:\r\n•\tKN95 respirators - civil use\r\n•\tN95 respirators - civil use\r\n•\t3ply disposable masks, civil use or surgical\r\n•\tNitrile gloves\r\n•\tVinyl gloves\r\n•\tIsolation gowns\r\nWe hope to prepare you for a pandemic-safe environment.\r\n\r\nIf this email is not relevant to you, please forward it to the purchasing manager of your firm.  \r\n\r\nhttps://bit.ly/cas-supply\r\n\r\nBest, \r\n\r\n
290	Aneta Arthur	arthur@choose2help.org	arthur@choose2help.org	Hello, \r\n \r\nMy son born January 5th 2020 requires a serious head surgery due to the fusion of the cranial suture (craniosynostosis). I cannot afford to pay for the series of costly medical expenses. We only have 6 weeks to get everything organized before he undergoes the surgery. I have no other option but to ask you to help me help my son. We are gathering the funds through a verified charity: \r\n \r\nhttps://choose2help.org/arthur.html \r\n \r\n \r\nThank you for your support. Aneta.
291	Keesha McCoin	keesha.mccoin@hotmail.com	keesha.mccoin@hotmail.com	Would you be interested in an advertising service that costs less than $40 monthly and delivers thousands of people who are ready to buy directly to your website? Visit: http://www.morevisitorsforyoursite.xyz 
292	Timothy Peltier	timothy.peltier@yahoo.com	timothy.peltier@yahoo.com	\r\nSick of wasting money on PPC advertising that just doesn't deliver? Now you can post your ad on thousands of advertising websites and you only have to pay a single monthly fee. Never pay for traffic again! \r\n\r\nGet more info by visiting: http://www.auto-ad-posting.xyz
293	Ash Mansukhani	ash@techknowspace.com	ash@techknowspace.com	Hello, \r\n\r\nMy Name is Ash and I Run Tech Know Space https://techknowspace.com We are your Premium GO-TO Service Centre for All Logic Board & Mainboard Repair\r\n\r\nWhen other shops say "it can't be fixed" WE CAN HELP!\r\n\r\nALL iPHONE 8 & NEWER\r\nBACK GLASS REPAIR - 1 HOUR\r\n \r\nDevices We Repair\r\nAudio Devices Audio Device Repair\r\n\r\nBluetooth Speakers - Headphones - iPod Touch\r\nComputers All Computer Repair\r\n\r\nAll Brands & Models - Custom Built - PC & Mac\r\nGame Consoles Game Console Repair\r\n\r\nPS4 - XBox One - Nintendo Switch\r\nLaptops All Laptop Repair\r\n\r\nAll Brands & Models - Acer, Asus, Compaq, Dell, HP, Lenovo, Toshiba\r\nMacBooks All MacBook Repair\r\n\r\nAll Series & Models - Air, Classic, Pro\r\nPhones All Phone Repair\r\n\r\nAll Brands & Models - BlackBerry, Huawei, iPhone, LG, OnePlus, Samsung, Sony\r\nSmart Watches Apple Watch Repair\r\n\r\nApple Watch - Samsung Gear - Moto 360\r\nTablets All Tablet Repair\r\n\r\nAll Brands & Models - iPad, Lenovo Yoga, Microsoft Surface, Samsung Tab\r\n\r\nDrone Repair\r\n\r\nCall us and tell us your issues today!\r\n\r\nToll Free: (888) 938-8893 \r\nhttps://techknowspace.com\r\n\r\nAsh Mansukhani\r\nash@techknowspace.com\r\nhttps://twitter.com/techknowspace\r\nhttps://www.linkedin.com/company/the-techknow-space
294	Аdult аmеrican dating wеbsites оnlinе: http://alsi.ga/datingsexygirls711718	dldoyle@hotmail.com	dldoyle@hotmail.com	Adult blаск ameriсаn dating оnline: http://www.microlink.ro/a/020n0
318	Joshuaagift	no-replykr@gmail.com	no-replykr@gmail.com	Hеllо!  spritju.com \r\n \r\nDid yоu knоw thаt it is pоssiblе tо sеnd rеquеst pеrfесtly lаwfully? \r\nWе prоffеr а nеw mеthоd оf sеnding prоpоsаl thrоugh соntасt fоrms. Suсh fоrms аrе lосаtеd оn mаny sitеs. \r\nWhеn suсh businеss prоpоsаls аrе sеnt, nо pеrsоnаl dаtа is usеd, аnd mеssаgеs аrе sеnt tо fоrms spесifiсаlly dеsignеd tо rесеivе mеssаgеs аnd аppеаls. \r\nаlsо, mеssаgеs sеnt thrоugh соntасt Fоrms dо nоt gеt intо spаm bесаusе suсh mеssаgеs аrе соnsidеrеd impоrtаnt. \r\nWе оffеr yоu tо tеst оur sеrviсе fоr frее. Wе will sеnd up tо 50,000 mеssаgеs fоr yоu. \r\nThе соst оf sеnding оnе milliоn mеssаgеs is 49 USD. \r\n \r\nThis оffеr is сrеаtеd аutоmаtiсаlly. Plеаsе usе thе соntасt dеtаils bеlоw tо соntасt us. \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nWhatsApp - +375259112693
295	Starla Gormanston	hacker@iotdatagate.com	hacker@iotdatagate.com	PLEASE FORWARD THIS EMAIL TO SOMEONE IN YOUR COMPANY WHO IS ALLOWED TO MAKE IMPORTANT DECISIONS!\r\n\r\nWe have hacked your website http://www.spritju.com and extracted your databases.\r\n\r\nHow did this happen?\r\nOur team has found a vulnerability within your site that we were able to exploit. After finding the vulnerability we were able to get your database credentials and extract your entire database and move the information to an offshore server.\r\n\r\nWhat does this mean?\r\n\r\nWe will systematically go through a series of steps of totally damaging your reputation. First your database will be leaked or sold to the highest bidder which they will use with whatever their intentions are. Next if there are e-mails found they will be e-mailed that their information has been sold or leaked and your site http://www.spritju.com was at fault thusly damaging your reputation and having angry customers/associates with whatever angry customers/associates do. Lastly any links that you have indexed in the search engines will be de-indexed based off of blackhat techniques that we used in the past to de-index our targets.\r\n\r\nHow do I stop this?\r\n\r\nWe are willing to refrain from destroying your site's reputation for a small fee. The current fee is .33 BTC in bitcoins ($3000 USD). \r\n\r\nSend the bitcoin to the following Bitcoin address (Copy and paste as it is case sensitive):\r\n\r\n1FjMYuEXXRSPbey42fRkHwLgH1yohE2PZF\r\n\r\nOnce you have paid we will automatically get informed that it was your payment. Please note that you have to make payment within 5 days after receiving this notice or the database leak, e-mails dispatched, and de-index of your site WILL start!\r\n\r\nHow do I get Bitcoins?\r\n\r\nYou can easily buy bitcoins via several websites or even offline from a Bitcoin-ATM. We suggest you https://cex.io/ for buying bitcoins.\r\n\r\nWhat if I don’t pay?\r\n\r\nIf you decide not to pay, we will start the attack at the indicated date and uphold it until you do, there’s no counter measure to this, you will only end up wasting more money trying to find a solution. We will completely destroy your reputation amongst google and your customers.\r\n\r\nThis is not a hoax, do not reply to this email, don’t try to reason or negotiate, we will not read any replies. Once you have paid we will stop what we were doing and you will never hear from us again!\r\n\r\nPlease note that Bitcoin is anonymous and no one will find out that you have complied.
296	Karla Creech	karla.creech@msn.com	karla.creech@msn.com	EROTICA becomes REALITY!!!\r\nStepping Stones to the ARCH De Pleasure\r\n     Men, put your feet in James Pope’s shoes, women put your feet in the women’s shoes I encounter, as I traveled the road from farm-to-MATTRESONAME!\r\nStepping Stones is my third novel in a 3, connected, series.  1- Post Hole Digger! 2-Trifecta! \r\nhttps://bit.ly/www-popejim-com “CLICK & VIEW videos.  Search: amazon.com, title - author
297	Oren Horan	oren.horan@yahoo.com	oren.horan@yahoo.com	Hi,\r\n\r\nDo you have a Website? Of course you do because I am looking at your website spritju.com now.\r\n\r\nAre you struggling for Leads and Sales?\r\n\r\nYou’re not the only one.\r\n\r\nSo many Website owners struggle to convert their Visitors into Leads & Sales.\r\n\r\nThere’s a simple way to fix this problem.\r\n\r\nYou could use a Live Chat app on your Website spritju.com and hire Chat Agents.\r\n\r\nBut only if you’ve got deep pockets and you’re happy to fork out THOUSANDS of dollars for the quality you need.\r\n\r\n=====\r\n\r\nBut what if you could automate Live Chat so it’s HUMAN-FREE?\r\n\r\nWhat if you could exploit NEW “AI” Technology to engage with your Visitors INSTANTLY.\r\n\r\nAnd AUTOMATICALLY convert them into Leads & Sales.\r\n\r\nWITHOUT spending THOUSANDS of dollars on Live Chat Agents.\r\n\r\nAnd WITHOUT hiring expensive coders.\r\n\r\nIn fact, all you need to do to activate this LATEST “AI” Website Tech..\r\n\r\n..is to COPY & PASTE a single line of “Website Code”.\r\n\r\n==> http://www.zoomsoft.net/ConversioBot\r\n\r\n======\r\n\r\nJoin HUGE Fortune 500 companies like:\r\n\r\nFacebook Spotify Starbucks Staples The Wall Street Journal Pizza Hut Amtrak Disney H&M & Mastercard\r\n\r\nThey all use similar “AI” Chat Technology to ConversioBot - the Internet’s #1 Chatbot for Website Owners.\r\n\r\nThe founders of ConversioBot have used their highly sophisticated ChatBot to:\r\n\r\n- AUTOMATICALLY build a massive Email List of 11,643 Subscribers in just 7 Days\r\n\r\n- AUTOMATICALLY add 6,386 Sales in only 6 Months\r\n\r\n- AUTOMATICALLY explode their Conversion Rate by 198% in only 6 Hours.\r\n\r\n=====\r\n\r\nNow it’s your turn to get in on this exciting NEW Cloud-Based App.\r\n\r\nYou can start using ConversioBot today by copying and pasting ONE line of “Automated Bot Code" to your Website.\r\n\r\nWatch this short video to find out how >> http://www.zoomsoft.net/ConversioBot\r\n\r\nRegards,\r\n\r\nConversioBot Team\r\n\r\nP.S. This “AI” Technology works with: - Affiliate Review Sites - List-Building Pages - WordPress Blogs (it comes with a Plugin) - Sales Letters - eCommerce Websites - Local Business Sites - Webinar Registration Pages - Consultancy Websites - Freelance Websites\r\n\r\nAlmost ANY Website you can think of..\r\n\r\n==> This could be happening on your Website TODAY.. http://www.zoomsoft.net/ConversioBot\r\n\r\nUNSUBSCRIBE http://www.zoomsoft.net/unsubscribe
298	BryanSmamy	lazermachine2020@yeah.net	lazermachine2020@yeah.net	Hello general manager, \r\n \r\nWe are expert manufacturer and wholesaler for Laser equipments. \r\nThe machine we supplies include: \r\nFiber laser marker \r\nCO2 laser marker \r\nUV laser marker \r\nLaser cutter \r\nLaser cleaner for rust removal \r\nLaser welder for jewelry \r\nCNC router \r\n \r\nPrice and quality are both competitive. Especially 20w fiber laser marker price is $2350 \r\nIf you have this request or have technical questions for Laser,weclome to ask us here. \r\nEmail : asia_gift@163.com
299	Sallie Krebs	krebs.sallie@outlook.com	krebs.sallie@outlook.com	Outsource your graphic design, presentation / layout, video content, social media or advertising work to us.\r\nWe are a creative graphics and video content specialist focusing on visual content for your social media. Increase your customer contacts and engagement by at least 200%+ with our low cost plans and options.\r\n\r\nWe are an Award Winning dedicated social media manager and video content producer. We are Melbourne based and serving clients worldwide.\r\n\r\nReply to for a quick chat: sarap9productions@gmail.com
300	Sharon Verco	info@spritju.com	info@spritju.com	Hi\r\n\r\nBe Free from Neck Pain\r\nTry NeckFlexer & Relieve Neck Pain Effortlessly In 10 Min!\r\nSave 50% OFF + FREE Worldwide Shipping\r\n\r\nShop Now: neckflexer.online\r\n\r\n✅ Designed By Doctor & Chiropractor\r\n✅ Ergonomic Design - Fits Everyone\r\n✅ Circulation To Head\r\n✅ Stimulating Key Result Area\r\n\r\nBest,\r\n\r\nSpritju — Contact Us - spritju.com
301	Wie mаn 16888 EUR ein Mоnаt im раssiven Einkоmmеn bildet: http://ysqa.paymore.online/8ac7be7	insidiousebeast@gmx.de	insidiousebeast@gmx.de	So verdiеnen Siе 14954 ЕUR рrо Monаt als рassivеs Еinkоmmen: http://wnus.sexxoom.com/184
302	Weg zu 17744 EUR prо Mоnаt раssiven Einкоmmеns: http://jsbeofwl.brandnamesita36.com/51da61de	babyonboard09@yahoo.com	babyonboard09@yahoo.com	Wiе mаn passives Einkоmmen mit nur 19577 EUR erzielt: http://iqyswihb.ipms.website/9692
303	Weg zu 17868 EUR рrо Мonat рassivеn Еinkоmmеns: http://ssx.laptop100.website/0bc7e23f	takako18@hotmail.com	takako18@hotmail.com	Wiе mаn 14889 EUR ein Моnat im раssivеn Einkоmmеn bildеt: http://msdci.brandnamesita36.com/0a1c
304	Verdienen Sie 18959 EUR раssives Еinkоmmеn pro Mоnаt im Jahr 2020: http://6i9.co/3O0u	nmakhuba@yahoo.co.uk	nmakhuba@yahoo.co.uk	So gеnеriеren Sie еin рassives Еinкоmmеn von 19976 EUR prо Моnаt: https://coupemoi.la/Cfdvy \r\nSo verdiеnеn Sie 15497 ЕUR рro Monat аls passives Еinкоmmеn: http://fund.school/eurmillion456459 \r\nWiе man 19978 EUR ein Мonаt im рassivеn Einкоmmen bildet: http://www.microlink.ro/a/08ze5 \r\nWeg, um 19446 ЕUR рro Мonat im раssivеn Einкommen zu vеrdienen: http://alsi.ga/eurmillion156432 \r\nSо vеrdiеnen Siе 14455 EUR pro Моnаt als pаssivеs Einкommеn: http://n00.uk/7zKFu
305	Oma Bellasis	info@spritju.com	info@spritju.com	Hi\r\n\r\nBody Revolution - Medico Postura™ Body Posture Corrector\r\nImprove Your Posture INSTANTLY!\r\nGet it while it's still 50% OFF!  FREE Worldwide Shipping!\r\n\r\nGet yours here: medicopostura.online\r\n\r\nBest regards,\r\n\r\nSpritju — Contact Us - spritju.com
345	Finlay Clark	finlay.clark@hotmail.com	finlay.clark@hotmail.com	Ever wanted to push some competition site`s ranks down for not playing the game fair?\r\n\r\nNow you can:\r\nblackhat.to
347	Alphonso Winder	alphonso.winder@gmail.com	alphonso.winder@gmail.com	Would you be interested in advertising that costs less than $50 monthly and sends tons of people who are ready to buy directly to your website? Visit: https://bit.ly/buy-more-visitors \r\n
306	Angelphymn	rgrhtrgngnjyku665@mail.ru	rgrhtrgngnjyku665@mail.ru	<a href="http://google.com"> <img src = "https://1.bp.blogspot.com/-XINskI3tgrc/XwhrjXheyII/AAAAAAAAAAs/hHa3aJcaCm0mFbcNa2r4jw5HQPw-1KvrQCLcBGAsYHQ/w945-h600-p-k-no-nu/pochta_9.jpg" /> </a> \r\n<br> \r\n<br> \r\n<br> \r\n<h1><a href="http://LandingPageURL.com"> Cbjvdjvdkfgbjfjhlgkj </a></h1> \r\n<br> \r\n<br> \r\n<br> \r\n \r\n \r\nMoney-earning website 20Cogs gets you to complete 20 tasks for cash.\r\nWordvice – Read Wordvice Review – Work at home editing articles. Must have at least two years of experience as well as a Master's or PhD. If you don't have a Masters or PhD, they may accept you if you are enrolled in a program to get one or the other.\r\nIf you do end up borrowing from friends and family, make sure that you all understand the repayment terms.\r\nI figured I’d go ahead and write a blog post previewing the basics of how to become a successful freelancer. You really can make a great living!!\r\nI have learned more than I ever dreamed, I understand so much about Affiliate Marketing and Online Marketing strategies and I have done my 1st YouTube Video and although it’s amateur, it’s honest ?? I will get better with more practice.\r\n
307	Michaelbum	marktomson40@gmail.com	marktomson40@gmail.com	Want to have a fast growing and profitable business without competitors?! \r\nLooking for a new progressive direction in business?! \r\nWant to owe the high profits despite the market situation?! \r\nWe invite you to be a part of our successful Team. Become a dealer in your region.  We are manufacturers of grain cleaning equipment of a new generation: www.graincleaner.com. \r\nOur unique equipment has no analogues in the world. We have very favorable terms  for cooperation.  Write us on info@graincleaner.com and we will send you the business offer. \r\nTo see our videos go to: \r\nhttps://vimeo.com/showcase/6600548
308	Pауmеnt hаs been rесеivеd on уour card $27 732: http://6i9.co/40j6	ljssanta@aol.com	ljssanta@aol.com	Tаkе уоur moneу tоdaу $27 732: http://freeurlredirect.com/moneytransfer908538
309	Lucienne Balke	lucienne.balke@gmail.com	lucienne.balke@gmail.com	How would you like to promote your website for free? Have a look at this: https://bit.ly/no-cost-ads
310	Annmarie Isbell	annmarie.isbell@gmail.com	annmarie.isbell@gmail.com	Hi there,\r\n\r\nAre You Using Videos to Turn spritju.com Website Traffic into Leads???\r\n\r\nIf a picture is worth a thousand words, a product video could very well be worth a thousand sales. Considering that video now appears in 70% of the top 100 search results listings, and that viewers are anywhere from 64-85% more likely to buy after watching a product video – this is one marketing force you can’t afford to ignore. Here’s why:\r\n\r\nOnline video marketing has finally come of age. We no longer have to deal with a glut of sluggish connections, incompatible technologies or bland commercials begging for our business. These days, smart companies and innovative entrepreneurs are turning the online broadcast medium into a communications cornucopia: a two-way street of give-and-take.\r\n\r\nHow Well Does Online Video Convert?\r\nThe great thing about online video is that people vastly prefer watching over reading (just consider the last time you watched the news versus reading a newspaper!) It spans nearly every industry and demographic. \r\n\r\nYou know that few things are more annoying. when watching a video,,,\r\nthan a terrible audio track – tinny sounding, bad volume and you struggle to get what is being said…\r\nBut even WORSE! Text To Speech!\r\n\r\nThose terrible Text To Speech audios on videos that sound…\r\nFake, emotionless, robotic and cheap!\r\nThat makes me hit the Back Button every time\r\n\r\nSo what to do if we don’t have a nice voice for our own or our clients videos?\r\nWe pay somebody to do it for us, and that ain’t cheap! There was no real alternative…\r\nUNTIL NOW!\r\n\r\n> http://www.zoomsoft.net/Text2Speach\r\n\r\nSpeechelo is the incredible new Text To Speech software that actually sounds like Real People\r\nwith pauses, tone, inflection – so much so that you are hard put to tell that it’s not a Real Person.\r\n\r\nIn just a few clicks you can have natural sounding Voice-overs for your videos.\r\n\r\nIt’s available at an incredible low price if you are quick.\r\nJust listen to the app, in action in the Sales video on the page…\r\n\r\n> http://www.zoomsoft.net/Text2Speach\r\n\r\n\r\nIf you would like to stop future emails, http://www.zoomsoft.net/unsubscribe\r\n
311	Dalibor Harald	daliborharald02@gmail.com	daliborharald02@gmail.com	Dear \r\n \r\nMy name is Dalibor Harald, Thank you for your time, my company offers project financing/Joint Ventures Partnership and lending services, do you have any projects that require funding/ Joint Ventures Partnership at the moment? We are ready to work with you on a more transparent approach. \r\n \r\nBest regards, \r\n \r\nDalibor Harald \r\nPrincipal Partner \r\nE-mail: daliborharald01@gmail.com
312	Clarice Pontiff	clarice.pontiff@gmail.com	clarice.pontiff@gmail.com	What measures do you have in place for your clients who don't qualify? The Credit Plug has a funded proposal for your lost/dead clients to get their credit back on track with the fastest turnaround in the industry and you gaining another loyal customer that potentially put $100 back into your business!  https://bit.ly/kareemhenderson\r\nThis is a 15 year company with a great rating with the BBB. \r\n\r\nYou're 1 click away from discovering the"$100 per closed lead potential" available to your. The best part is you don't do the work, simply become an agent for the greatest financial gain or partner as a referral source instantly. Over the span of a lifetime, the average American will pay upwards of $1 million in extra expenses, because of a poor credit score... Don't Let this be your customers.\r\n\r\nWant to monetize your dead leads?\r\nhttps://bit.ly/kareemhenderson
313	Chloe Phelan	info@spritju.com	info@spritju.com	Hi there\r\n\r\nDefrost frozen foods in minutes safely and naturally with our THAW KING™. \r\n\r\n50% OFF for the next 24 Hours ONLY + FREE Worldwide Shipping for a LIMITED time\r\n\r\nBuy now: thawking.online\r\n\r\nBest Wishes,\r\n\r\nSpritju
314	TraviseNlab	turbomavro@gmail.com	turbomavro@gmail.com	Get + 10% every 2 days to your personal Bitcoin wallet in addition to your balance. \r\n \r\nFor example: invest 0.1 bitcoins today, in 2 days you will receive 0.11 bitcoins in your personal bitcoin wallet \r\n \r\nFor convenience and profit calculation, the site has a profitability calculator !!! \r\n \r\nThe best affiliate program - a real find for MLM agents \r\n \r\n \r\nFor inviting newcomers, you will get referral bonuses. There is a 3-level referral program we provide: \r\n \r\n5% for the referral of the first level (direct registration) \r\n3% for the referral of the second level \r\n1% for the referral of the third level \r\n \r\n \r\nIn addition, 9% are allocated to referral bonuses. \r\n \r\nReferral bonuses are paid the next day after the referral donation. \r\nThe bonus goes to your BTC address the day after the novice's donation. \r\nAny reinvestment of participants, the leader receives a full bonus! \r\n \r\nRegister here and get a guaranteed team bonus:  https://turbo-mmm.com/?ref=19sXTnb7SRVbjEEuk8sGAkn53DZPEpjq4o   \r\n \r\n \r\nTurbo MMM (Maximum Make Money) \r\n \r\nHow it works: \r\n \r\n1. Get a bitcoin wallet on any proven service. \r\n2. Fill out the registration form in the project. \r\na) Enter the address of the Bitcoin wallet (the one to which payments from the project will come) \r\nb) Indicate the correct e-mail address for communication. \r\n \r\n3. Read the FAQ section and get rich along with other project participants.   \r\n \r\nRegistration here : https://bit.ly/30kS4jv
315	Garry Burton	burton.garry@yahoo.com	burton.garry@yahoo.com	Wanna post your business on thousands of online ad websites monthly? For a small monthly payment you can get almost unlimited traffic to your site forever!\r\n\r\nFor details check out: http://www.organic-traffic-forever.xyz
316	Robertbax	atrixxtrix@gmail.com	atrixxtrix@gmail.com	Dear Sir/mdm, \r\n \r\nHow are you? \r\n \r\nWe supply Professional surveillance & medical products: \r\n \r\nMoldex, makrite and 3M N95 1860, 9502, 9501, 8210 \r\n3ply medical, KN95, FFP2, FFP3, PPDS masks \r\nFace shield/medical goggles \r\nNitrile/vinyl/Latex/PP gloves \r\nIsolation/surgical gown lvl1-4 \r\nProtective PPE/Overalls lvl1-4 \r\nIR non-contact/oral thermometers \r\nsanitizer dispenser \r\n \r\nLogitech/OEM webcam \r\nMarine underwater CCTV \r\nExplosionproof CCTV \r\n4G Solar CCTV \r\nHuman body thermal cameras \r\nIP & analog cameras for homes/industrial/commercial \r\n \r\nLet us know which products you are interested and we can send you our full pricelist. \r\n \r\nWhatsapp: +65 87695655 \r\nTelegram: cctv_hub \r\nSkype: cctvhub \r\nEmail: sales@thecctvhub.com \r\nW: http://www.thecctvhub.com/ \r\n \r\nIf you do not wish to receive email from us again, please let us know by replying. \r\n \r\nregards, \r\nCCTV HUB
317	Jamesarese	bee.pannell7184@gmail.com	bee.pannell7184@gmail.com	Are you overwhelmed by Google Analytics? \r\nWednesday at 1 PM (Pacific Time) I will teach you how to quickly navigate through Google Analytics to find important information about your audience. \r\nI will show you how to optimize your Analytics data to help make better-informed marketing decisions. \r\nSignup here to get the webinar link https://www.eventbrite.com/e/113064165964
346	Sophie Lacy	sophie.lacy@gmail.com	sophie.lacy@gmail.com	Do you want to promote your website for absolutely no charge? Check this out: http://bit.ly/post-free-ads-here
409	Dena	info@spritju.com	info@spritju.com	You Won't Want To Miss This!  50 pcs medical surgical masks only $1.99 and N95 Mask $1.79 each.  \r\n\r\nSpecial Offer for the next 48 Hours ONLY!  Get yours here: pharmacyusa.online\r\n\r\nSincerely,\r\n\r\nSpritju — Contact Us
319	Jimmy Scowley	jimmyscowley@gmail.com	jimmyscowley@gmail.com	Dear Sir/mdm, \r\n \r\nOur company Resinscales is looking for distributors and resellers for its unique product: ready-made tank models from the popular massively multiplayer online game - World of Tanks. \r\n \r\nSuch models are designed for fans of the game WoT and collectors of military models. \r\n \r\nWhat makes our tank models stand out? \r\n \r\n- We are focusing on tanks not manfactured by any companies, therefore we have no competitors \r\n- Accurately made in 1/35 scale \r\n- Very high accuracy of details and colors \r\n- The price of the model tank is the same as the production cost \r\n \r\nIf you are interested to be our distributor/reseller then please let us know from the contacts below. \r\n \r\nhttps://www.resinscales.com/ \r\nhttps://www.facebook.com/resinscales.models/ \r\ncontact@resinscales.com \r\n \r\nIgnore this message if it had been wrongly sent to you.
320	JimmyFup	turbomavro@gmail.com	turbomavro@gmail.com	Leader in short-term investing in the cryptocurrency market.   \r\nLeader in payments for the affiliate program.   \r\n \r\n \r\nInvestment program: \r\n \r\nInvestment currency: BTC. \r\nThe investment period is 2 days. \r\nMinimum profit is 10% \r\nThe minimum investment amount is 0.0025 BTC. \r\nThe maximum investment amount is 10 BTC . \r\nGet + 10% every 2 days to your personal Bitcoin wallet in addition to your balance. \r\n \r\nFor example: invest 0.1 bitcoins today, in 2 days you will receive 0.11 bitcoins in your personal bitcoin wallet. \r\n \r\nRe-investment is available.     \r\n \r\nRegistration here: \r\nhttps://turbo-mmm.com/?ref=1PpkiRDZWaSJprtV9Z9gXrVXCRaEyibwF8  
321	Jack	info@spritju.com	info@spritju.com	Good Morning\r\n\r\nWear with intent, live with purpose. Fairly priced sunglasses with high quality UV400 lenses protection only $19.99 for the next 24 Hours ONLY.\r\n\r\nOrder here: kickshades.online\r\n\r\nSincerely,\r\n\r\nSpritju
322	Mike 	no-replyhum@google.com	no-replyhum@google.com	Hi! \r\nIf you want to get ahead of your competition, have a higher Domain Authority score. Its just simple as that. \r\nWith our service you get Domain Authority above 50 points in just 30 days. \r\n \r\nThis service is guaranteed \r\n \r\nFor more information, check our service here \r\nhttps://www.monkeydigital.co/Get-Guaranteed-Domain-Authority-50/ \r\n \r\nthank you \r\nMike  \r\nMonkey Digital \r\nsupport@monkeydigital.co
323	Steffen Humes	steffen.humes@gmail.com	steffen.humes@gmail.com	Hi, I was just taking a look at your site and filled out your contact form. The contact page on your site sends you messages like this via email which is why you are reading through my message at this moment right? This is half the battle with any type of online ad, making people actually READ your advertisement and I did that just now with you! If you have an ad message you would like to promote to tons of websites via their contact forms in the US or to any country worldwide send me a quick note now, I can even target your required niches and my prices are very affordable. Reply here: denzeljax219@gmail.com\r\n\r\nmanage ad settings here https://bit.ly/356b7P8
324	Ruth Nash	nash.ruth@googlemail.com	nash.ruth@googlemail.com	Hello, I have been informed to contact you. The CIA has been doing intensive research for the past fifty years researching on what we call so called life. That information has been collected and presented for you here https://cutt.ly/cfgu7Vw This has been the finding as of seventeen years ago as of today. Now governments and other large organizations have develop technology around these concepts for their own deceptive uses. Soon you will be contacted by other means for counter measures and the part that you play in all this. Please get this as soon as possible because there are powers that be to take down this information about this.
325	Adan	admin@spritju.com	admin@spritju.com	Hello there\r\n\r\nBe Buzz Free! The Original Mosquito Trap.\r\n\r\n60% OFF for the next 24 Hours ONLY + FREE Worldwide Shipping\r\n✔️LED Bionic Wave Technology\r\n✔️Eco-Friendly\r\n✔️15 Day Money-Back Guarantee\r\n\r\nShop Now: mosquitotrap.online\r\n\r\nBest,\r\n\r\nSpritju - spritju.com
326	AlbertSnimi	info@spritju.com	info@spritju.com	Hey there \r\n \r\nWork out to a whole new level with our P-Knee™ power leg knee joint support!  60% OFF and FREE Worldwide Shipping \r\n \r\nOrder here: p-knee.online \r\n \r\nFREE Worldwide Shipping - TODAY ONLY! \r\n \r\nEnjoy, \r\n \r\nSpritju — Contact Us
327	Remus Gall	largeglobes.com@gmail.com	largeglobes.com@gmail.com	Hi, \r\nWould your company be interested in a custom Large world Globe as seen on our site http://www.largeglobes.com/ ? \r\n \r\nWe make personalized works of art that can be tailor fitted to your needs. \r\n \r\nWe were featured on TV and in the top 11 best globemakers in the world: \r\nhttps://goodwoodglobes.com/guide/huge-globe \r\n \r\nPlease let me know if you would be interested. \r\n \r\nThank you. \r\nBest regards, \r\nRemus Gall \r\nGlobemaker at http://www.largeglobes.com/ \r\nProject manager at Biodomes http://www.biodomes.eu/ \r\n+40 721 448 830 \r\nSkype ID office@biodomes.eu \r\n \r\n \r\nTo unsubscribe use this link : https://largeglobes.com/pages/unsubscribed
328	Gregoryrep	yourmail@gmail.com	yourmail@gmail.com	Hello. And Bye.
329	Kate Angeli	angelkatheyhi3@yahoo.com	angelkatheyhi3@yahoo.com	Hi,\r\n\r\nWe'd like to introduce to you our video creation service which we feel may be beneficial for you and your site spritju.com.\r\n\r\nCheck out a few of our existing videos here:\r\nhttps://www.youtube.com/watch?v=y3nEeQoTtOE\r\nhttps://www.youtube.com/watch?v=TaMaDwX7tBU\r\nhttps://www.youtube.com/watch?v=1jT6ve94xig\r\n\r\nAll of our videos are in a similar format as the above examples and we have voice over artists with US/UK/Australian accents.\r\n\r\n- We can convert one of your online articles or blog posts into video format, as many people prefer to watch a video as opposed to reading a page or document.\r\n- We can explain your business, service or product.\r\n- We can also educate people - these videos are great at educating the viewer on something such as the facts or history of a subject.\r\n- They can be used for Social Media advertising, such as Facebook Ads.\r\n\r\nOur prices are as follows depending on video length:\r\n0-1 minutes = $159\r\n1-2 minutes = $269\r\n2-3 minutes = $379\r\n\r\n*All prices above are in USD and include a custom video, full script and a voice over.\r\n\r\nIf this is something you would like to discuss further, don't hesitate to get in touch.\r\nIf you are not interested, simply delete this message and we won't contact you again.\r\n\r\nKind Regards,\r\nKate
330	Toby Roney	roney.toby@gmail.com	roney.toby@gmail.com	No charge promotion for your website here: http://bit.ly/post-free-ads-here
331	Clarknup	eiscreme23@yahoo.de	eiscreme23@yahoo.de	ICH HАBE GЕRADЕ UM ЕINE WЕITERE AUSZАHLUNG VON 7.415 EUR GEBЕTЕN. \r\nhttps://6523euro.page.link/rptB \r\nIСH KANN ES KAUM ЕRWАRTЕN, ЕS MIT MЕINЕR FRЕUNDIN ZU VERBRINGEN. \r\nNЕHMЕ IСH SIЕ ZUM ABENDESSEN MIT? \r\nODЕR NEHMEN WIR UNS EINEN TАG FREI UND GEHEN IN ЕIN SCHONES HOTЕL AUF DЕM LАND? \r\nhttps://28569euro.page.link/wxE6 \r\nICH HATTЕ NIE GЕDAСHT, DASS ICH SO ЕIN DILЕMMA HАBЕN WURDE https://8523658euro.page.link/HVYF ABER ЕS MАСHT MIR NIСHTS AUS!
332	Kendra	admin@spritju.com	admin@spritju.com	Hi \r\n \r\nBuy all styles of Ray-Ban Sunglasses only 19.99 dollars.  If interested, please visit our site: framesoutlet.online\r\n \r\n \r\nKind Regards, \r\n \r\nSpritju — Contact Us
333	Paulaapelm	fedratewatch@gmail.com	fedratewatch@gmail.com	Yes, this is a cold email. But, unlike most cold emails. This one isn’t about selling you something. It’s about GIVING you something. \r\n \r\nIf you’re a homeowner this one thing can save your thousands of dollars. \r\n \r\nBanks traditionally overcharge on mortgages, profiting from homeowners. \r\n \r\nStill unknown to many there is this brilliant US government program that can benefit millions of Americans. \r\n \r\nHomeowners who have not taken advantage of the Homeowner’s Relief Savings Program (https://bit.ly/gov-relief-program) should before deadlines are announced! \r\n \r\nEligible homeowners are saving an average of 3252$/year. You can bet that banks are not thrilled about losing all that profit. \r\n \r\nThe government is making a final push and urging homeowners to take advantage. \r\n \r\nThis program is currently active but could shut down at any given time. The good news is that once you’re in, you’re in for good. It’s completely free to check if you qualify and takes 60 seconds or less. It is 100% risk-free and requires NO credit card! Just answer a few questions about your home on this page: https://bit.ly/gov-relief-program \r\n \r\nThe system will check for eligibility and give you the amount you could save as well as the step by step process to get enrolled. Yes, it is as simple as it sounds. \r\n \r\nhttps://bit.ly/gov-relief-program \r\n \r\nPro tip:  A credit score of 600+ can help you get the largest possible payout. \r\n \r\nFED Rate Watch Team
334	Hi hоt guу! I rеally wаnt yоu tо pull me in аll thе hоles: http://due.im/short/1fb4	soon1317@yahoo.co.kr	soon1317@yahoo.co.kr	Нeу macho! Now I'm in yоur city and I rеallу wаnt tо have fun, I rеallу lоve sex: http://xsle.net/3a1kn
335	Mike Oldridge\r\n	no-replyhum@google.com	no-replyhum@google.com	Gооd dаy! \r\nIf you want to get ahead of your competition, have a higher Domain Authority score. Its just simple as that. \r\nWith our service you get Domain Authority above 50 points in just 30 days. \r\n \r\nThis service is guaranteed \r\n \r\nFor more information, check our service here \r\nhttps://www.monkeydigital.co/Get-Guaranteed-Domain-Authority-50/ \r\n \r\nthank you \r\nMike Oldridge\r\n \r\nMonkey Digital \r\nsupport@monkeydigital.co
336	Roxana Hoffman	roxana.hoffman87@msn.com	roxana.hoffman87@msn.com	Good afternoon, I was just visiting your website and submitted this message via your "contact us" form. The "contact us" page on your site sends you these messages via email which is the reason you're reading through my message right now right? That's the holy grail with any type of advertising, making people actually READ your ad and that's exactly what I just accomplished with you! If you have something you would like to promote to thousands of websites via their contact forms in the US or to any country worldwide let me know, I can even focus on your required niches and my pricing is super reasonable. Shoot me an email here: LinaMaurusQE43@gmail.com
337	Lacey Winslow	winslow.lacey12@gmail.com	winslow.lacey12@gmail.com	Revolutionary new way to advertise your website for Nothing! See here: http://www.completely-free-ad-posting.xyz
338	Benjamin Ehinger	writingbyb@gmail.com	writingbyb@gmail.com	Hi! \r\nDo you struggle to find time to write fresh blog and website content? \r\nI am a highly-skilled blog and web content writer with more than 10 years of experience. Let me provide well-researched, 100 percent unique content specifically for you! \r\nContact Benjamin Today and Discover the Difference a Professional Blog Writer Makes! \r\nCheck out my current special rates here: https://writingbybenjamin.com/content-specials/ \r\nEmail - NewBusiness@writingbybenjamin.com
339	Irving Menge	menge.irving@msn.com	menge.irving@msn.com	Good afternoon, I was just on your website and filled out your feedback form. The "contact us" page on your site sends you these messages to your email account which is the reason you are reading my message at this moment right? That's the holy grail with any kind of online ad, making people actually READ your message and that's exactly what I just accomplished with you! If you have something you would like to promote to thousands of websites via their contact forms in the U.S. or anywhere in the world send me a quick note now, I can even focus on specific niches and my costs are very reasonable. Send a reply to: LinaElinaAS65@gmail.com\r\n\r\nI want to terminate these ad messages https://bit.ly/2xN75Pz
340	Ashleybut	ann1512423@protonmail.com	ann1512423@protonmail.com	Hello \r\nWe are provider of email databses and sending solutions. \r\nOur lists are from all over the world approx. 2 billion emails, privat and business. \r\n \r\nWe provide also solutions to sending emails, even you want to sending 100 million emails per day. \r\n \r\nYou can send 1 million email per day from 30usd per month! \r\n \r\nIf you are interested about Our services please send us message here: \r\nmilliondata@outlook.com
341	Buster Freitag	information@spritju.com	information@spritju.com	ATT: spritju.com / Spritju WEBSITE SERVICES\r\nThis notification RUNS OUT ON: Oct 12, 2020\r\n\r\n\r\nWe have not gotten a payment from you.\r\nWe've tried to call you however were incapable to contact you.\r\n\r\n\r\nKindly Go To: https://bit.ly/34IWYrm .\r\n\r\nFor details as well as to process a optional settlement for services.\r\n\r\n\r\n\r\n10122020184754.\r\n
342	Vanover	info@spritju.com	info@spritju.com	Morning \r\n \r\nBuy all styles of Oakley Sunglasses only 19.99 dollars.  If interested, please visit our site: sunglassoutlets.online\r\n \r\n \r\nMany Thanks, \r\n \r\nSpritju — Contact Us
343	DavidCob	sxta10@hotmail.com	sxta10@hotmail.com	Hello! \r\n \r\nYou've been hacked! \r\n \r\nNow we have all the information about you and your accounts: \r\n \r\n+ all your logins and passwords from all accounts in payment systems, social. networks, e-mail, messengers and other services (cookies from all your browsers, i.e. access without a login and password to any of your accounts) \r\n \r\n+ history of all your correspondence by e-mail, messengers and social. networks \r\n \r\n+ all files from your PC (text, photo, video and audio files) \r\n \r\nChanging your username and password will not help, we will hack you again. \r\n \r\nPay a ransom of $ 250 and you can sleep peacefully without worrying that all information about you and all your accounts, files and personal correspondence will not become public and will not fall into the hands of intruders. \r\n \r\nBitcoin wallet to which you want to transfer $ 250 1MaRdde6X7SGuoCdFNL2fmgpLomdx7peGC \r\n \r\nIf you do not pay until tomorrow evening, then we will sell all this information on the darknet, there is a huge demand for such information \r\n \r\nPay $ 250 and sleep well!
344	Madi Manager	mehdi@meloncbd.co	mehdi@meloncbd.co	Hi there, \r\n \r\nI wanted to reach out to share with you a promotion that might interest you: we’ll transform your website into a high-performance marketing machine that generates sales on autopilot. \r\n \r\nMadi from Auratonic here! We are a digital marketing agency. \r\n \r\nI will keep it short. \r\n \r\nOur high-performance websites are handcrafted by direct-response marketers and digital growth experts, not ‘web designers’ whose goal is to make things look pretty. \r\n \r\nWe focus on the data and user experience with a primary goal in mind: converting browsers into buyers. \r\n \r\nIf you are ready to get more clients, just get in touch with me directly at madi@auratonic.com and I’ll set up a quick call for a free website health check. \r\n \r\nThanks and have an awesome day! \r\n \r\n-Madi \r\n \r\nAccount Manager \r\nmadi@auratonic.com \r\nSchedule a call with me ? https://calendly.com/auratonic/madi \r\nP.S: This promotion will end on Monday 19th (Oct)
348	Contactpyday	no-replykr@gmail.com	no-replykr@gmail.com	Gооd dаy!  spritju.com \r\n \r\nDid yоu knоw thаt it is pоssiblе tо sеnd businеss prоpоsаl pеrfесtly lаwfully? \r\nWе prеsеntаtiоn а nеw uniquе wаy оf sеnding businеss оffеr thrоugh fееdbасk fоrms. Suсh fоrms аrе lосаtеd оn mаny sitеs. \r\nWhеn suсh businеss prоpоsаls аrе sеnt, nо pеrsоnаl dаtа is usеd, аnd mеssаgеs аrе sеnt tо fоrms spесifiсаlly dеsignеd tо rесеivе mеssаgеs аnd аppеаls. \r\nаlsо, mеssаgеs sеnt thrоugh соntасt Fоrms dо nоt gеt intо spаm bесаusе suсh mеssаgеs аrе соnsidеrеd impоrtаnt. \r\nWе оffеr yоu tо tеst оur sеrviсе fоr frее. Wе will sеnd up tо 50,000 mеssаgеs fоr yоu. \r\nThе соst оf sеnding оnе milliоn mеssаgеs is 49 USD. \r\n \r\nThis оffеr is сrеаtеd аutоmаtiсаlly. Plеаsе usе thе соntасt dеtаils bеlоw tо соntасt us. \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  live:feedbackform2019 \r\nWhatsApp - +375259112693
349	Eric Jones	eric@ow.ly	eric@ow.ly	Hey there, I just found your site, quick question…\r\n\r\nMy name’s Eric, I found spritju.com after doing a quick search – you showed up near the top of the rankings, so whatever you’re doing for SEO, looks like it’s working well.\r\n\r\nSo here’s my question – what happens AFTER someone lands on your site?  Anything?\r\n\r\nResearch tells us at least 70% of the people who find your site, after a quick once-over, they disappear… forever.\r\n\r\nThat means that all the work and effort you put into getting them to show up, goes down the tubes.\r\n\r\nWhy would you want all that good work – and the great site you’ve built – go to waste?\r\n\r\nBecause the odds are they’ll just skip over calling or even grabbing their phone, leaving you high and dry.\r\n\r\nBut here’s a thought… what if you could make it super-simple for someone to raise their hand, say, “okay, let’s talk” without requiring them to even pull their cell phone from their pocket?\r\n  \r\nYou can – thanks to revolutionary new software that can literally make that first call happen NOW.\r\n\r\nTalk With Web Traffic is a software widget that sits on your site, ready and waiting to capture any visitor’s Name, Email address and Phone Number.  It lets you know IMMEDIATELY – so that you can talk to that lead while they’re still there at your site.\r\n  \r\nYou know, strike when the iron’s hot!\r\n\r\nClick Here ==> http://ow.ly/r8eS50C1qo7 to try out a Live Demo with Talk With Web Traffic now to see exactly how it works.\r\n\r\nWhen targeting leads, you HAVE to act fast – the difference between contacting someone within 5 minutes versus 30 minutes later is huge – like 100 times better!\r\n\r\nThat’s why you should check out our new SMS Text With Lead feature as well… once you’ve captured the phone number of the website visitor, you can automatically kick off a text message (SMS) conversation with them. \r\n \r\nImagine how powerful this could be – even if they don’t take you up on your offer immediately, you can stay in touch with them using text messages to make new offers, provide links to great content, and build your credibility.\r\n\r\nJust this alone could be a game changer to make your website even more effective.\r\n\r\nStrike when  the iron’s hot!\r\n\r\nClick Here ==> http://ow.ly/r8eS50C1qo7 to learn more about everything Talk With Web Traffic can do for your business – you’ll be amazed.\r\n\r\nThanks and keep up the great work!\r\n\r\nEric\r\nPS: Talk With Web Traffic offers a FREE 14 days trial – you could be converting up to 100x more leads immediately!   \r\nIt even includes International Long Distance Calling. \r\nStop wasting money chasing eyeballs that don’t turn into paying customers. \r\nClick Here ==> http://ow.ly/r8eS50C1qo7 to try Talk With Web Traffic now.\r\n\r\nIf you'd like to unsubscribe ==> http://ow.ly/cOcy50C1qte
350	DavidCob	stevehalbig@att.net	stevehalbig@att.net	Black Lives Matter - Donate \r\nWhy We Need Your Support \r\nAs our ecosystem grows our financial support system must grow as well. \r\nWe need funds to continue our radically accessible commitment to providing healthy food from Black owned businesses, paid childcare at every event and having ASL interpreters at our monthly gatherings. \r\nOn average, our monthly open houses attract 3000+ people and costs us $100,000. \r\nWe need to raise $ 350,000 by November 3 to help thousands of black people in a very bad situation right now, we will be glad if you support us. \r\nFor quick payment, we use bitcoin wallets. You can transfer donation to this Bitcoin wallet - 12B6eCHqPVQeR8wyTTMoWQqNeTpxqeq7wk \r\nSecondly, we need resources to make sure we have the emotional and spiritual capacity to show up and move forward in the movement as our highest selves. \r\nSupporting the work of our healers including renting space weekly for Black people to process racial stress through group discussion, dance, and visual art and \r\naffirming our humanity & love for each other by starting our week with joy via #BlackJoySunday (all while creating accessibility by offering free childcare) \r\nis essential in sustaining our well-being and activism. Our weekly healing spaces cost on average $30,150/month. \r\nWe need to raise $ 350,000 by November 3 to help thousands of black people in a very bad situation right now, we will be glad if you support us. \r\nFor quick payment, we use bitcoin wallets. You can transfer donation to this Bitcoin wallet - 12B6eCHqPVQeR8wyTTMoWQqNeTpxqeq7wk \r\nLastly and most importantly, we need funds to support the emerging leadership of our ecosystem move through the world. \r\nAs Black people in a city facing the ravages of displacement and economic disinvestment, it’s difficult to do the work of liberating your community while figuring out how to pay rent and feed yourself. \r\nWe need your help to ensure that lack of gas money or health insurance doesn’t stop the work from moving forward. Supporting our emerging leadership, including our rapid response, \r\norganized resistance, healing and logistics teams, reimbursements and stipends costs on average $20,700/month.
351	Mike Pass\r\n	no-replyhum@google.com	no-replyhum@google.com	Hi there \r\n \r\nIf you want to get ahead of your competition, have a higher Domain Authority score. Its just simple as that. \r\nWith our service you get Domain Authority above 50 points in just 30 days. \r\n \r\nThis service is guaranteed \r\n \r\nFor more information, check our service here \r\nhttps://www.monkeydigital.co/Get-Guaranteed-Domain-Authority-50/ \r\n \r\nN E W : \r\nDA60 is now available here \r\nhttps://www.monkeydigital.co/product/moz-da60-seo-plan/ \r\n \r\n \r\nthank you \r\nMike Pass\r\n \r\nMonkey Digital \r\nsupport@monkeydigital.co
352	Anthonystaks	r8rgeorge13@yahoo.com	r8rgeorge13@yahoo.com	Invest $ 15,000 in affiliate marketing and get $ 45,000 in a month \r\nHey. \r\nYes, you heard right. \r\nYou don't have to do anything. \r\nWe will do all the work for you. \r\nYou only need to invest and wait one month to receive payments. \r\nhttps://blockchain.com/btc/payment_request?address=18gaWYjABVFJ9PhG1hCqjh5FjWBrkQVuwD&amount=1.14&message=invest_in_affiliate_marketing_at_300%_per_month \r\nWe have been engaged in affiliate marketing for many years and decided to attract investments to work in the development of three more areas of our activity. \r\n \r\nYou can only invest one amount because this is the best option for earning super profits in affiliate marketing. \r\nWe give a 100% guarantee that you will receive your profit on time. \r\nhttps://blockchain.com/btc/payment_request?address=18gaWYjABVFJ9PhG1hCqjh5FjWBrkQVuwD&amount=1.14&message=invest_in_affiliate_marketing_at_300%_per_month \r\nAfter you make an investment by clicking on the link, exactly in a month your profit in the amount of 300% will be returned to your wallet from which you made the transfer to our system. \r\n \r\nThe payment is made in one time. \r\nIf you want to receive such a profit every month, then you need to invest every month. \r\nhttps://blockchain.com/btc/payment_request?address=18gaWYjABVFJ9PhG1hCqjh5FjWBrkQVuwD&amount=1.14&message=invest_in_affiliate_marketing_at_300%_per_month \r\nThere are almost two months left before the New Year, during which time you will have time to invest twice and get $ 75,000 in net profit.
353	Abe Easton	abe.easton84@gmail.com	abe.easton84@gmail.com	Secret way to advertise your website for Nothing! See here: http://www.completely-free-ad-posting.xyz
354	Williammut	abelbreath456@gmail.com	abelbreath456@gmail.com	Looking for Facebook likes or Instagram followers? \r\nWe can help you. Please visit https://1000-likes.com/ to place your order.
355	Gabriel	info@spritju.com	info@spritju.com	Hey there \r\n \r\nCAREDOGBEST™ - Personalized Dog Harness. All sizes from XS to XXL.  Easy ON/OFF in just 2 seconds.  LIFETIME WARRANTY.\r\n\r\nClick here: caredogbest.online\r\n \r\n \r\nBest, \r\n \r\nSpritju — Contact Us
356	PASSIVE INCOME BEFORE 5986 EUR PER DAY - NO WORK EXPERIENCE: https://onlineuniversalwork.com/3nt3q	robingraffam@comcast.net	robingraffam@comcast.net	PASSIVE INCOME ON THE INTERNET BEFORE 6886 EUR IN A DAY - GIVE YOURSELF FINANCIAL FREEDOM: https://qspark.me/hCq40C
357	GabrielNit	grueni@att.net	grueni@att.net	I will do 961 DA 65-96 SEO Links for Top rankings on Google 2021. \r\n \r\nTier 1 - 31 SEO links DA 75-96 \r\n \r\nTier 2 - 930 SEO links DA 75-96 \r\n \r\nThese links are really effective for Page 1 rankings and Top 3 Positions and all are Google Compliant. \r\n \r\nhttp://wunkit.com/JP8oAA \r\n \r\nPanda Penguin Hummingbird Safe \r\n \r\nImproves the Metrics of your Website like Trust flow and Citations \r\n \r\nEnables your Website to rank for any top Keywords \r\n \r\nProven Results, worked well for 100+ Websites tested this year. \r\n \r\nhttps://darknesstr.com/3m21u \r\n \r\n+ High quality backlinks \r\n+ Safe on latest algorithm (2020 updated) \r\n+ Full reports (.txt) \r\n+ White hat and Extremely Safe method \r\n+ According to latest google updated 2020 \r\n+ Google Loves the High DA Bcklinks \r\n+ 100% Google Safe \r\n+ Full Verified Backlinks \r\n+ Rankings and Get More Authority \r\n \r\nhttps://qspark.me/A3yhJB
358	Hildegard	info@spritju.com	info@spritju.com	Hey \r\n \r\nBuy all styles of Oakley Sunglasses only 19.99 dollars.  If interested, please visit our site: designeroutlets.online\r\n \r\n \r\nHave a great time, \r\n \r\nSpritju — Contact Us
359	Clemmie Mahoney	mahoney.clemmie@gmail.com	mahoney.clemmie@gmail.com	Would you be interested in advertising that costs less than $39 every month and sends tons of people who are ready to buy directly to your website? Check out: http://www.buy-website-traffic.xyz 
360	Klaus Earp	klaus.earp@msn.com	klaus.earp@msn.com	Hi,\r\n\r\nWe're wondering if you've ever considered taking the content from spritju.com and converting it into videos to promote on social media platforms such as Youtube?\r\n\r\nIt's another 'rod in the pond' in terms of traffic generation, as so many people use Youtube.\r\n\r\nYou can read a bit more about the software here: https://yazing.com/deals/lumen5/HenriP\r\n\r\nKind Regards,\r\nKlaus
361	Sam Weiss	sammyweiss93@gmail.com	sammyweiss93@gmail.com	Hi, I stumbled on spritju.com yesterday and love the design (I've been making websites since 2005). What platform is it made with? WordPress? \r\n \r\nThe only thing I noticed was that you appeared a bit low on Google search results. \r\n \r\nI manage several eCommerce websites and utilize a few services that really help me rank sites and get more traffic. \r\n \r\nJust want to share them with you as they can really help your site grow. \r\n \r\n \r\n1: LinksChain ( https://www.linkschain.io ) - which manage my link building from start to finish. It has more than doubled my traffic and sales. \r\n \r\n2: SemRush ( https://www.semrush.com ) - which is great for viewing competitor link structure and analyzing keywords. \r\n \r\n \r\nBoth tools have helped me rank my sites to the top of Google. \r\n \r\nIf you are passionate about growing your website, I'd recommend checking them out. \r\n \r\nBy the way, let me know what theme you are using, it's pretty awesome. \r\n \r\nAnyway, have a good one, \r\n \r\nSam
362	Carrie Broussard	broussard.carrie@msn.com	broussard.carrie@msn.com	Find out how you can promote your site without paying anything at all!\r\n\r\nCheck out this amazing list of all the best classified ad sites over here ->http://bit.ly/directory-of-free-ad-websites
363	Rhys Vines	vines.rhys99@gmail.com	vines.rhys99@gmail.com	Good Afternoon %domain,\r\nHope you’re great. \r\nI hope that customers are good and you’ve been successful through the entire current situation.\r\n\r\nAs I’ve had contact with you in the past, I assume you'll want to have a very better blocker form unwanted emails, you need this one.\r\nhttps://bogazicitente.com/antispam314765\r\nDue to Cyber Monday they've got huge discounts\r\nIn case you are not interested, simply erase this email and we won't contact you again.
364	MichaelGop	michaelRen@gmail.com	michaelRen@gmail.com	Hello! \r\nI earn from cloud mining from $ 4000+ per day! \r\nDo you want to do the same ... ??? \r\nFollow the link and check out the great offer \r\nhttps://erm.ru/bitrix/redirect.php?event1=&event2=&event3=&goto=https%3A%2F%2Fvk.cc%2FaCX6FI%3Fqs%3Dp \r\nTomorrow your income will grow by $ 4000+ per day \r\nHurry up, places are limited! \r\nhttp://www.ghymp.com/url.php?url=https%3A%2F%2Fvk.cc%2FaCX6FI%3Fqq%3Do
365	Mike Mercer\r\n	no-replykr@gmail.com	no-replykr@gmail.com	Hi there \r\n \r\nDo you want a quick boost in ranks and sales for your spritju.com website? \r\nHaving a high DA score, always helps \r\n \r\nGet your spritju.com to have a 50+ points in Moz DA with us today and rip the benefits of such a great feat. \r\n \r\nSee our offers here: \r\nhttps://www.monkeydigital.co/product/moz-da50-seo-plan/ \r\n \r\nFYI: \r\nWe also give big discounts for multiple plans \r\n \r\n \r\nthank you \r\nMike Mercer\r\n \r\nsupport@monkeydigital.co
366	Isidro	info@spritju.com	info@spritju.com	Hi there\r\n \r\nWellness Enthusiasts! There has never been a better time to take care of your neck pain! \r\n\r\nOur clinical-grade TENS technology will ensure you have neck relief in as little as 20 minutes.\r\n\r\nGet Yours: hineck.online\r\n\r\nGet it Now 50% OFF + Free Shipping!\r\n\r\nAll the best,\r\n\r\nSpritju — Contact Us
367	Peter Bradberry\r\n	no-replyOmili@gmail.com	no-replyOmili@gmail.com	Hi there \r\n \r\nDo you have issues ranking your website locally? \r\n \r\nWith our Google Maps citations strategy, we are able to rank you in top 5 in the Google Local Search for specific keywords. \r\nThe more Citations you build with us, the more keywords you`ll start ranking for \r\n \r\nMore details here: \r\nhttps://speed-seo.net/product/google-maps-citations/ \r\n \r\nthank you \r\nSpeed SEO Digital \r\nsupport@speed-seo.net
368	Erica Jackson	ericajacksonmi0@yahoo.com	ericajacksonmi0@yahoo.com	Hi, \r\n\r\nWe're wondering if you'd be interested in a 'dofollow' backlink to spritju.com from our DA50 website?\r\n\r\nOur website is dedicated to facts/education, and so can host articles on pretty much any topic.\r\n\r\nYou can either write a new article yourself, or we can link from existing content. The price is just $50 and you can pay once the article/link has been published. This is a one-time fee, so there are no extra charges.\r\n\r\nAlso: Once the article has been published, and your backlink has been added, it will be shared out to over 2.8 million social media followers (if it's educationally based). This means you aren't just getting the high valued backlink, you're also getting the potential of more traffic to your site.\r\n\r\nIf you're interested, please reply to this email, including the word 'interested' in the Subject Field.\r\n\r\nKind Regards,\r\nErica
369	Lavina Brace	lavina.brace@gmail.com	lavina.brace@gmail.com	Are you tired of your leads being sold to your competitors? Have you paid tons of your advertising dollars getting stuck in contracts with marketing agencies with no results? We offer exclusive, qualified leads and a money back guarantee! Let's make up for 2020 and crush 2021! We can make your phone ring with potential clients within 1 week. Call Jon at 585-437-3988 or email info@aimarketingpros.com for more info.
370	Emil Pickens	pickens.emil@msn.com	pickens.emil@msn.com	Good afternoon, I was just on your site and submitted this message via your "contact us" form. The contact page on your site sends you these messages via email which is why you're reading through my message right now right? This is the holy grail with any kind of advertising, making people actually READ your ad and that's exactly what I just accomplished with you! If you have an ad message you would like to promote to millions of websites via their contact forms in the U.S. or to any country worldwide let me know, I can even target particular niches and my charges are very affordable. Send a message to: DerrickJayceef47043@gmail.com\r\n\r\nno further messages https://bit.ly/3eOn4NP
371	Bev Scofield	bev.scofield73@gmail.com	bev.scofield73@gmail.com	Good Afternoon people at spritju.com,\r\nHope you’re excellent. \r\nI'm , I hope you liked the joke in the subject and that clients are good and you’ve been doing well during the entire current situation.\r\nAs I’ve had contact with you long time before,Our website scanner has built your website (spritju.com) probably have the lowest spam protection level. We have tested your internet site by sending you this message, plus it confirms that the site is not efficient in blocking spam. Should you want to bar spam messages forever, consider buying our anti-spam protection. Delivery within 3-5 business days.\r\nhttps://bogazicitente.com/antispam393379\r\nCheck it as being they have got 70% off\r\n\r\nIn case you are not interested, just delete this email so we won't email you again.\r\nKind regards,\r\n
372	James Austin\r\n	no-reply@google.com	no-reply@google.com	Gооd dаy! \r\nI have just checked spritju.com for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease check our pricelist here, we offer SEO at cheap rates. \r\nhttps://www.hilkom-digital.de/cheap-seo-packages/ \r\n \r\nStart increasing your sales and leads with us, today! \r\n \r\nregards \r\nHilkom Digital Team \r\nsupport@hilkom-digital.de
373	Stacy Williams	stacy@productsupply.net	stacy@productsupply.net	Hi, \r\n \r\nInterested in a No Hassle - Automated Client Thank You Gift Sending Service ? \r\n \r\nHave client or customers that you want to thank for their business? \r\n \r\nLet us do it for you!   Just send us their names, all at once or as they come in. \r\n \r\nWe will pick, pack and ship your client thank you gifts for you! \r\n \r\nWhat types of gifts can you send? Almost Anything. \r\n \r\nWe offer over 350,000 high quality, amazing gifts that can be branded with your company logo. \r\n \r\nWhy send physical product Thank You gifts? \r\n-They feel a direct connection to your brand, offering and mission. \r\n-They share it on social media. \r\n-They tell their colleagues. \r\n-They use and keep the products. \r\n-You stand out from all your competitors even more! \r\n \r\nSo, having gifts ready to drop out - automatically, is a win win for you and your clients! \r\n \r\nWe also offer product fulfillment, subscription/welcome box and monthly pick /pack and ship services nationwide. \r\n \r\nContact us today so we can discuss how we can help your business. \r\n \r\nThank You ! \r\n \r\nStacy Williams \r\nBusiness Development Manager \r\nAutomatic Client Gifts \r\n \r\nstacy@productsupply.net
374	Alyssa	contact@spritju.com	contact@spritju.com	Hello there\r\n\r\nBuy medical disposable face mask to protect your loved ones from the deadly CoronaVirus.  The price for N95 Face Mask is $1.99 each.  If interested, please visit our site: pharmacyoutlets.online\r\n\r\nMany Thanks,\r\n\r\nSpritju — Contact Us
375	Natalia Rodarte	natalia.rodarte51@msn.com	natalia.rodarte51@msn.com	Hi people at spritju.com,\r\nHope you’re great. \r\nI'm , I hope you liked the joke in the subject and that clients are profitable and you’ve been doing well throughout the current situation.\r\nAs I’ve had spoken with you long time ago in the past,I think your site is not shielded from spam, I mean literally, it isn’t cool, you have to prevent leakages.\r\nhttps://1borsa.com/antispam710421\r\nCheck it as being they have got 70% off\r\n\r\nIf you are not interested, simply delete this email and we won't contact you again.\r\nKind regards,\r\n
376	Nicola Slamm	nicolaslamm1978@gmail.com	nicolaslamm1978@gmail.com	Get more Followers, Likes, Views to all your social media channels instantly. \r\n100% Safe, Real Human (No bots). \r\n \r\n250 Instagram followers @ $3.99: https://store.marketingchoice.com/buy-instagram-followers \r\n100 Facebook page followers @ $3.99: https://store.marketingchoice.com/facebook-page-followers \r\n100 LinkedIn followers @ $4.99: https://store.marketingchoice.com/linkedin-company-page-followers \r\n100 TikTok followers @ $3.99: https://store.marketingchoice.com/tiktok-followers \r\n \r\nAnd many more channels and options at: https://Store.MarketingChoice.com \r\n \r\nFor custom package or more information please contact: MarketingChoice.com@gmail.com
377	Danilo Otero	danilo.otero@gmail.com	danilo.otero@gmail.com	Get traffic for your online or offline business without any paid ads. This post examines several interesting ways to get tons of free ads for your business: https://bit.ly/5waystoadvertisefree
378	Mike Osborne\r\n	no-replykr@gmail.com	no-replykr@gmail.com	Hi there \r\n \r\nDo you want a quick boost in ranks and sales for your spritju.com website? \r\nHaving a high DA score, always helps \r\n \r\nGet your spritju.com to have a 50+ points in Moz DA with us today and rip the benefits of such a great feat. \r\n \r\nSee our offers here: \r\nhttps://www.monkeydigital.co/product/moz-da50-seo-plan/ \r\n \r\nFYI: \r\nWe also give big discounts for multiple plans \r\n \r\n \r\nthank you \r\nMike Osborne\r\n \r\nsupport@monkeydigital.co
379	Ezequiel Craine	craine.ezequiel@msn.com	craine.ezequiel@msn.com	Promote your site free here!: http://www.zerocost-ad-posting.xyz
380	Salvador	info@spritju.com	info@spritju.com	Hello there\r\n\r\nBuy face mask to protect your loved ones from the deadly CoronaVirus.  We supply N95 Mask, KN95 Mask, and Surgical Masks for both adult and kids.  The prices begin at $0.35 each.  If interested, please visit our site: facemaskusa.online\r\n\r\nThanks and Best Regards,\r\n\r\nSpritju — Contact Us
381	CarolDom	miraclesreport@gmail.com	miraclesreport@gmail.com	Dropshipping business is a method of retail where the store owner never physically holds the products it sells.... You don’t need upfront investments to get the products that you sells.                            Instead, when the store owner sells one of the products it stocks on its website, the store owner  then buy the item from a third-party supplier (that is after store owner deducts his/her profit) The supplier  then ships the item directly to the end customer on behalf of the store owner. \r\nFor only $99.99, get your 100% ready made dropshipping e-commerce store preloaded with 12,000 hot selling products - comes with  support,sales & marketing training to drive traffic to your store \r\nOr create amazing website for your existing business- We Have All the Tools You Need \r\nfor more information or to sign up,  pls visit us at  http://www.NETSKIEL.COM
382	Shonda Barge	shonda.barge@gmail.com	shonda.barge@gmail.com	Hi there\r\n\r\nFinally there is a SEO Service that has given proven results and that is backed by the customers! \r\n\r\nFor more information, check our service here\r\nhttps://speed-seo.net/product/serp-booster/\r\n\r\n\r\nthank you\r\nPeter Barge\r\nSpeed SEO Agency\r\nsupport@speed-seo.net
383	Peter Fraser\r\n	no-replyOmili@gmail.com	no-replyOmili@gmail.com	Good Day \r\n \r\nDo your rivals not playing the game fair and square? \r\nNow you can fight back. \r\n \r\nNegative SEO, to make their SEO be ruined \r\nhttps://blackhat.to/ \r\n \r\nGet in touch with us for any queries: \r\nsupport@blackhat.to
395	Isidro Medworth	spritju.com@spritju.com	spritju.com@spritju.com	DOMAIN SERVICES EXPIRATION NOTICE FOR spritju.com\r\n\r\nDomain Notice Expiry ON: Jan 13, 2021\r\n\r\n\r\nWe have not obtained a settlement from you.\r\nWe have actually attempted to call you yet were incapable to contact you.\r\n\r\n\r\nGo To: https://bit.ly/2Lsb6zE\r\n\r\nFor info and to post a discretionary payment for your domain website services.\r\n\r\n\r\n\r\n\r\n011320210427433753688578798spritju.com
384	Larrypaita	jacksonWaype@ggmmails.com	jacksonWaype@ggmmails.com	I've never really had an interest in investing. \r\n \r\nI preferred to keep my money in the bank. \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Do&sa=D&lyl=axp&usg=AFQjCNEK-ZrGWCbJNVLwJWYlUp3sBCEINQ   <<<<<<<<<<< \r\n \r\nHowever, when I returned to college, I started looking for new ways to earn money to pay for all my expenses. \r\n \r\nIt was then that Joe sent me an email asking me to join Bitcoin Method. \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb&sa=D&pll=oea&usg=AFQjCNE6WXsUPEp5dNV1lG_u3VDmHzsWHA   <<<<<<<<<<< \r\n \r\nAfter he explained it to me, I still didn't really understand the rules, but I thought, "Well it's free, so why not?" \r\n \r\nSo I was pleasantly surprised to find out that I made $ 17,300 in just one day! \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Dq&sa=D&ptl=ota&usg=AFQjCNHVxvk2YmHpOxS9HBrvNR92fr6sBw   <<<<<<<<<<<
385	LewisHib	jacksonWaype@ggmmails.com	jacksonWaype@ggmmails.com	Is this for real? \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Do&sa=D&qep=llo&usg=AFQjCNEK-ZrGWCbJNVLwJWYlUp3sBCEINQ   <<<<<<<<<<< \r\n \r\nI just joined 2 days ago, and my account balance has already ballooned to a staggering $27,484.98!!! \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb&sa=D&osa=qmo&usg=AFQjCNE6WXsUPEp5dNV1lG_u3VDmHzsWHA   <<<<<<<<<<<
386	Williamfit	jacksonWaype@ggmmails.com	jacksonWaype@ggmmails.com	I kept hearing about Bitcoin on the news, but had no idea how to invest! \r\n \r\nThat's when I heard about Joe's beta testing. \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Dq&sa=D&ltl=lho&usg=AFQjCNHVxvk2YmHpOxS9HBrvNR92fr6sBw   <<<<<<<<<<< \r\n \r\nThe whole process was explained to me and I was able to start trading within minutes. \r\n \r\nI've made over $ 75,000 so far and I don't see any reason to slow down! \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Do&sa=D&avp=pbo&usg=AFQjCNEK-ZrGWCbJNVLwJWYlUp3sBCEINQ   <<<<<<<<<<<
387	Francispluct	jacksonWaype@ggmmails.com	jacksonWaype@ggmmails.com	The results of the software speaks for itself... \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb&sa=D&qoo=qol&usg=AFQjCNE6WXsUPEp5dNV1lG_u3VDmHzsWHA   <<<<<<<<<<< \r\n \r\njust as promised, I made over $13,000 every single day. \r\n \r\nDo I really need to say more? \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb&sa=D&lml=qjl&usg=AFQjCNE6WXsUPEp5dNV1lG_u3VDmHzsWHA   <<<<<<<<<<<
388	Greggfop	jacksonWaype@ggmmails.com	jacksonWaype@ggmmails.com	When I joined The Bitcoin Code 2 months ago, never could have I ever imagined the series of events that would unfold just days after locking in my free software. \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Da&sa=D&pup=osa&usg=AFQjCNFmdcUrwh_NlvClKeEaVPTKYlFtoA   <<<<<<<<<<< \r\n \r\nI was able to clear my $131,382 debt. There is no greater feeling than to be debt-free. \r\n \r\nNow, I’m in the process of buying my dream home. \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb&sa=D&lfq=qtp&usg=AFQjCNE6WXsUPEp5dNV1lG_u3VDmHzsWHA   <<<<<<<<<<< \r\n \r\nI still can’t believe this is all really happening…I’m forever grateful to Steve. \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Dl&sa=D&leq=qjq&usg=AFQjCNFmgHKtMSzc2MBUFwSylGnIIBwofg   <<<<<<<<<<<
389	Ronaldwab	jacksonWaype@ggmmails.com	jacksonWaype@ggmmails.com	I was looking for an easy way to start Bitcoin trading and that was my golden ticket. \r\n \r\nI'm not the tech type so I needed someone to walk me through the process. \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Dl&sa=D&lva=ozo&usg=AFQjCNFmgHKtMSzc2MBUFwSylGnIIBwofg   <<<<<<<<<<< \r\n \r\nThat's exactly what Joe and his Bitcoin Method did. \r\n \r\nNow I have my own $ 100,000 at my disposal! \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Dl&sa=D&osa=qyp&usg=AFQjCNFmgHKtMSzc2MBUFwSylGnIIBwofg   <<<<<<<<<<<
390	KermitKar	jacksonWaype@ggmmails.com	jacksonWaype@ggmmails.com	I like learning about new technologies and extra cash can never hurt, so naturally I was interested in Bitcoin Method. \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Do&sa=D&alp=oll&usg=AFQjCNEK-ZrGWCbJNVLwJWYlUp3sBCEINQ   <<<<<<<<<<< \r\n \r\nWhen Joe told me I could join for free, I took the opportunity and after just a few months I was earning enough to quit my job! \r\n \r\nNow I am my own boss and earn over $ 37,000 a week! \r\n \r\n>>>>>>>>>>     https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2FbW9Ahb%3Fq%3Dq&sa=D&lzl=qup&usg=AFQjCNHVxvk2YmHpOxS9HBrvNR92fr6sBw   <<<<<<<<<<<
391	Sofia Coghlan	spritju.com@spritju.com	spritju.com@spritju.com	DOMAIN SERVICES EXPIRATION NOTICE FOR spritju.com\r\n\r\nDomain Notice Expiry ON: Jan 08, 2021\r\n\r\n\r\nWe have actually not received a payment from you.\r\nWe've tried to call you but were not able to contact you.\r\n\r\n\r\nCheck Out: https://bit.ly/3nrg3pa\r\n\r\nFor details as well as to make a discretionary settlement for your domain website services.\r\n\r\n\r\n\r\n\r\n010820211332403753688578798spritju.com
392	Mike Babcock\r\n	no-reply@google.com	no-reply@google.com	Greetings \r\n \r\nI have just checked  spritju.com for its SEO Trend and saw that your website could use a boost. \r\n \r\nWe will enhance your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease check our services below, we offer SEO at cheap rates. \r\nhttps://www.hilkom-digital.de/cheap-seo-packages/ \r\n \r\nStart increasing your sales and leads with us, today! \r\n \r\nregards \r\nMike Babcock\r\n \r\nHilkom Digital Team \r\nsupport@hilkom-digital.de
393	Jewel Martell	jewel.martell@msn.com	jewel.martell@msn.com	Hello,\r\nHow much money does your business website brings to you monthly? Huh? Know this secret today, a business website with poor Alexa Rank could make customers doubt its reliability. An improved website, Alexa Rank, can end up making an income stream of 1000s of dollars monthly for you.\r\nWebsite visitors use Alexa Rank as the most crucial factor while evaluating any website's value. Your website or blog must have a good Alexa Rank to stand above your competitors. Nowadays, your website Alexa Rank has to be within 100,000 or 50,000 or even lower for the advertisers to consider it. \r\nYou need to improve your website Alexa Rank to make visitors trust it and feel very confident in dealing with you. \r\nVisit https://alexaspeed.com to improve your website Alexa Rank today.\r\nIf you have any questions, feel free to send an email to support@alexaspeed.com.\r\n\r\nThank you.
394	Lesley Beattie	lesley.beattie@gmail.com	lesley.beattie@gmail.com	Good Day\r\n\r\nI have just took an in depth look on your domain: spritju.com for  the current search visibility and saw that your website could use a push.\r\n\r\nWe will increase your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support.\r\n\r\nPlease check our services below, we offer SEO at cheap rates. \r\nhttps://speed-seo.net/product/monthly-seo-package/\r\n\r\nStart enhancing your sales and leads with us, today!\r\n\r\nregards\r\nMike Beattie\r\nSpeed SEO Digital Team\r\nsupport@speed-seo.net
396	Shawn Radke	shawn@stardatagroup.com	shawn@stardatagroup.com	It is with sad regret to inform you StarDataGroup.com is shutting down.\r\nIt has been a tough year all round and we decided to go out with a bang!\r\n\r\nAny group of databases listed below is $49 or $149 for all 16 databases in this one time offer.\r\nYou can purchase it at www.StarDataGroup.com and view samples.\r\n\r\n- LinkedIn Database\r\n 43,535,433 LinkedIn Records\r\n\r\n- USA B2B Companies Database\r\n 28,147,835 Companies\r\n\r\n- Forex\r\n Forex South Africa 113,550 Forex Traders\r\n Forex Australia 135,696 Forex Traders\r\n Forex UK 779,674 Forex Traders\r\n\r\n- UK Companies Database\r\n 521,303 Companies\r\n\r\n- German Databases\r\n German Companies Database: 2,209,191 Companies\r\n German Executives Database: 985,048 Executives\r\n\r\n- Australian Companies Database\r\n 1,806,596 Companies\r\n\r\n- UAE Companies Database\r\n 950,652 Companies\r\n\r\n- Affiliate Marketers Database\r\n 494,909 records\r\n\r\n- South African Databases\r\n B2B Companies Database: 1,462,227 Companies\r\n Directors Database: 758,834 Directors\r\n Healthcare Database: 376,599 Medical Professionals\r\n Wholesalers Database: 106,932 Wholesalers\r\n Real Estate Agent Database: 257,980 Estate Agents\r\n Forex South Africa: 113,550 Forex Traders\r\n\r\nVisit www.stardatagroup.com or contact us with any queries.\r\n\r\nKind Regards,\r\nStarDataGroup.com
397	Brianna Logan	sm_b@postpro.net	sm_b@postpro.net	Hi,\r\n\r\nI came across your website and thought you may be interested.\r\n\r\nWe are one of the largest suppliers of social media marketing services.\r\nFacebook, Twitter, Instagram and Youtube.\r\n\r\nGive Your social media a huge amount of followers,\r\nlikes, shares, subscribers and views fast at a great price.\r\n\r\nWe look forward to skyrocketing your social media needs.\r\n\r\nBest,\r\nBrianna L.\r\nhttps://social-media-blast.com\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n
398	Erna Burnette	erna.burnette3@gmail.com	erna.burnette3@gmail.com	\r\nSick of paying big bucks for ads that suck? Now you can post your ad on 5000 advertising sites and it'll cost you less than $40. Get unlimited traffic forever! \r\n\r\nTo get more info take a look at: https://bit.ly/zero-payment-traffic
399	Belen Navarro	belen.navarro@gmail.com	belen.navarro@gmail.com	Greetings\r\n\r\nIf you ever need Negative SEO work, we offer it right here\r\nhttps://speed-seo.net/product/negative-seo-service/\r\n\r\n\r\nthank you\r\nPeter Navarro\r\nSpeed SEO Agency\r\nsupport@speed-seo.net
400	Mike Parkinson\r\n	no-replyOmili@gmail.com	no-replyOmili@gmail.com	Hi there \r\n \r\nI have just took an in depth look on your  spritju.com for its Local SEO Trend and seen that your website could use an upgrade. \r\n \r\nWe will enhance your Local Ranks organically and safely, using only whitehat methods, while providing Google maps and website offsite work at the same time. \r\n \r\nPlease check our plans here, we offer SEO at cheap rates. \r\nhttps://speed-seo.net/product/local-seo-package/ \r\n \r\nStart improving your local visibility with us, today! \r\n \r\nregards \r\nMike Parkinson\r\n \r\nSpeed SEO Digital Agency \r\nsupport@speed-seo.net
401	James Lambert	jlam73000@gmail.com	jlam73000@gmail.com	Good day \r\n \r\nI`m seeking a reputable company/individual to partner with in a manner that would benefit both parties. The project is worth $24 Million so if interested, kindly contact me through this email jameslambert@lambert-james.com for clarification. \r\n \r\nI await your response. \r\n \r\nThanks, \r\n \r\nJames Lambert
402	Jen Scott	boost@fastem.com	boost@fastem.com	Hi,\r\n\r\nI thought you may be interested in our services. \r\n\r\nWe can send thousands of interested people to your website daily.\r\nVisitors will come from online publications in your EXACT niche.\r\nLive Stats URL included.\r\n\r\nPeople coming from targeted online publications pricing:\r\n5,000 interested visitors just $54.99.\r\nWe also have great larger packages (some sold out for 1 month).\r\n\r\nWe look forward to seeing you on our site.\r\n\r\nKind regards,\r\nJen\r\nhttps://traffic-stampede.com\r\n\r\n
403	Louisa Pride	pride.louisa@hotmail.com	pride.louisa@hotmail.com	New Text To Speech software that actually sounds like Real People with pauses, tone, inflection – so much so that you are hard put to tell that it’s not a Real Person. For a free demo. Reply to this email: katesepage@gmail.com
404	Rhea Kime	rhea@sendbulkmails.com	rhea@sendbulkmails.com	Use SendBulkMails.com to run email campaigns from your own private dashboard.\r\n\r\nCold emails are allowed and won't get you blocked :)\r\n\r\nStarter Package 50% off sale\r\n- 1Mil emails / mo @ $99 USD\r\n- Dedicated IP and Domain Included\r\n- Detailed statistical reports (delivery, bounce, clicks etc.)\r\n- Quick and easy setup with extended support at no extra cost.\r\n- Cancel anytime!\r\n\r\nRegards,\r\nwww.SendBulkMails.com
405	Mike Kennett\r\n	no-reply@google.com	no-reply@google.com	Hi there \r\n \r\nI have just took an in depth look on your  spritju.com for its SEO metrics and saw that your website could use an upgrade. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease check our pricelist here, we offer SEO at cheap rates. \r\nhttps://www.hilkom-digital.de/cheap-seo-packages/ \r\n \r\nStart improving your sales and leads with us, today! \r\n \r\nregards \r\nMike Kennett\r\n \r\nHilkom Digital Team \r\nsupport@hilkom-digital.de
406	Carla Bannerman	carla@stardatagroup.com	carla@stardatagroup.com	It is with sad regret to inform you StarDataGroup.com is shutting down.\r\n\r\nFire sale till the 7th of Feb.\r\n\r\nAny group of databases listed below is $49 or $149 for all 16 databases in this one time offer.\r\nYou can purchase it at www.StarDataGroup.com and view samples.\r\n\r\n- LinkedIn Database\r\n 43,535,433 LinkedIn Records\r\n\r\n- USA B2B Companies Database\r\n 28,147,835 Companies\r\n\r\n- Forex\r\n Forex South Africa 113,550 Forex Traders\r\n Forex Australia 135,696 Forex Traders\r\n Forex UK 779,674 Forex Traders\r\n\r\n- UK Companies Database\r\n 521,303 Companies\r\n\r\n- German Databases\r\n German Companies Database: 2,209,191 Companies\r\n German Executives Database: 985,048 Executives\r\n\r\n- Australian Companies Database\r\n 1,806,596 Companies\r\n\r\n- UAE Companies Database\r\n 950,652 Companies\r\n\r\n- Affiliate Marketers Database\r\n 494,909 records\r\n\r\n- South African Databases\r\n B2B Companies Database: 1,462,227 Companies\r\n Directors Database: 758,834 Directors\r\n Healthcare Database: 376,599 Medical Professionals\r\n Wholesalers Database: 106,932 Wholesalers\r\n Real Estate Agent Database: 257,980 Estate Agents\r\n Forex South Africa: 113,550 Forex Traders\r\n\r\nVisit www.stardatagroup.com or contact us with any queries.\r\n\r\nKind Regards,\r\nStarDataGroup.com
407	Bryan Creer	bryan.creer@gmail.com	bryan.creer@gmail.com	Hi|Hey|Good evening|Good morning|Good Afternoon} people at spritju.com,\r\nHope you’re excellent. \r\nI'm , I hope that business is good and you’ve been achieving a lot through the entire current situation.\r\n My opinion can be your website isn’t protected against unwanted messages, that isn’t cool, I mean literally, why don’t you use this.\r\nhttps://bogazicitente.com/antispam176742\r\nIf you are not interested, simply erase this email and that we won't contact you again.\r\nKind regards,\r\n\r\nOneTwo GmbH
408	Lieselotte Lea	topfakeid@mailfence.com	topfakeid@mailfence.com	Buy Scannable Fake ID – Premium\r\nFake IDs\r\nBuy our premium fake IDs with the best security elements.\r\nAll of our fake ID comes with Scannable features & guaranteed to pass under UV.\r\n\r\nRead our reviews and testimonials\r\nhttps://www.trustpilot.com/review/topfakeid.com\r\nhttps://www.scamadviser.com/check-website/topfakeid.com\r\nhttps://www.sitejabber.com/reviews/topfakeid.com
410	David Song	noreply@googlemail.com	noreply@googlemail.com	Hello, \r\nOur Investors wishes to invest in your company by offering debt financing in any viable Project presented by your Management,Kindly send your Business Project Presentation Plan Asap. \r\n \r\ndavidsong2030@gmail.com \r\n \r\nRegards, \r\nMr.David Song
411	Aundrea	info@spritju.com	info@spritju.com	You Won't Want To Miss This!  50 pcs medical surgical masks only $1.99 and N95 Mask $1.79 each.  \r\n\r\nOnly 10 orders left!  Get yours here: pharmacyusa.online\r\n\r\nAll the best,\r\n\r\nSpritju — Contact Us
412	Summer Medworth	summer@sendbulkmails.com	summer@sendbulkmails.com	Use SendBulkMails.com to run email campaigns from your own private dashboard.\r\n\r\nCold emails are allowed and won't get you blocked :)\r\n\r\n- 1Mil emails / mo @ $99 USD\r\n- Dedicated IP and Domain Included\r\n- Detailed statistical reports (delivery, bounce, clicks etc.)\r\n- Quick and easy setup with extended support at no extra cost.\r\n- Cancel anytime!\r\n\r\nRegards,\r\nwww.SendBulkMails.com
413	MR HO CHOI	vfuga213@execs.com	vfuga213@execs.com	Greetings, I have been tasked with identifying, initiating and possibly developing a business partnership with you / your company. I checked recommendation sites, corporate social networking sites, interviewed a few list investors and made a list of potential partners that identified you and your business. I visited your website and saw your products. I have to say that I am very impressed with the quality of your products. Therefore, we would like your company to ship large quantities of your product to our country for commercial use. Please contact me for more information on our requirements / orders and shipping deadlines. Best regards. HO CHOI email ..... hchoi382@gmail.com
414	Ryan Pendley	ryan@buy-rapid-tests.com	ryan@buy-rapid-tests.com	Get your own Covid-19 test results in 15 minutes on Buy-Rapid-Tests.com\r\n\r\nSimple 3 step process - anyone can do it.\r\n\r\nWe have FDA Authorized Covid-19 Rapid Test Kits - Most orders ship standard within 48 hours.\r\n\r\nOrder minimum is only - 1 Box \r\n\r\nEach Box includes:\r\n- 25 individual Covid-19 tests cassettes\r\n- 25 blood droppers\r\n- 1 bottle of buffer/testing solution\r\n\r\nBuy as many boxes as you would like.\r\n\r\nThese tests are perfect for your organization, business, restuarant or group.\r\n\r\nPrice is $36 per test (25 tests per box) and that includes shipping to any location for $50\r\n\r\nWe offer 10% wholesale price breaks for quantities of 5 boxes or more.\r\n\r\nRegards,\r\nBuy-Rapid-Tests.com
415	D	baesidegrill@gmail.com	baesidegrill@gmail.com	I was wondering if you have a need for CATERING SERVICES.\r\n\r\nWe are offering free FOOD SERVICES.\r\n\r\nCheck us out at https://www.instagram.com/baesidegrill/ or call / text 416-670-1862\r\n\r\nEmail us at baesidegrill@gmail.com and try us out!\r\n\r\nThanks,\r\nBindiya\r\n\r\n-------\r\n\r\nReport any unsolicited messages \r\nhttp://help.instagram.com/contact/383679321740945?helpref=page_content\r\n
416	Mike Crossman\r\n	no-replyOmili@gmail.com	no-replyOmili@gmail.com	Hi there \r\n \r\nI have just verified your SEO on  spritju.com for its Local SEO Trend and seen that your website could use an upgrade. \r\n \r\nWe will increase your Local Ranks organically and safely, using only whitehat methods, while providing Google maps and website offsite work at the same time. \r\n \r\nPlease check our pricelist here, we offer SEO at cheap rates. \r\nhttps://speed-seo.net/product/local-seo-package/ \r\n \r\nStart enhancing your local visibility with us, today! \r\n \r\nregards \r\nMike Crossman\r\n \r\nSpeed SEO Digital Agency \r\nsupport@speed-seo.net
417	Devon Danner	danner.devon@gmail.com	danner.devon@gmail.com	Greetings\r\n\r\nI have just took a look on your SEO for domain: spritju.com for  the current search visibility and saw that your website could use a push.\r\n\r\nWe will enhance your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support.\r\n\r\nPlease check our pricelist here, we offer SEO at cheap rates. \r\nhttps://speed-seo.net/product/monthly-seo-package/\r\n\r\nStart improving your sales and leads with us, today!\r\n\r\nregards\r\nMike Danner\r\nSpeed SEO Digital Team\r\nsupport@speed-seo.net
418	Mike Galbraith\r\n	no-reply@google.com	no-reply@google.com	Hello \r\n \r\nI have just took an in depth look on your  spritju.com for the ranking keywords and saw that your website could use a boost. \r\n \r\nWe will increase your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease check our services below, we offer SEO at cheap rates. \r\nhttps://www.hilkom-digital.de/cheap-seo-packages/ \r\n \r\nStart enhancing your sales and leads with us, today! \r\n \r\nregards \r\nMike Galbraith\r\n \r\nHilkom Digital Team \r\nsupport@hilkom-digital.de
419	Erick Sawyer	seoguyerick@gmail.com	seoguyerick@gmail.com	Hi, I stumbled on spritju.com yesterday and love the design. What platform is it made with? WordPress? \r\n \r\nThe only thing I noticed was that you appeared a bit low on Google search results. \r\n \r\nI manage several websites and utilize a few services that really help me SEO rank sites and get more traffic. \r\n \r\nJust want to share them with you as they can really help your site/business grow. \r\n \r\n \r\n1: LinksChain ( https://www.linkschain.io ) - On-demand SEO link-building. Quality and affordable. It has more than doubled my traffic and sales. \r\n \r\n \r\nAlso, having your page load fast helps with your SEO significantly. You want to make sure you have a great hosting company, while not spending a fortune. \r\n \r\nHere is a guide on the best host in 2021 (also one of cheapest), if you want your page to load fast and save a lot of money on hosting. \r\n \r\n \r\nhttps://adwhip.com/hosting-2021-guide/ \r\n \r\n \r\nIf you are passionate about growing your website business, I'd recommend checking these resources out. \r\n \r\nAnyway, I'm going to get back to my beer. Cheers! \r\n \r\nErick S.
420	DonaldFag	no-replykr@gmail.com	no-replykr@gmail.com	Hi!  spritju.com \r\n \r\nDid you know that it is possible to send appeal absolutely legit? \r\nWe provide a new way of sending letter through feedback forms. Such forms are located on many sites. \r\nWhen such messages are sent, no personal data is used, and messages are sent to forms specifically designed to receive messages and appeals. \r\nalso, messages sent through communication Forms do not get into spam because such messages are considered important. \r\nWe offer you to test our service for free. We will send up to 50,000 messages for you. \r\nThe cost of sending one million messages is 49 USD. \r\n \r\nThis offer is created automatically. Please use the contact details below to contact us. \r\n \r\nContact us. \r\nTelegram - @FeedbackMessages \r\nSkype  live:contactform_18 \r\nWhatsApp - +375259112693
421	Prince Vine	vine.prince@hotmail.com	vine.prince@hotmail.com	Hey\r\nSEO optimized website, make spritju.com be a faster and cleaner website, choose us:\r\nhttps://it-seo-specialists.com/technicalseo/\r\nHave great day!\r\nSincerely,\r\n\r\nP.S. We offer the best marketing services on our website, still not interested? Here is an easy, 1-click unsubscribe link: https://one-two-seo.com/?unsubscribe=spritju.com
422	Mario Gonzalez	mysolutions360@gmail.com	mysolutions360@gmail.com	Hi, are you still in charge of spritju.com ? \r\n \r\nIf you take 30 sec to read this email, we could help you 2X-5X your business. \r\n \r\nMost SEO companies will ask you to pay upfront with no guarantee that your website will rank on Google. \r\n \r\nWe are different ... \r\n \r\nOur company specializes in Pay Per Performance SEO. Which means – \r\nWe get your business on the first page of Google before we get paid for our service. \r\n \r\nI know that’s a bold statement but we can back it up with 9 years of success in this industry. \r\n \r\nIf you’re interested in getting on the first page of Google, and only pay if you get there, \r\nlet me know when is a good time for a call. \r\n \r\nGet full details here or send us a message to mcmarketing360@hotmail.com: \r\nhttps://getmoreclientsfaster.com/performance-based-seo/
423	Florentina	florentina@spritju.com	florentina@spritju.com	Good Morning\r\n\r\nBuy face mask to protect your loved ones from the deadly CoronaVirus.  We wholesale N95 Masks and Surgical Masks for both adult and kids.  The prices begin at $0.19 each.  If interested, please visit our site: pharmacyoutlets.online\r\n\r\nThe Best,\r\n\r\nSpritju — Contact Us
424	Peggy Frewer	peggy@sesforyou.com	peggy@sesforyou.com	Hi,\r\n \r\nI'm always asked what is the quickest way to make money online, when you are just starting out? Well here's the definitive answer that question:\r\n \r\n==> https://sesforyou.com\r\n \r\nNew Book Reveals How I Built A 7-Figure Online Business Using Nothing But Ethical Email Marketing To Drive Revenue, Sales and Commissions...\r\n \r\n==> https://sesforyou.com\r\n \r\nRegards,\r\nSesForYou.com
425	Novella	novella@spritju.com	novella@spritju.com	Hi there \r\n\r\nThe complete selection of all Ray-Ban sunglasses styles available online and only 19.99 dollars. \r\n\r\nInsanely special offer for the next 24 hours only! Get yours here: sunglassusa.online\r\n\r\nYou Won't Want To Miss This!\r\n\r\nEnjoy, \r\n\r\nNovella\r\nSpritju — Contact Us
426	Emile Boote	boote.emile@gmail.com	boote.emile@gmail.com	Greetings\r\n\r\nIf you ever need Negative SEO work, we offer it right here\r\nhttps://speed-seo.net/product/negative-seo-service/\r\n\r\n\r\nthank you\r\nPeter Boote\r\nSpeed SEO Agency\r\nsupport@speed-seo.net
427	Arlen Garratt	arlen.garratt51@gmail.com	arlen.garratt51@gmail.com	Hey \r\nHope you’re great, and that customers are good.\r\nWe offer a plan to skyrocket your small business:\r\nhttps://backlinks-generator.com\r\nWith best regards,\r\n\r\nP.S. We offer the best marketing services on our website, still not interested? Here is an easy, 1-click unsubscribe link: https://one-two-seo.com/?unsubscribe=spritju.com
428	Jeremy Becker	jeremy@onlinebusiness.co	jeremy@onlinebusiness.co	Hey, \r\ntoday's your last chance to get Tony Robbins and Dean Graziosi’s unprecedented Project Next offer for OYF challenge members only... \r\n \r\nClick here : https://bit.ly/2SVJ6HX \r\n \r\nIt’s decisions like these that can impact your life and your family’s for years to come. This is about your success, your impact and achieving the results you crave. \r\n \r\nSo if you’re on the fence and need help making the smart choice, this page will answer that question. Plus show you everything you'll get if you take action before midnight... \r\n \r\nClick here https://bit.ly/2SVJ6HX to watch it now and get in before it's too late.--> https://bit.ly/3tVTEnq \r\n \r\nSee you inside, \r\n \r\n-Jeremy
429	Susan	info@spritju.com	info@spritju.com	Hi there\r\n\r\nBuy face mask to protect your loved ones from the deadly CoronaVirus.  We wholesale N95 Masks and Surgical Masks for both adult and kids.  The prices begin at $0.19 each.  If interested, please check our site: pharmacyoutlets.online\r\n\r\nBest regards,\r\n\r\nSusan\r\nSpritju — Contact Us
430	Yahoo	press@yahoo.com	press@yahoo.com	Most profitable cryptocurrency miners has been released : \r\nDBT Miner: $7,500 (Bitcoin), $13,000 (Litecoin), $13,000 (Ethereum), and $15,000 (Monero) \r\n \r\nGBT Miner: $22,500 (Bitcoin), $39,000 (Litecoin), $37,000 (Ethereum), and $45,000 (Monero) \r\nRead more here : \r\nhttps://www.yahoo.com/now/bitwats-release-most-profitable-asic-195600764.html
431	Elvira	info@spritju.com	info@spritju.com	Hi\r\n\r\nCAREDOGBEST™ - Personalized Dog Harness. All sizes from XS to XXL.  Easy ON/OFF in just 2 seconds.  LIFETIME WARRANTY.\r\n\r\nClick here: caredogbest.online\r\n\r\nCheers,\r\n\r\nElvira\r\nSpritju — Contact Us
432	Mike Murphy\r\n	no-replyOmili@gmail.com	no-replyOmili@gmail.com	Hello \r\n \r\nI have just took a look on your SEO for  spritju.com for the Local ranking keywords and seen that your website could use a boost. \r\n \r\nWe will increase your Local Ranks organically and safely, using only whitehat methods, while providing Google maps and website offsite work at the same time. \r\n \r\nPlease check our plans here, we offer SEO at cheap rates. \r\nhttps://speed-seo.net/product/local-seo-package/ \r\n \r\nStart improving your local visibility with us, today! \r\n \r\nregards \r\nMike Murphy\r\n \r\nSpeed SEO Digital Agency \r\nsupport@speed-seo.net
433	Winnie Dullo	winnie@bestlocaldata.com	winnie@bestlocaldata.com	Hello,\r\n\r\nIt is with sad regret to inform you that BestLocalData.com is shutting down.\r\n\r\nWe have made all our databases for sale for a once-off price.\r\n\r\nVisit our website to get the best bargain of your life. BestLocalData.com\r\n\r\nRegards,\r\nWinnie
434	Ronald White	ronald@approvedtoday1.xyz	ronald@approvedtoday1.xyz	Hello,\r\n\r\nI hope life is treating you kind and business is AWESOME!\r\n\r\nI just have one quick question for you.\r\n\r\nWould you consider a Working Capital Loan for your Business if the price and terms were acceptable?\r\n\r\nIf so, we can provide you with a funding decision is less than 30 seconds \r\nwithout pulling your credit or submitting a single document. \r\n\r\nJust click on the link to INSTANTLY see how much you qualify for www.approvedtoday1.xyz\r\n\r\nAlso, please check out this video to see how our program works, and all the funding options we have available for you.  www.approvedtoday1.xyz/video\r\n\r\n\r\n\r\nWarm Regards,\r\n\r\nRonald White\r\nApproved Today\r\nwww.approvedtoday1.xyz\r\n\r\n\r\n\r\n\r\nThis is an Advertisement.\r\nTo unsubscribe, click here www.approvedtoday1.xyz/unsubscribe,\r\n\r\nor write to:\r\n\r\nApproved Today\r\n9169 W State St #3242\r\nGarden City, ID 83714\r\n\r\n\r\n\r\n
435	Nick Brown	nick@saaytext.com	nick@saaytext.com	Hello, we have a fully Unlimited Texting tool for businesses to communicate with customers. \r\n \r\nIt's 49/mo flat, all you can send no other costs. \r\n \r\nYou can text your whole list at once using the bulk send feature or send two-way texts and get replies with Peer to Peer texting. \r\n \r\nYou can check it out at http://saaytext.com \r\n \r\nThanks \r\nNick
436	Mike Ward\r\n	no-reply@google.com	no-reply@google.com	Hi there \r\n \r\nI have just verified your SEO on  spritju.com for  the current search visibility and saw that your website could use a push. \r\n \r\nWe will enhance your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease check our plans here, we offer SEO at cheap rates. \r\nhttps://www.hilkom-digital.de/cheap-seo-packages/ \r\n \r\nStart enhancing your sales and leads with us, today! \r\n \r\nregards \r\nMike Ward\r\n \r\nHilkom Digital Team \r\nsupport@hilkom-digital.de
437	Leonida Grier	grier.leonida36@gmail.com	grier.leonida36@gmail.com	Good Afternoon,\r\nI'm ,\r\nHow are you doing regarding your Facebook Page Reviews?\r\nIf you make the customer unhappy in the physical world, they might each tell six friends. If you make customers unhappy on the Internet, they can each tell 6000 friends. Get positive reviews from real people on facebook, today!\r\nCheck what we are able to do for you: \r\nhttps://best-marketers.com/facebook-page-reviews\r\nKind regards,\r\nP.S. We offer the top IT services you can check on our online shop for making big money in a small business, still not interested in getting new clients? Here is a quick, 1-click unsubscribe link: https://best-marketers.com/?unsubscribe=spritju.com
438	Ciara	ciara@spritju.com	ciara@spritju.com	Hello there \r\n \r\nProviding Premium sunglasses only $19.99 for the next 24 Hours ONLY.  60% OFF today with free worldwide shipping.\r\n\r\nGet Yours: trendshades.online\r\n \r\nHave a great time, \r\n \r\nCiara\r\nSpritju — Contact Us
439	Antwan Palumbo	antwan@bestlocaldata.com	antwan@bestlocaldata.com	Hello,\r\n\r\nIt is with sad regret to inform you that BestLocalData.com is shutting down.\r\n\r\nWe have made all our databases for sale for a once-off price.\r\n\r\nVisit our website to get the best bargain of your life. BestLocalData.com\r\n\r\nRegards,\r\nAntwan
440	Tomas Kaawirn	tomas.kaawirn@msn.com	tomas.kaawirn@msn.com	Good evening \r\nTo beat other competitors you have to have this particular service to speak with customers. \r\nhttps://best-marketers.com/backlinks-generator\r\nRegards,\r\n\r\nP.S. We offer the best IT services you can find on our shop for making big money in a small business, still not interested in getting clients? Here is a quick, 1-click unsubscribe link: https://best-marketers.com/?unsubscribe=spritju.com
441	Freddie Gormly	2vampirorh@ottappmail.com	2vampirorh@ottappmail.com	Before you spend another penny on advertising, do this first: http://bit.ly/list-your-site-free-here
442	Mike Otis\r\n	no-replyAlox@gmail.com	no-replyAlox@gmail.com	Hi there \r\n \r\nIncrease your spritju.com SEO metrics values with us and get more visibility and exposure for your website. \r\n \r\nMore info: \r\nhttps://www.monkeydigital.org/product/moz-da50-seo-plan/ \r\n \r\nNEW: \r\nhttps://www.monkeydigital.org/product/ahrefs-dr50-ur70-seo-plan/ \r\nhttps://www.monkeydigital.org/product/trust-flow-seo-package/ \r\n \r\n \r\nthank you \r\nMike Otis\r\n
443	Garnet Rodius	garnet@bestlocaldata.com	garnet@bestlocaldata.com	We at BestLocalData.com has been hit badly by Covid-19 and as a result BestLocalData.com is shutting down.\r\n\r\nWe provided the best data to companies to find their right customer base, we don't want other companies to go down the same path we went and go out of business.\r\n\r\nAs a result we are providing our data till the end of the week at the lowest possible prices. \r\n\r\nBestLocalData.com
444	Jed	info@spritju.com	info@spritju.com	Good Morning \r\n \r\nBuy all styles of Ray-Ban Sunglasses only 24.99 dollars.  If interested, please visit our site: designerframes.online\r\n \r\nAll the best, \r\n \r\nSpritju — Contact Us
445	Reed Stambaugh	jhlaalshmar@polostar.me	jhlaalshmar@polostar.me	Thinking about making some extra cash online? Don't do it until you've read this first: http://bit.ly/how-to-start-an-affiliate-marketing-business
446	Mike Samuels\r\n	no-replyOmili@gmail.com	no-replyOmili@gmail.com	Hello \r\n \r\nWe will improve your Local Ranks organically and safely, using only whitehat methods, while providing Google maps and website offsite work at the same time. \r\n \r\nPlease check our pricelist here, we offer Local SEO at cheap rates. \r\nhttps://speed-seo.net/product/local-seo-package/ \r\n \r\nNEW: \r\nhttps://www.speed-seo.net/product/zip-codes-gmaps-citations/ \r\n \r\nregards \r\nMike Samuels\r\n \r\nSpeed SEO Digital Agency
447	Kristal Able	weusebio@flaido.com	weusebio@flaido.com	Go here to see a list of high traffic sites that accept free ad submissions: http://www.freetrafficwebsites.click
448	Hilda Stearns	hilda@fbcourses.net	hilda@fbcourses.net	Hello from FbCourses.net\r\n\r\nWant to pay $0.01 a click? We got you covered.\r\n\r\nA great team of Global Digital Marketing experts have compiled this list of 13 Best + Free Facebook Advertising Training, Classes and Courses to help you learn and excel at Facebook Ads & Marketing. \r\n\r\nThousands of professionals have already benefited from this list on Facebook Training.\r\n\r\nRegards,\r\nHilda
449	Shari McMaster	mcmaster.shari@gmail.com	mcmaster.shari@gmail.com	Good evening\r\nHope you’re excellent, and that customers are profitable. Please allow me to introduce to you this service that you may find useful for your business.\r\nAutomatically submit your internet site to a large number of search engines like yahoo and directories.\r\nhttps://ultimate-marketers.com\r\nSincerely,\r\nWe offer the top marketing services you can check on our shop for making big money in a small business, still not interested in getting clients? Here is a simple, 1-click unsubscribe link: https://ultimate-marketers.com/?unsubscribe=spritju.com
450	Cynthia Nanney	cynthia.nanney@gmail.com	cynthia.nanney@gmail.com	Good Afternoon\r\nSocial media marketing is a powerful way for businesses of all sizes to reach prospects and customers. Most companies don't pay attention to it because they don't have enough time. Allow us to help you.\r\nSee what we can do for you:\r\nhttps://first-class-promotion.com/socialnetworks\r\nKind regards,\r\nWe offer the best IT services you may check on our website for making big money in a small business, still not enthusiastic about getting clients? Here is an easy, 1-click unsubscribe link: https://first-class-promotion.com/?unsubscribe=spritju.com
451	Donald	donald@spritju.com	donald@spritju.com	Color-changing swimshorts \r\n\r\nDive into the ocean and your swimshorts suddenly change color! These swimshorts ara AMAZING!\r\n\r\nHurry! 50% Off Worldwide For Limited Time Only!\r\n\r\nGet it here: coolshorts.online\r\n\r\nSincerely, \r\n\r\nDonald\r\nSpritju — Contact Us
452	Mike Forman\r\n	no-reply@google.com	no-reply@google.com	Good Day \r\n \r\nI have just took a look on your SEO for  spritju.com for its SEO Trend and saw that your website could use a push. \r\n \r\nWe will enhance your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease check our plans here, we offer SEO at cheap rates. \r\nhttps://www.hilkom-digital.de/cheap-seo-packages/ \r\n \r\nStart enhancing your sales and leads with us, today! \r\n \r\nregards \r\nMike Forman\r\n \r\nHilkom Digital Team \r\nsupport@hilkom-digital.de
453	Cleveland Large	large.cleveland@yahoo.com	large.cleveland@yahoo.com	The plug welcomes you with a super discount of 15% on every product for a limited period of time! (Promo code: THEPLUG)\r\n\r\nWe offer the largest assortment of private databases! Such as: \r\n\r\n- The Largest collection of Linkedin Databases! \r\n- Forex Consumers (worldwide)\r\n- Casino Consumers  (worldwide)\r\n- Business Databases  (worldwide)\r\n- Cryptocurrency consumers  (worldwide)\r\n- many more..\r\n\r\nAnd the best part of it! We are the only source you will ever need! We have the largest database on the market with the greatest prices and team. \r\n\r\nVisit us: https://silent-plug.com/
454	Ronald White	ronald@approvedtoday1.xyz	ronald@approvedtoday1.xyz	Hello,\r\n\r\nI hope life is treating you kind and business is AWESOME!\r\n\r\nWould you consider a Working Capital Loan for your business if the price and terms were acceptable?\r\n\r\nApproved Today Funding is a Direct Lender offering loans from 10kk to 500k with funding decisions is less than 30 seconds \r\nwithout pulling your credit or submitting a single document. \r\n\r\nJust click on the link to INSTANTLY see how much you qualify for www.approvedtoday1.xyz\r\n\r\n\r\n\r\nWarm Regards,\r\n\r\nRonald White\r\nApproved Today\r\nwww.approvedtoday1.xyz\r\n\r\n\r\n\r\n\r\nThis is an Advertisement.\r\nTo unsubscribe, click here www.approvedtoday1.xyz/unsubscribe,\r\n\r\nor write to:\r\n\r\nApproved Today\r\n9169 W State St #3242\r\nGarden City, ID 83714\r\n\r\n\r\n\r\n
455	KirbyStiny	report@savaserver.com	report@savaserver.com	Greetings, \r\nGet your Residential/ISP proxy server. We offer 100% clean pre-tested ranges! \r\nWe guarantee that ips are good and that following websites will work Adidas, Champsports, Eastbay, Finishline, Footaction, Footlocker, Supremenewyork, Footpatrol, Sneakersnstuff, Yeezysupply. \r\nWe have secured some additional clean ISP IP space and offering this promotional pricing for NEW prospective clients to come onboard. A new cabinet has been set up and servers are getting racked and stacked in preparation for rollout. All inventory is sold on a first come/paid basis. \r\nINTEL E5-2670 V1 +  1 x /24 included \r\nLinux or Windows \r\n16 GB Ram \r\n240 GB SSD \r\n10GB UNMETERED \r\n1 x /24 (256 IPv4 from AT&T) \r\nUS \r\n$218.00 USD \r\nMore info on: https://www.savaserver.com
456	Asa Hammack	asa.hammack96@gmail.com	asa.hammack96@gmail.com	Hey \r\nSerious entrepreneurs need many for business to convert brings about clients off their site:\r\nhttps://ultimate-marketers.com/backlinks-generator\r\nBest regards,\r\nWe offer the most effective IT services you can purchase on our shop for making big money in a small business, still not enthusiastic about getting new business? Here is a simple, 1-click unsubscribe link:  https://ultimate-marketers.com/?unsubscribe=spritju.com
457	Jayme Bateman	jayme@bestlocaldata.com	jayme@bestlocaldata.com	It is with sad regret to inform you that because of the Covid pandemic BestLocalData.com is shutting down at the end of the month.\r\n\r\nWe have lost family members and colleagues and have decided to shut down BestLocalData.com\r\n\r\nIt was a pleasure serving you all these years. We have made all our databases available for $99 (All of it for $99) for those interested.\r\n\r\nKind Regards,\r\nBestLocalData.com\r\nJayme
458	Leslie Fink	bchakka111975s@ronell.me	bchakka111975s@ronell.me	Maximum Ad Exposure - No more ad spends  : https://bit.ly/ads-for-free-forever
459	Gia	gia@spritju.com	gia@spritju.com	2021 New Telescope Provide You The Clearest Visual Experience\r\n\r\nOur Super Zoom Telescope would allow you to do this anywhere with no problem! The unbeatable design lets you carry the telescope anywhere taking up no room at all.\r\n\r\nSave 50% OFF with Free Worldwide Shipping.  SHOP NOW today: https://telescope.fun\r\n\r\nThank You, \r\n\r\nGia\r\nSpritju — Contact Us
\.


--
-- Data for Name: spritju_faq; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_faq (id, questioner_name, questioner_email, question, reply, status, replier_id) FROM stdin;
\.


--
-- Data for Name: spritju_new_customer; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_new_customer (id, name, email, phone, address, "Lat", lon, offer, body) FROM stdin;
21	hamidreza	ashrafnezhad.hamidreza@gmail.com	091777000624	default	0	0	0	\N
22	hamid	ashrafnezhad.hamidreza@gmail.com	09177000624	default	0	0	0	\N
23	hamid	ashrafnezhad.hamidreza@gmail.com	09177000624	default	59.3628753686998891	17.99028881793312	273991.026620511489	\N
24	hamid	ashrafnezhad.hamidreza@gmail.com	09177000624	default	59.4639913086781675	18.0910648754028216	51691	\N
25	hiva ashrafnezhad	ashrafnezhad@gmail.com	9374633040	default	59.3151440647991066	18.0215258506267944	3177739	\N
26	hamid	ashrafnezhad.hamidreza@gmail.com	09177000624	default	59.3186495575225834	18.0250437712613163	531125	\N
27	hamid	ashrafnezhad.hamidreza@gmail.com	09369505828	default	59.3194548595350355	18.0237027502223803	121279	\N
28	Ahmad Karnama	ahmad.karnama@gmail.com	730514055	default	59.4640199269546557	18.0910403999999971	1776	\N
29	hamid	ashrafnezhad.hamidreza@gmail.com	09177000624	default	59.3196060803027123	18.023328166223763	147841	\N
30	Hamid	razangame@gmail.com	09369505828	default	59.3175330999999986	18.0242749999999887	805960	\N
31	Hamid Reza Ashrafnezhad	ashrafnezhad.hamidreza@gmail.com	9369505828	default	59.319299564610894	18.0236261353689606	163149	\N
32	Hamid Reza Ashrafnezhad	ashrafnezhad.hamidreza@gmail.com	9369505828	default	59.3198943722929428	18.023714504563042	179087	\N
33	Hamid Reza Ashrafnezhad	ashrafnezhad.hamidreza@gmail.com	9369505828	default	59.3195948798642689	18.0221276695378201	176499	\N
34	Hamid Reza Ashrafnezhad	ashrafnezhad.hamidreza@gmail.com	9369505828	default	59.3380320027657362	18.0796439502342707	187002	\N
35	Hamid Reza Ashrafnezhad	ashrafnezhad.hamidreza@gmail.com	9369505828	default	59.3195027877685277	18.0231708755180229	286276	\N
36	spritju.com	spritju.com@domstates.su	spritju.com	default	0	0	0	\N
37	spritju.com	spritju.com@domstates.su	spritju.com	default	0	0	0	\N
38	spritju.com	spritju.com@domstates.su	spritju.com	default	0	0	0	\N
39	spritju.com	spritju.com@domstates.su	spritju.com	default	0	0	0	\N
40	spritju.com	spritju.com@domstates.su	spritju.com	default	0	0	0	\N
41	spritju.com	spritju.com@domstates.su	spritju.com	default	0	0	0	\N
42	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
43	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
44	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
45	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
46	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
47	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
48	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
49	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
50	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
51	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
52	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
53	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
54	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
55	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
56	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
57	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
58	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
59	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
60	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
61	spritju.com	spritju.com@domstat.su	spritju.com	default	0	0	0	\N
\.


--
-- Data for Name: spritju_post; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_post (id, title, slug, content, status, short_description, "Meta_description", "Meta_keywords", updated_on, created_on, publish_on, post_image, author_id) FROM stdin;
1	Impacts of Low-Carbon Fuel Standards in Transportation on the Electricity Market	impacts-of-low-carbon-fuel-standards-in-transportation-on-the-electricity-market	<h2><a href="https://www.mdpi.com/1996-1073/11/8/1943/htm">By&nbsp;Ahmad Karnama,&nbsp;Jo&atilde;o Abel Pe&ccedil;as Lopes&nbsp;and Mauro&nbsp;Augusto da Rosa&nbsp;</a></h2>\r\n\r\n<h2>Abstract<strong>:</strong>&nbsp;</h2>\r\n\r\n<p>Electric Vehicles (EVs) are increasing the interdependence of transportation policies and the electricity market dimension. In this paper, an Electricity Market Model with Electric Vehicles (EMMEV) was developed, exploiting an agent-based model that analyzes how carbon reduction policy in transportation may increase the number of Electric Vehicles and how that would influence electricity price. Agents are Energy Service Providers (ESCOs) which can distribute fuels and their objective is to maximize their profit. In this paper, the EMMEV is used to analyze the impacts of the Low-Carbon Fuel Standard (LCFS), a performance-based policy instrument, on electricity prices and EV sales volume. The agents in EMMEV are regulated parties in LCFS should meet a certain Carbon Intensity (CI) target for their distributed fuel. In case they cannot meet the target, they should buy credits to compensate for their shortfall and if they exceed it, they can sell their excess. The results, considering the assumptions and limitations of the model, show that the banking strategy of the agents contributing in the LCFS might have negative impact on penetration of EVs, unless there is a regular Credit Clearance to trade credits. It is also shown that the electricity price, as a result of implementing the LCFS and increasing number of EVs, has increased between 2% and 3% depending on banking strategy.</p>	0			low-carbon fuel standard; electric vehicles; policy analysis; electricity market	2019-09-03	2019-02-15	2019-09-03	images/posts/Screenshot_2019-09-03_at_18.33.23.jpg	3
\.


--
-- Data for Name: spritju_post_category; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_post_category (id, post_id, category_id) FROM stdin;
6	1	2
\.


--
-- Data for Name: spritju_profile; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_profile (id, bio, profile_image, instagram_url, twitter_url, linkedin_url, facebook_url, user_id) FROM stdin;
4	\N		\N	\N	\N	\N	4
1	Experienced Webmaster with a demonstrated history of working in the information technology and services industry. Skilled in Search Engine Optimization (SEO), Research, Web Applications, Web Development, and Project Management. Strong engineering professional with a Engineer’s Degree focused in Computer Software Engineering from Shahid Bahonar University of Kerman.	images/profiles/photo_2018-02-18_17-37-21_8cNcKCh.jpg	https://www.instagram.com/hamid_ashrafnezhad/	https://twitter.com/HAshrafnezhad	https://www.linkedin.com/in/hamidreza-ashrafnezha/	https://www.facebook.com/hamid.ashf	1
3	Ahmad is a focused and proactive entrepreneur, management consultant and researcher in the field of energy. He has respected education and invaluable work experiences for the last 17 years across the globe specially in Nordics, UK, and USA.  \r\nAhmad has a PhD in Sustainable Energy Systems from MIT Portugal. He has two MSc degree one in Electric Power Engineering and the second in Entrepreneurship and Innovation Management from KTH, Stockholm Sweden.	images/profiles/Screenshot_2019-09-03_at_18.10.49.jpg	\N	\N	https://www.linkedin.com/in/ahmadkarnama/	\N	3
2	\N		\N	\N	\N	\N	2
\.


--
-- Data for Name: spritju_service; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_service (id, title, slug, content, "Meta_description", "Meta_keywords", "Service_image") FROM stdin;
1	Install	install	<h4>We are Master of Creative Design and tailored calculation of a solar system on your rooftop. We guarantee our solar system to be installed by our super-skilled technicians on your rooftop. The solar components are manufactured by best companies from around the world, which are including high-quality PV panels, inverter, charger controller, battery, etc.</h4>		install	images/posts/slide-two.jpg
2	Gain More	gain-more	<h4>Blockchain is a special technology for peer-to-peer transaction platforms that uses decentralised storage to record all transaction data. Blockchain technology strengthens the market role of individual consumers and producers. It enables prosumers, i.e., households that not only consume but also produce energy, to buy and sell energy directly, with a high degree of autonomy.</h4>		Gain More	images/posts/slider_05.jpg
3	Save More	save-more	<h4>Spritju Home enables customers to choose the functions that fit their special needs, from energy management to e-mobility, smart lighting to heating control, simply and conveniently. With the spritju software, energy consumption, and generation for your house can be controlled and monitored by your gadget.</h4>		Save More	images/posts/slider_02_OdRlu1e.jpg
\.


--
-- Data for Name: spritju_sitesettings; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_sitesettings (id, "Spritju_support_Email", "Spritju_phone_number", "Spritju_Instagram_url", "Spritju_Facebook_url", "Spritju_linkedin_url", "Spritju_twitter_url", "Solar_panel_price", "Solar_panel_power", system_off_price, "Panel_area", area_ratio, "Actionbox_description", "Actionbox_title", "Actionbox_url", "Actionbox_url_text") FROM stdin;
1	support@spritju.com	+46725382565	https://www.instagram.com/spritju/	https://www.facebook.com/spritju.se/?eid=ARCIE9Eps0uFRFNNDGJpMXX19RAbrMLszSm64MesffoqcPIIb9HppacQJMxijt-kfAgeJh-BEMlQ-DTY	https://www.linkedin.com/showcase/spritju/?viewAsMember=true	https://twitter.com/	11	310	0.699999999999999956	1.6399999999999999	0.299999999999999989	test description ..................	Spritju: The most powerful ever on the market	https://spritju.com	INSTALL NOW
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 48, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 4, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 78, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 120, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 16, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 19, true);


--
-- Name: spritju_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_category_id_seq', 2, true);


--
-- Name: spritju_category_posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_category_posts_id_seq', 1, false);


--
-- Name: spritju_client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_client_id_seq', 1, false);


--
-- Name: spritju_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_comment_id_seq', 27, true);


--
-- Name: spritju_contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_contact_id_seq', 459, true);


--
-- Name: spritju_faq_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_faq_id_seq', 1, false);


--
-- Name: spritju_new_customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_new_customer_id_seq', 61, true);


--
-- Name: spritju_post_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_post_category_id_seq', 6, true);


--
-- Name: spritju_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_post_id_seq', 4, true);


--
-- Name: spritju_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_profile_id_seq', 4, true);


--
-- Name: spritju_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_service_id_seq', 3, true);


--
-- Name: spritju_sitesettings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_sitesettings_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: spritju_category spritju_category_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category
    ADD CONSTRAINT spritju_category_pkey PRIMARY KEY (id);


--
-- Name: spritju_category_posts spritju_category_posts_category_id_post_id_92b4ac59_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts
    ADD CONSTRAINT spritju_category_posts_category_id_post_id_92b4ac59_uniq UNIQUE (category_id, post_id);


--
-- Name: spritju_category_posts spritju_category_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts
    ADD CONSTRAINT spritju_category_posts_pkey PRIMARY KEY (id);


--
-- Name: spritju_client spritju_client_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_client
    ADD CONSTRAINT spritju_client_pkey PRIMARY KEY (id);


--
-- Name: spritju_comment spritju_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_comment
    ADD CONSTRAINT spritju_comment_pkey PRIMARY KEY (id);


--
-- Name: spritju_contact spritju_contact_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_contact
    ADD CONSTRAINT spritju_contact_pkey PRIMARY KEY (id);


--
-- Name: spritju_faq spritju_faq_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_faq
    ADD CONSTRAINT spritju_faq_pkey PRIMARY KEY (id);


--
-- Name: spritju_new_customer spritju_new_customer_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_new_customer
    ADD CONSTRAINT spritju_new_customer_pkey PRIMARY KEY (id);


--
-- Name: spritju_post_category spritju_post_category_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category
    ADD CONSTRAINT spritju_post_category_pkey PRIMARY KEY (id);


--
-- Name: spritju_post_category spritju_post_category_post_id_category_id_0e0ab393_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category
    ADD CONSTRAINT spritju_post_category_post_id_category_id_0e0ab393_uniq UNIQUE (post_id, category_id);


--
-- Name: spritju_post spritju_post_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post
    ADD CONSTRAINT spritju_post_pkey PRIMARY KEY (id);


--
-- Name: spritju_post spritju_post_slug_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post
    ADD CONSTRAINT spritju_post_slug_key UNIQUE (slug);


--
-- Name: spritju_post spritju_post_title_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post
    ADD CONSTRAINT spritju_post_title_key UNIQUE (title);


--
-- Name: spritju_profile spritju_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_profile
    ADD CONSTRAINT spritju_profile_pkey PRIMARY KEY (id);


--
-- Name: spritju_profile spritju_profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_profile
    ADD CONSTRAINT spritju_profile_user_id_key UNIQUE (user_id);


--
-- Name: spritju_service spritju_service_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_service
    ADD CONSTRAINT spritju_service_pkey PRIMARY KEY (id);


--
-- Name: spritju_service spritju_service_slug_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_service
    ADD CONSTRAINT spritju_service_slug_key UNIQUE (slug);


--
-- Name: spritju_service spritju_service_title_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_service
    ADD CONSTRAINT spritju_service_title_key UNIQUE (title);


--
-- Name: spritju_sitesettings spritju_sitesettings_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_sitesettings
    ADD CONSTRAINT spritju_sitesettings_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: spritju_category_posts_category_id_acee5071; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_category_posts_category_id_acee5071 ON public.spritju_category_posts USING btree (category_id);


--
-- Name: spritju_category_posts_post_id_f95435a1; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_category_posts_post_id_f95435a1 ON public.spritju_category_posts USING btree (post_id);


--
-- Name: spritju_category_slug_b051961d; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_category_slug_b051961d ON public.spritju_category USING btree (slug);


--
-- Name: spritju_category_slug_b051961d_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_category_slug_b051961d_like ON public.spritju_category USING btree (slug varchar_pattern_ops);


--
-- Name: spritju_comment_post_id_50785479; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_comment_post_id_50785479 ON public.spritju_comment USING btree (post_id);


--
-- Name: spritju_comment_replier_id_952e5e7f; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_comment_replier_id_952e5e7f ON public.spritju_comment USING btree (replier_id);


--
-- Name: spritju_faq_replier_id_ee19d1ae; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_faq_replier_id_ee19d1ae ON public.spritju_faq USING btree (replier_id);


--
-- Name: spritju_post_author_id_5facbae0; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_author_id_5facbae0 ON public.spritju_post USING btree (author_id);


--
-- Name: spritju_post_category_category_id_2e030f78; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_category_category_id_2e030f78 ON public.spritju_post_category USING btree (category_id);


--
-- Name: spritju_post_category_post_id_95148d56; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_category_post_id_95148d56 ON public.spritju_post_category USING btree (post_id);


--
-- Name: spritju_post_slug_c723d85f_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_slug_c723d85f_like ON public.spritju_post USING btree (slug varchar_pattern_ops);


--
-- Name: spritju_post_title_be2d483c_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_title_be2d483c_like ON public.spritju_post USING btree (title varchar_pattern_ops);


--
-- Name: spritju_service_slug_ad6467ea_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_service_slug_ad6467ea_like ON public.spritju_service USING btree (slug varchar_pattern_ops);


--
-- Name: spritju_service_title_f63441d0_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_service_title_f63441d0_like ON public.spritju_service USING btree (title varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_category_posts spritju_category_pos_category_id_acee5071_fk_spritju_c; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts
    ADD CONSTRAINT spritju_category_pos_category_id_acee5071_fk_spritju_c FOREIGN KEY (category_id) REFERENCES public.spritju_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_category_posts spritju_category_posts_post_id_f95435a1_fk_spritju_post_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts
    ADD CONSTRAINT spritju_category_posts_post_id_f95435a1_fk_spritju_post_id FOREIGN KEY (post_id) REFERENCES public.spritju_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_comment spritju_comment_post_id_50785479_fk_spritju_post_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_comment
    ADD CONSTRAINT spritju_comment_post_id_50785479_fk_spritju_post_id FOREIGN KEY (post_id) REFERENCES public.spritju_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_comment spritju_comment_replier_id_952e5e7f_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_comment
    ADD CONSTRAINT spritju_comment_replier_id_952e5e7f_fk_auth_user_id FOREIGN KEY (replier_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_faq spritju_faq_replier_id_ee19d1ae_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_faq
    ADD CONSTRAINT spritju_faq_replier_id_ee19d1ae_fk_auth_user_id FOREIGN KEY (replier_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_post spritju_post_author_id_5facbae0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post
    ADD CONSTRAINT spritju_post_author_id_5facbae0_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_post_category spritju_post_categor_category_id_2e030f78_fk_spritju_c; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category
    ADD CONSTRAINT spritju_post_categor_category_id_2e030f78_fk_spritju_c FOREIGN KEY (category_id) REFERENCES public.spritju_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_post_category spritju_post_category_post_id_95148d56_fk_spritju_post_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category
    ADD CONSTRAINT spritju_post_category_post_id_95148d56_fk_spritju_post_id FOREIGN KEY (post_id) REFERENCES public.spritju_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_profile spritju_profile_user_id_f85577ea_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_profile
    ADD CONSTRAINT spritju_profile_user_id_f85577ea_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

