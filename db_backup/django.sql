--
-- PostgreSQL database dump
--

-- Dumped from database version 10.17 (Ubuntu 10.17-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.17 (Ubuntu 10.17-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO django;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO django;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO django;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO django;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO django;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO django;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO django;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO django;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO django;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO django;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO django;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO django;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO django;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO django;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO django;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO django;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO django;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO django;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO django;

--
-- Name: spritju_category; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_category (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    slug character varying(80) NOT NULL,
    description text
);


ALTER TABLE public.spritju_category OWNER TO django;

--
-- Name: spritju_category_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_category_id_seq OWNER TO django;

--
-- Name: spritju_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_category_id_seq OWNED BY public.spritju_category.id;


--
-- Name: spritju_category_posts; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_category_posts (
    id integer NOT NULL,
    category_id integer NOT NULL,
    post_id integer NOT NULL
);


ALTER TABLE public.spritju_category_posts OWNER TO django;

--
-- Name: spritju_category_posts_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_category_posts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_category_posts_id_seq OWNER TO django;

--
-- Name: spritju_category_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_category_posts_id_seq OWNED BY public.spritju_category_posts.id;


--
-- Name: spritju_client; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_client (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    url character varying(300),
    logo character varying(100)
);


ALTER TABLE public.spritju_client OWNER TO django;

--
-- Name: spritju_client_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_client_id_seq OWNER TO django;

--
-- Name: spritju_client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_client_id_seq OWNED BY public.spritju_client.id;


--
-- Name: spritju_comment; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_comment (
);


ALTER TABLE public.spritju_comment OWNER TO django;

--
-- Name: spritju_contact; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_contact (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    email character varying(255) NOT NULL,
    phone character varying(25) NOT NULL,
    body text NOT NULL
);


ALTER TABLE public.spritju_contact OWNER TO django;

--
-- Name: spritju_contact_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_contact_id_seq OWNER TO django;

--
-- Name: spritju_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_contact_id_seq OWNED BY public.spritju_contact.id;


--
-- Name: spritju_faq; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_faq (
    id integer NOT NULL,
    questioner_name character varying(100) NOT NULL,
    questioner_email character varying(255) NOT NULL,
    question text NOT NULL,
    reply text NOT NULL,
    status integer NOT NULL,
    replier_id integer NOT NULL
);


ALTER TABLE public.spritju_faq OWNER TO django;

--
-- Name: spritju_faq_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_faq_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_faq_id_seq OWNER TO django;

--
-- Name: spritju_faq_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_faq_id_seq OWNED BY public.spritju_faq.id;


--
-- Name: spritju_new_customer; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_new_customer (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    email character varying(255) NOT NULL,
    phone character varying(100) NOT NULL,
    address character varying(300),
    "Lat" double precision,
    lon double precision,
    offer double precision,
    body text
);


ALTER TABLE public.spritju_new_customer OWNER TO django;

--
-- Name: spritju_new_customer_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_new_customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_new_customer_id_seq OWNER TO django;

--
-- Name: spritju_new_customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_new_customer_id_seq OWNED BY public.spritju_new_customer.id;


--
-- Name: spritju_post; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_post (
    id integer NOT NULL,
    title character varying(80) NOT NULL,
    slug character varying(80) NOT NULL,
    content text NOT NULL,
    updated_on date NOT NULL,
    created_on date NOT NULL,
    publish_on date NOT NULL,
    author_id integer NOT NULL,
    post_image character varying(100),
    "Meta_description" text,
    "Meta_keywords" character varying(90),
    status integer NOT NULL,
    short_description text
);


ALTER TABLE public.spritju_post OWNER TO django;

--
-- Name: spritju_post_category; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_post_category (
    id integer NOT NULL,
    post_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.spritju_post_category OWNER TO django;

--
-- Name: spritju_post_category_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_post_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_post_category_id_seq OWNER TO django;

--
-- Name: spritju_post_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_post_category_id_seq OWNED BY public.spritju_post_category.id;


--
-- Name: spritju_post_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_post_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_post_id_seq OWNER TO django;

--
-- Name: spritju_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_post_id_seq OWNED BY public.spritju_post.id;


--
-- Name: spritju_profile; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_profile (
    id integer NOT NULL,
    bio text,
    instagram_url character varying(300),
    twitter_url character varying(300),
    linkedin_url character varying(300),
    facebook_url character varying(300),
    user_id integer NOT NULL,
    profile_image character varying(100)
);


ALTER TABLE public.spritju_profile OWNER TO django;

--
-- Name: spritju_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_profile_id_seq OWNER TO django;

--
-- Name: spritju_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_profile_id_seq OWNED BY public.spritju_profile.id;


--
-- Name: spritju_sitesettings; Type: TABLE; Schema: public; Owner: django
--

CREATE TABLE public.spritju_sitesettings (
    id integer NOT NULL,
    "Spritju_support_Email" character varying(254) NOT NULL,
    "Spritju_phone_number" character varying(255),
    "Spritju_Instagram_url" character varying(300),
    "Spritju_Facebook_url" character varying(300),
    "Spritju_linkedin_url" character varying(300),
    "Spritju_twitter_url" character varying(300)
);


ALTER TABLE public.spritju_sitesettings OWNER TO django;

--
-- Name: spritju_sitesettings_id_seq; Type: SEQUENCE; Schema: public; Owner: django
--

CREATE SEQUENCE public.spritju_sitesettings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spritju_sitesettings_id_seq OWNER TO django;

--
-- Name: spritju_sitesettings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: django
--

ALTER SEQUENCE public.spritju_sitesettings_id_seq OWNED BY public.spritju_sitesettings.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: spritju_category id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category ALTER COLUMN id SET DEFAULT nextval('public.spritju_category_id_seq'::regclass);


--
-- Name: spritju_category_posts id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts ALTER COLUMN id SET DEFAULT nextval('public.spritju_category_posts_id_seq'::regclass);


--
-- Name: spritju_client id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_client ALTER COLUMN id SET DEFAULT nextval('public.spritju_client_id_seq'::regclass);


--
-- Name: spritju_contact id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_contact ALTER COLUMN id SET DEFAULT nextval('public.spritju_contact_id_seq'::regclass);


--
-- Name: spritju_faq id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_faq ALTER COLUMN id SET DEFAULT nextval('public.spritju_faq_id_seq'::regclass);


--
-- Name: spritju_new_customer id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_new_customer ALTER COLUMN id SET DEFAULT nextval('public.spritju_new_customer_id_seq'::regclass);


--
-- Name: spritju_post id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post ALTER COLUMN id SET DEFAULT nextval('public.spritju_post_id_seq'::regclass);


--
-- Name: spritju_post_category id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category ALTER COLUMN id SET DEFAULT nextval('public.spritju_post_category_id_seq'::regclass);


--
-- Name: spritju_profile id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_profile ALTER COLUMN id SET DEFAULT nextval('public.spritju_profile_id_seq'::regclass);


--
-- Name: spritju_sitesettings id; Type: DEFAULT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_sitesettings ALTER COLUMN id SET DEFAULT nextval('public.spritju_sitesettings_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_group (id, name) FROM stdin;
1	Admin
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	1
2	1	2
3	1	3
4	1	4
5	1	5
6	1	6
7	1	7
8	1	8
9	1	9
10	1	10
11	1	11
12	1	12
13	1	13
14	1	14
15	1	15
16	1	16
17	1	17
18	1	18
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add permission	3	add_permission
8	Can change permission	3	change_permission
9	Can delete permission	3	delete_permission
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add post	7	add_post
20	Can change post	7	change_post
21	Can delete post	7	delete_post
22	Can add category	8	add_category
23	Can change category	8	change_category
24	Can delete category	8	delete_category
25	Can add comment	9	add_comment
26	Can change comment	9	change_comment
27	Can delete comment	9	delete_comment
28	Can add faq	10	add_faq
29	Can change faq	10	change_faq
30	Can delete faq	10	delete_faq
31	Can add contact	11	add_contact
32	Can change contact	11	change_contact
33	Can delete contact	11	delete_contact
34	Can add new_ customer	12	add_new_customer
35	Can change new_ customer	12	change_new_customer
36	Can delete new_ customer	12	delete_new_customer
37	Can add profile	13	add_profile
38	Can change profile	13	change_profile
39	Can delete profile	13	delete_profile
40	Can add client	14	add_client
41	Can change client	14	change_client
42	Can delete client	14	delete_client
43	Can add site settings	15	add_sitesettings
44	Can change site settings	15	change_sitesettings
45	Can delete site settings	15	delete_sitesettings
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
3	pbkdf2_sha256$36000$HkXmzS4AC6kE$By5bQnN12VfoppjnFjvtBXnJeTWmy4tvDvDCHQ5v0bA=	\N	t	rahimi	ainollah	rahimi	a.rahimisadegh@gmail.com	t	t	2018-12-27 07:00:22+00
2	pbkdf2_sha256$36000$iUAGbfUcRYjR$HbmZe15x+612FU0G5QriamT/z6yCUhGEy4+DPJ359FE=	\N	t	karnama	ahmad	karnama	ahmad.karnama@gmail.com	t	t	2018-12-16 22:03:42+00
4	pbkdf2_sha256$36000$ldtqgQsihs8a$g+HQAH1JG/Kly4iLSh/k/pKfc4oiwBqZG2PYhIxsPeY=	2019-01-31 06:23:58.412715+00	t	mehrabiyan	fateme	mehrabiyan	fatemehhmehrabian@gamil.com	t	t	2018-12-27 07:26:38+00
1	pbkdf2_sha256$36000$sM6AgiYtZwxH$1NPaHbY+s7DhwoiFSP9laxPM6aliylMR6eUoO3Wem2k=	2019-02-14 12:21:59.741356+00	t	admin	Hamid Reza	Ashrafnezhad	ashrafnezhad.hamidreza@gmail.com	t	t	2018-12-16 21:58:28+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
1	2	1
2	3	1
3	4	1
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-12-16 22:00:43.394602+00	1	Admin	1	[{"added": {}}]	2	1
2	2018-12-16 22:03:42.200246+00	2	karnama	1	[{"added": {}}]	4	1
3	2018-12-16 22:04:09.87523+00	2	karnama	2	[{"changed": {"fields": ["first_name", "last_name", "is_staff", "is_superuser"]}}]	4	1
4	2018-12-16 22:04:13.963287+00	2	karnama	2	[]	4	1
5	2018-12-16 22:04:50.768417+00	1	admin	2	[{"changed": {"fields": ["first_name", "last_name", "last_login"]}}]	4	1
6	2018-12-17 19:33:03.577346+00	2	karnama	2	[{"changed": {"fields": ["groups"]}}]	4	1
7	2018-12-27 07:00:22.215788+00	3	rahimi	1	[{"added": {}}]	4	1
8	2018-12-27 07:01:18.529229+00	3	rahimi	2	[{"changed": {"fields": ["first_name", "last_name", "email", "is_staff", "is_superuser", "groups"]}}]	4	1
9	2018-12-27 07:01:25.285677+00	3	rahimi	2	[]	4	1
10	2018-12-27 07:26:38.65445+00	4	mehrabiyan	1	[{"added": {}}]	4	1
11	2018-12-27 07:27:51.380762+00	4	mehrabiyan	2	[{"changed": {"fields": ["first_name", "last_name", "email", "is_staff", "is_superuser", "groups"]}}]	4	1
12	2018-12-27 12:59:40.851916+00	1	Category object	1	[{"added": {}}]	8	1
13	2018-12-27 13:01:42.083867+00	2	Category object	1	[{"added": {}}]	8	1
14	2018-12-27 13:15:32.577905+00	2	karnama	2	[{"changed": {"fields": ["email"]}}]	4	1
15	2019-01-01 22:24:44.237341+00	1	Post object	1	[{"added": {}}]	7	1
16	2019-01-05 20:07:20.300945+00	2	test 1	1	[{"added": {}}]	7	1
17	2019-01-05 20:09:37.833937+00	3	test 3 test	1	[{"added": {}}]	7	1
18	2019-01-07 22:21:28.50951+00	3	test 3 test	2	[{"changed": {"fields": ["category", "short_description"]}}]	7	1
19	2019-01-07 22:26:14.834062+00	2	test 1	2	[{"changed": {"fields": ["short_description", "Meta_description"]}}]	7	1
20	2019-01-07 22:26:32.924731+00	1	test	2	[{"changed": {"fields": ["content", "short_description", "Meta_description"]}}]	7	1
21	2019-01-07 22:33:13.756404+00	3	test 3 test	2	[{"changed": {"fields": ["content"]}}]	7	1
22	2019-01-07 22:33:26.880701+00	2	test 1	2	[{"changed": {"fields": ["content"]}}]	7	1
23	2019-01-07 22:33:38.581897+00	1	test	2	[{"changed": {"fields": ["content"]}}]	7	1
24	2019-01-14 18:51:07.483959+00	2	spritju	2	[]	8	1
25	2019-01-14 18:51:17.274326+00	2	spritju	2	[]	8	1
26	2019-01-14 18:51:26.402154+00	1	test cat	2	[]	8	1
27	2019-01-31 06:20:09.520193+00	4	mehrabiyan	2	[{"changed": {"fields": ["password"]}}]	4	1
28	2019-01-31 06:20:18.666002+00	4	mehrabiyan	2	[]	4	1
29	2019-01-31 06:21:50.804402+00	2	spritju	2	[{"changed": {"fields": ["posts"]}}]	8	1
30	2019-01-31 06:26:45.45228+00	4	test post 1	1	[{"added": {}}]	7	4
31	2019-02-07 13:04:57.452795+00	2	Hamid Reza Ashrafnezhad	1	[{"added": {}}]	12	4
32	2019-02-07 18:23:42.92722+00	1	Profile object	1	[{"added": {}}]	13	4
33	2019-02-14 00:00:08.914717+00	1	Profile object	2	[{"changed": {"fields": ["twitter_url", "linkedin_url", "facebook_url"]}}]	13	4
34	2019-02-14 00:05:50.997919+00	1	Profile object	2	[{"changed": {"fields": ["profile_image"]}}]	13	4
35	2019-02-14 12:23:19.237817+00	1	Profile object	2	[{"changed": {"fields": ["user"]}}]	13	1
36	2019-02-14 12:23:41.918566+00	1	Profile object	2	[{"changed": {"fields": ["user"]}}]	13	1
37	2019-02-14 19:27:05.714281+00	1	SiteSettings object	2	[{"changed": {"fields": ["Spritju_phone_number", "Spritju_Instagram_url", "Spritju_Facebook_url", "Spritju_linkedin_url", "Spritju_twitter_url"]}}]	15	1
38	2019-02-14 20:21:13.335714+00	1	SiteSettings object	2	[{"changed": {"fields": ["Spritju_phone_number"]}}]	15	1
39	2019-02-14 22:57:41.001562+00	1	test Question test Question test Question test Question?	1	[{"added": {}}]	10	1
40	2019-02-14 22:58:21.6621+00	2	test test test test?	1	[{"added": {}}]	10	1
41	2019-02-14 23:01:05.394194+00	4	test post 1	2	[{"changed": {"fields": ["author"]}}]	7	1
42	2019-02-14 23:03:59.227426+00	3	hamidashraf@gmail.com	2	[{"changed": {"fields": ["Message", "replier", "reply", "post", "status"]}}]	9	1
43	2019-02-14 23:04:45.694332+00	2	Hamid Reza AshrafnezhadRazanGame@gmail.com	2	[{"changed": {"fields": ["Message", "replier", "reply", "post", "status"]}}]	9	1
44	2019-02-14 23:04:54.942785+00	1	hamidRazanGame@gmail.com	3		9	1
45	2019-02-14 23:05:42.09383+00	3	hamidashraf@gmail.com	2	[{"changed": {"fields": ["Website_Url"]}}]	9	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	group
3	auth	permission
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	spritju	post
8	spritju	category
9	spritju	comment
10	spritju	faq
11	spritju	contact
12	spritju	new_customer
13	spritju	profile
14	spritju	client
15	spritju	sitesettings
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-12-16 21:57:21.571436+00
2	auth	0001_initial	2018-12-16 21:57:21.681409+00
3	admin	0001_initial	2018-12-16 21:57:21.730398+00
4	admin	0002_logentry_remove_auto_add	2018-12-16 21:57:21.749934+00
5	contenttypes	0002_remove_content_type_name	2018-12-16 21:57:21.778223+00
6	auth	0002_alter_permission_name_max_length	2018-12-16 21:57:21.786669+00
7	auth	0003_alter_user_email_max_length	2018-12-16 21:57:21.799339+00
8	auth	0004_alter_user_username_opts	2018-12-16 21:57:21.810757+00
9	auth	0005_alter_user_last_login_null	2018-12-16 21:57:21.825455+00
10	auth	0006_require_contenttypes_0002	2018-12-16 21:57:21.828173+00
11	auth	0007_alter_validators_add_error_messages	2018-12-16 21:57:21.841991+00
12	auth	0008_alter_user_username_max_length	2018-12-16 21:57:21.866158+00
13	sessions	0001_initial	2018-12-16 21:57:21.884231+00
14	spritju	0001_initial	2018-12-17 21:02:32.041301+00
15	spritju	0002_post_post_image	2018-12-26 13:35:25.627957+00
16	spritju	0003_auto_20181226_1355	2018-12-26 13:57:09.697207+00
17	spritju	0004_auto_20181226_1357	2018-12-26 13:57:09.725422+00
18	spritju	0005_auto_20181226_1411	2018-12-26 14:11:16.74177+00
19	spritju	0006_auto_20181227_1020	2018-12-27 10:21:15.349624+00
20	spritju	0007_contact_new_customer	2018-12-27 11:03:00.028322+00
21	spritju	0008_auto_20190101_2355	2019-01-01 23:55:21.256372+00
22	spritju	0009_auto_20190102_0013	2019-01-02 00:13:13.773033+00
23	spritju	0010_auto_20190105_1445	2019-01-05 14:45:57.798934+00
24	spritju	0011_auto_20190105_1450	2019-01-05 14:50:53.043771+00
25	spritju	0012_auto_20190105_1951	2019-01-05 19:51:34.733253+00
26	spritju	0013_post_short_description	2019-01-07 22:11:03.420367+00
27	spritju	0014_auto_20190109_2135	2019-01-09 21:35:57.977835+00
28	spritju	0015_category_description	2019-01-14 18:46:47.641851+00
29	spritju	0016_comment_website_url	2019-01-17 22:55:09.425924+00
30	spritju	0017_auto_20190207_1255	2019-02-07 12:55:32.657464+00
31	spritju	0018_auto_20190207_1301	2019-02-07 13:01:37.133993+00
32	spritju	0019_auto_20190207_1302	2019-02-07 13:02:30.294513+00
33	spritju	0020_remove_new_customer_body	2019-02-07 13:03:22.185667+00
34	spritju	0021_new_customer_body	2019-02-07 13:03:43.842909+00
35	spritju	0022_auto_20190207_1304	2019-02-07 13:04:43.204542+00
36	spritju	0023_profile	2019-02-07 17:48:33.217494+00
37	spritju	0024_auto_20190207_1803	2019-02-07 18:03:12.516719+00
38	spritju	0025_client_sitesettings	2019-02-13 23:18:08.067071+00
39	spritju	0026_auto_20190214_0005	2019-02-14 00:05:21.661775+00
40	spritju	0027_auto_20190214_1222	2019-02-14 12:22:55.165828+00
41	spritju	0028_auto_20190215_0010	2019-02-15 15:49:33.047063+00
42	spritju	0029_auto_20190215_1543	2019-02-15 15:49:33.052613+00
43	spritju	0030_auto_20190215_1546	2019-02-15 15:49:33.056554+00
44	spritju	0031_auto_20190215_1549	2019-02-15 15:49:50.979191+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
voc3ezyg4gfm3jy5dwf97hkos2c48kfw	YTQxM2VjNGRiNzgxNWVlMmY4MDk1NjJlY2YzYTFhYjFlNDBhYTg1OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxNjYwYTBlYzIyNGJjZjE4OGQ0OTlhZGFhNDM0NDA3Y2YzNWRjMGQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-01-10 07:24:02.316618+00
yfn3d9wl1i4beud2nu1o0c0ukdd6tm4i	YTQxM2VjNGRiNzgxNWVlMmY4MDk1NjJlY2YzYTFhYjFlNDBhYTg1OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxNjYwYTBlYzIyNGJjZjE4OGQ0OTlhZGFhNDM0NDA3Y2YzNWRjMGQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-01-10 13:12:12.98779+00
ruetdgole10l5ksasobrsimta0p86xfy	YTQxM2VjNGRiNzgxNWVlMmY4MDk1NjJlY2YzYTFhYjFlNDBhYTg1OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxNjYwYTBlYzIyNGJjZjE4OGQ0OTlhZGFhNDM0NDA3Y2YzNWRjMGQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-01-15 21:10:05.152501+00
ammqac36rfpriji4eajs06d3grc46d0k	YTQxM2VjNGRiNzgxNWVlMmY4MDk1NjJlY2YzYTFhYjFlNDBhYTg1OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxNjYwYTBlYzIyNGJjZjE4OGQ0OTlhZGFhNDM0NDA3Y2YzNWRjMGQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-01-15 23:55:41.329399+00
dh0h4afwknew3brmsj7p2236hsbv36q0	YTQxM2VjNGRiNzgxNWVlMmY4MDk1NjJlY2YzYTFhYjFlNDBhYTg1OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxNjYwYTBlYzIyNGJjZjE4OGQ0OTlhZGFhNDM0NDA3Y2YzNWRjMGQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-01-16 00:04:17.051627+00
r49cat79hwy9bi9x0b4o8b08mf6g4rb1	YTQxM2VjNGRiNzgxNWVlMmY4MDk1NjJlY2YzYTFhYjFlNDBhYTg1OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxNjYwYTBlYzIyNGJjZjE4OGQ0OTlhZGFhNDM0NDA3Y2YzNWRjMGQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-01-28 18:49:22.158726+00
usxwws56arc0q6urqhwxvfmo3vjp9tk5	YWQxZmY0NzFkOTRiYjc4OThhMDc2NjFhMWYwMThkYzE1ZDJlOTc3Yjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwZDY4YWE2NzE3NDRkYjViZGM3OGQ1NjY2YmFlMzM0MzRjMzQ2YTQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2019-02-14 06:23:58.415891+00
wrbkj9nvb96go99mfmp6th4dgcfg3xgz	YTQxM2VjNGRiNzgxNWVlMmY4MDk1NjJlY2YzYTFhYjFlNDBhYTg1OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxNjYwYTBlYzIyNGJjZjE4OGQ0OTlhZGFhNDM0NDA3Y2YzNWRjMGQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-02-28 12:21:59.754888+00
\.


--
-- Data for Name: spritju_category; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_category (id, name, slug, description) FROM stdin;
1	test cat	test-cat	
2	spritju	spritju	
\.


--
-- Data for Name: spritju_category_posts; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_category_posts (id, category_id, post_id) FROM stdin;
1	2	2
2	2	3
\.


--
-- Data for Name: spritju_client; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_client (id, name, url, logo) FROM stdin;
\.


--
-- Data for Name: spritju_comment; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_comment  FROM stdin;
\.


--
-- Data for Name: spritju_contact; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_contact (id, name, email, phone, body) FROM stdin;
1	hamid	razangame@gmail.com	razangame@gmail.com	test hello
2	hamid	test@gmail.com	test@gmail.com	hello
3	hamid	razangame@gmail.com	razangame@gmail.com	test
\.


--
-- Data for Name: spritju_faq; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_faq (id, questioner_name, questioner_email, question, reply, status, replier_id) FROM stdin;
1	hamid	RazanGame@gmail.com	test Question test Question test Question test Question?	test answer	0	3
2	test	RazanGame@gmail.com	test test test test?	answer answer answer answer answer answer answer answer	0	1
\.


--
-- Data for Name: spritju_new_customer; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_new_customer (id, name, email, phone, address, "Lat", lon, offer, body) FROM stdin;
2	Hamid Reza Ashrafnezhad	RazanGame@gmail.com	9369505828	ghasro dasht	\N	\N	\N	
3	hamid	emailtest@gmail.com	09177000624	default	0	0	0	\N
4	hamid 	test@gmail.com	09177000624	default	0	0	0	\N
\.


--
-- Data for Name: spritju_post; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_post (id, title, slug, content, updated_on, created_on, publish_on, author_id, post_image, "Meta_description", "Meta_keywords", status, short_description) FROM stdin;
3	test 3 test	test-3-test	<h3>Myth 4: Solar panels are too expensive</h3>\r\n\r\n<p>for an average house owner. Fact: thanks to government incentives, solar panels have a short payback period and give you huge savings on monthly bills and regular profits from the Feed in Tariff. Plus, if you&rsquo;re worried about the incentives being scrapped soon, keep into consideration that it would only lead to a drop in prices due to the increased competition. Finally, solar panels can dramatically increase the value of your house if you were to sell it in the future</p>	2019-01-07	2019-01-05	2019-01-05	1	images/posts/photo_2018-10-13_00-31-08.jpg	lead to a drop in prices due to the increased competition. Finally, solar panels can dramatically increase the value of your house if you were to sell it in the future	test3	0	Fact: thanks to government incentives, solar panels have a short payback period and give you huge savings .
2	test 1	test-1	<h3>Myth 4: Solar panels are too expensive</h3>\r\n\r\n<p>for an average house owner. Fact: thanks to government incentives, solar panels have a short payback period and give you huge savings on monthly bills and regular profits from the Feed in Tariff. Plus, if you&rsquo;re worried about the incentives being scrapped soon, keep into consideration that it would only lead to a drop in prices due to the increased competition. Finally, solar panels can dramatically increase the value of your house if you were to sell it in the future</p>	2019-01-07	2019-01-05	2019-01-05	1	images/posts/photo_2018-10-13_00-25-55.jpg	Fact: thanks to government incentives, solar panels have a short payback period and give you huge savings .	test1	0	Fact: thanks to government incentives, solar panels have a short payback period and give you huge savings .
1	test	test	<h3>Myth 4: Solar panels are too expensive</h3>\r\n\r\n<p>for an average house owner. Fact: thanks to government incentives, solar panels have a short payback period and give you huge savings on monthly bills and regular profits from the Feed in Tariff. Plus, if you&rsquo;re worried about the incentives being scrapped soon, keep into consideration that it would only lead to a drop in prices due to the increased competition. Finally, solar panels can dramatically increase the value of your house if you were to sell it in the future</p>	2019-01-07	2019-01-01	2019-01-01	1	images/posts/photo_2018-10-13_01-09-22.jpg	Fact: thanks to government incentives, solar panels have a short payback period and give you huge savings .	test	0	Fact: thanks to government incentives, solar panels have a short payback period and give you huge savings .
4	test post 1	test-post-1	<h2>Hello world</h2>\r\n\r\n<p>test test</p>	2019-02-14	2019-01-31	2019-01-25	1	images/posts/500_F_215566709_XEAcH6tTgXhMXhLaz8ayuFxQXdUDqgBD.jpg	Hello world\r\ntest test	test	0	Hello world\r\ntest test
\.


--
-- Data for Name: spritju_post_category; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_post_category (id, post_id, category_id) FROM stdin;
1	1	2
2	2	1
3	3	1
4	3	2
5	4	2
\.


--
-- Data for Name: spritju_profile; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_profile (id, bio, instagram_url, twitter_url, linkedin_url, facebook_url, user_id, profile_image) FROM stdin;
1	hell test bio for admin	https://www.instagram.com/hamid_ashrafnezhad/	https://twitter.com/HAshrafnezhad	https://www.linkedin.com/in/hamidreza-ashrafnezha/	https://www.facebook.com/hamid.ashf	1	images/profiles/photo_2018-02-18_17-37-21_u0b0697.jpg
\.


--
-- Data for Name: spritju_sitesettings; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.spritju_sitesettings (id, "Spritju_support_Email", "Spritju_phone_number", "Spritju_Instagram_url", "Spritju_Facebook_url", "Spritju_linkedin_url", "Spritju_twitter_url") FROM stdin;
1	support@spritju.com	+46725382565	https://www.instagram.com	https://www.facebook.com	https://www.linkedin.com	https://twitter.com/
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 18, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 45, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 3, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 4, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 45, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 15, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 44, true);


--
-- Name: spritju_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_category_id_seq', 2, true);


--
-- Name: spritju_category_posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_category_posts_id_seq', 2, true);


--
-- Name: spritju_client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_client_id_seq', 1, false);


--
-- Name: spritju_contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_contact_id_seq', 3, true);


--
-- Name: spritju_faq_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_faq_id_seq', 2, true);


--
-- Name: spritju_new_customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_new_customer_id_seq', 4, true);


--
-- Name: spritju_post_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_post_category_id_seq', 5, true);


--
-- Name: spritju_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_post_id_seq', 4, true);


--
-- Name: spritju_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_profile_id_seq', 1, true);


--
-- Name: spritju_sitesettings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.spritju_sitesettings_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: spritju_category spritju_category_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category
    ADD CONSTRAINT spritju_category_pkey PRIMARY KEY (id);


--
-- Name: spritju_category_posts spritju_category_posts_category_id_post_id_92b4ac59_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts
    ADD CONSTRAINT spritju_category_posts_category_id_post_id_92b4ac59_uniq UNIQUE (category_id, post_id);


--
-- Name: spritju_category_posts spritju_category_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts
    ADD CONSTRAINT spritju_category_posts_pkey PRIMARY KEY (id);


--
-- Name: spritju_client spritju_client_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_client
    ADD CONSTRAINT spritju_client_pkey PRIMARY KEY (id);


--
-- Name: spritju_contact spritju_contact_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_contact
    ADD CONSTRAINT spritju_contact_pkey PRIMARY KEY (id);


--
-- Name: spritju_faq spritju_faq_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_faq
    ADD CONSTRAINT spritju_faq_pkey PRIMARY KEY (id);


--
-- Name: spritju_new_customer spritju_new_customer_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_new_customer
    ADD CONSTRAINT spritju_new_customer_pkey PRIMARY KEY (id);


--
-- Name: spritju_post_category spritju_post_category_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category
    ADD CONSTRAINT spritju_post_category_pkey PRIMARY KEY (id);


--
-- Name: spritju_post_category spritju_post_category_post_id_category_id_0e0ab393_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category
    ADD CONSTRAINT spritju_post_category_post_id_category_id_0e0ab393_uniq UNIQUE (post_id, category_id);


--
-- Name: spritju_post spritju_post_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post
    ADD CONSTRAINT spritju_post_pkey PRIMARY KEY (id);


--
-- Name: spritju_post spritju_post_slug_c723d85f_uniq; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post
    ADD CONSTRAINT spritju_post_slug_c723d85f_uniq UNIQUE (slug);


--
-- Name: spritju_post spritju_post_title_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post
    ADD CONSTRAINT spritju_post_title_key UNIQUE (title);


--
-- Name: spritju_profile spritju_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_profile
    ADD CONSTRAINT spritju_profile_pkey PRIMARY KEY (id);


--
-- Name: spritju_profile spritju_profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_profile
    ADD CONSTRAINT spritju_profile_user_id_key UNIQUE (user_id);


--
-- Name: spritju_sitesettings spritju_sitesettings_pkey; Type: CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_sitesettings
    ADD CONSTRAINT spritju_sitesettings_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: spritju_category_posts_category_id_acee5071; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_category_posts_category_id_acee5071 ON public.spritju_category_posts USING btree (category_id);


--
-- Name: spritju_category_posts_post_id_f95435a1; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_category_posts_post_id_f95435a1 ON public.spritju_category_posts USING btree (post_id);


--
-- Name: spritju_category_slug_b051961d; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_category_slug_b051961d ON public.spritju_category USING btree (slug);


--
-- Name: spritju_category_slug_b051961d_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_category_slug_b051961d_like ON public.spritju_category USING btree (slug varchar_pattern_ops);


--
-- Name: spritju_faq_replier_id_ee19d1ae; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_faq_replier_id_ee19d1ae ON public.spritju_faq USING btree (replier_id);


--
-- Name: spritju_post_author_id_5facbae0; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_author_id_5facbae0 ON public.spritju_post USING btree (author_id);


--
-- Name: spritju_post_category_category_id_2e030f78; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_category_category_id_2e030f78 ON public.spritju_post_category USING btree (category_id);


--
-- Name: spritju_post_category_post_id_95148d56; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_category_post_id_95148d56 ON public.spritju_post_category USING btree (post_id);


--
-- Name: spritju_post_slug_c723d85f_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_slug_c723d85f_like ON public.spritju_post USING btree (slug varchar_pattern_ops);


--
-- Name: spritju_post_title_be2d483c_like; Type: INDEX; Schema: public; Owner: django
--

CREATE INDEX spritju_post_title_be2d483c_like ON public.spritju_post USING btree (title varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_category_posts spritju_category_pos_category_id_acee5071_fk_spritju_c; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts
    ADD CONSTRAINT spritju_category_pos_category_id_acee5071_fk_spritju_c FOREIGN KEY (category_id) REFERENCES public.spritju_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_category_posts spritju_category_posts_post_id_f95435a1_fk_spritju_post_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_category_posts
    ADD CONSTRAINT spritju_category_posts_post_id_f95435a1_fk_spritju_post_id FOREIGN KEY (post_id) REFERENCES public.spritju_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_faq spritju_faq_replier_id_ee19d1ae_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_faq
    ADD CONSTRAINT spritju_faq_replier_id_ee19d1ae_fk_auth_user_id FOREIGN KEY (replier_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_post spritju_post_author_id_5facbae0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post
    ADD CONSTRAINT spritju_post_author_id_5facbae0_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_post_category spritju_post_categor_category_id_2e030f78_fk_spritju_c; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category
    ADD CONSTRAINT spritju_post_categor_category_id_2e030f78_fk_spritju_c FOREIGN KEY (category_id) REFERENCES public.spritju_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_post_category spritju_post_category_post_id_95148d56_fk_spritju_post_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_post_category
    ADD CONSTRAINT spritju_post_category_post_id_95148d56_fk_spritju_post_id FOREIGN KEY (post_id) REFERENCES public.spritju_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spritju_profile spritju_profile_user_id_f85577ea_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: django
--

ALTER TABLE ONLY public.spritju_profile
    ADD CONSTRAINT spritju_profile_user_id_f85577ea_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

